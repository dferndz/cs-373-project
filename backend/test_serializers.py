import unittest
from flask import Flask
from controllers.controller import blueprint
from controllers.serializers import CountrySerializer, CitySerializer, FlightSerializer
from models.city import City
from models.country import Country
from models.flights import Flights


class TestFilters(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        app = Flask(__name__)
        app.register_blueprint(blueprint, url_prefix="")
        cls.app = app.test_client()

    def test_flights_serializer(self):
        flights_filter = FlightSerializer()
        query = Flights.query.limit(10).all()
        many = flights_filter.serialize(query, many=True)

        self.assertIsInstance(many, list)
        for item in many:
            self.assertIsInstance(item, dict)
            self.assertTrue("id" in item)
            self.assertTrue("departure_city" in item)
            self.assertTrue("departure_country_name" in item)

    def test_country_serializer(self):
        country_serializer = CountrySerializer()
        query = Country.query.limit(10).all()
        many = country_serializer.serialize(query, many=True)

        self.assertIsInstance(many, list)
        for item in many:
            self.assertIsInstance(item, dict)
            self.assertTrue("id" in item)
            self.assertTrue("name" in item)
            self.assertTrue("temp_average" in item)

    def test_city_serializer(self):
        city_serializer = CitySerializer()
        query = City.query.limit(10).all()
        many = city_serializer.serialize(query, many=True)

        self.assertIsInstance(many, list)
        for item in many:
            self.assertIsInstance(item, dict)
            self.assertTrue("id" in item)
            self.assertTrue("country_name" in item)
            self.assertTrue("flag_url" in item)
            self.assertTrue("name" in item)
