import os
from sqlalchemy import create_engine, func
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import database_exists, create_database, drop_database
from g_secrets import get_secret


uri = os.environ.get("DATABASE_URL")
if uri.startswith("secret"):
    _, name = uri.split("/")
    uri = get_secret(name)

engine = create_engine(uri)
db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine)
)
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    import models

    Base.metadata.create_all(bind=engine)


def clear_db():
    # import models
    Base.metadata.drop_all(bind=engine)
    init_db()
