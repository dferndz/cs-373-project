from database.populate import add_country
from utils import constants
import requests


# Adds a hard-coded list of countries to the database.
# Returns a string of those country names separated by commas
def add_countries_small(db_session):
    countries = ["US", "China", "France", "Canada", "India", "South Africa", "Chile"]
    for country in countries:
        country_names = [country]
        add_country(db_session, country_names)
    db_session.commit()
    return ",".join(countries)


# Adds the top_x most populous countries to the DB
# Returns the how many countries were added.
def add_countries_all(db_session, n_countries=1):
    import requests

    url = "https://restcountries.com/v2/all"
    response = requests.request("GET", url).json()
    # top_x = 3 # TODO: Add this as an environment variable or something? 125 seems to be the optimal value
    country_pops = [
        ((entry["name"], entry["alpha2Code"], entry["alpha3Code"]), entry["population"])
        for entry in response
    ]
    country_pops.sort(key=lambda x: x[1], reverse=True)
    assert n_countries < len(country_pops)
    countries = [country[0] for country in country_pops[:n_countries]]

    counter = 1
    for country in countries:
        add_country(db_session, country)
        print(
            "("
            + str(counter)
            + "/"
            + str(len(countries))
            + ") Added country: "
            + country[0]
        )
        counter += 1
    db_session.commit()
    return n_countries


# Adds the top_x most popular cities of all
# the countries in the DB
def add_cities_all(db_session):
    from models.country import Country
    from models.city import City
    from utils import airportsdata
    from apis.utils import get_amadeus_api_access_token
    from database.populate import add_city
    from apis.covid import get_country_and_city_iata_code

    # Setup tools to populate db...
    airports = airportsdata.load("city")
    amadeus_access_token = get_amadeus_api_access_token()

    cities_added = 0

    print("Querying Countries...")
    # Loop through Country table
    # to get country_name and capital_name
    countries = db_session.query(Country).all()
    print("Iterating Countries...")
    for country in countries:

        # TODO: Get a list of cities for
        # each country. For now, we just take the capital
        # of each city
        country_name = country.name
        city_name = country.capital

        # if city_name in constants.CITY_NAME_CORRECTIONS:
        #     city_name = constants.CITY_NAME_CORRECTIONS[city_name]

        print("Current Location:", country_name, ",", city_name)
        # country_city_iata_codes = get_country_and_city_iata_code(city_name, amadeus_access_token)
        # Check if our database has IATA code for
        # the city
        if (
            country_name in constants.COUNTRY_CITY_IATA_CODES
            and city_name in constants.COUNTRY_CITY_IATA_CODES
        ):
            country_iata = constants.COUNTRY_CITY_IATA_CODES[country_name]
            city_iata = constants.COUNTRY_CITY_IATA_CODES[city_name]
            print("IATA Codes:", country_iata, ",", city_iata)
            try:
                city_added = add_city(
                    db_session, country_iata, city_iata, amadeus_access_token
                )
                if city_added == constants.ADD_CITY_FAILED:
                    city_add_fail_msg(country_name, city_name)
                else:
                    city_add_success_msg(country_name, city_name)
                    cities_added += 1
            except:
                city_add_fail_msg(country_name, city_name)
        else:
            city_add_fail_msg(country_name, city_name)
            continue

    db_session.commit()

    return cities_added


# Adds the top_x most popular cities of all
# the countries in the DB
def add_cities_all_v2(db_session, cities_by_countries_data, popular_cities):
    from models.country import Country
    from models.city import City
    from utils import airportsdata
    from apis.utils import get_amadeus_api_access_token
    from database.populate import add_city
    from apis.covid import get_country_and_city_iata_code
    import random

    # Setup tools to populate db...
    airports = airportsdata.load("city")
    amadeus_access_token = get_amadeus_api_access_token()

    cities_added = []

    print("Querying Countries...")
    # Loop through Country table
    # to get country_name and capital_name
    countries = db_session.query(Country).all()
    print("Iterating Countries...")
    city_add_count = 0
    for country in countries:
        print("Iterating:", country.name, country.code)
        # TODO: Get a list of cities for
        # each country. For now, we just take the capital
        # of each city
        country_name = country.name
        country_code = country.code
        country_id = country.id
        # Check if our database has IATA code for
        # the city
        if country_code in cities_by_countries_data and country_name in popular_cities:
            # For now, just take second city
            # Pick a random city iata
            cities = popular_cities[country_name]
            for city_name in cities:
                city_iata = ""
                c_n = city_name.lower()
                if c_n in cities_by_countries_data[country_code]:
                    city_codes = cities_by_countries_data[country_code][c_n]
                    if len(city_codes) == 1:
                        city_iata = city_codes[0]
                else:
                    l = c_n.split(" ")
                    for n in l:
                        if n in cities_by_countries_data[country_code]:
                            city_codes = cities_by_countries_data[country_code][n]
                            if len(city_codes) == 1:
                                city_iata = city_codes[0]

                if city_iata == "":
                    print("CITY IATA NOT IN cities_by_countries_data")
                    city_add_fail_msg(country_name, city_iata)

                else:
                    city_add_count += 1
                    print(country_name + " " + c_n)
                    print(country_code + " " + city_iata)

                    if city_iata != "":
                        try:
                            city_added = add_city(
                                db_session,
                                country_code,
                                city_iata,
                                amadeus_access_token,
                                country_id,
                            )
                            if city_added == constants.ADD_CITY_FAILED:
                                city_add_fail_msg(country_name, city_iata)
                            else:
                                city_add_success_msg(country_name, city_iata)
                                cities_added.append(city_added)
                        except:
                            city_add_fail_msg(country_name, city_iata)

        else:
            print("COUNTRY NOT IN cities_by_countries_data")
            city_add_fail_msg(country_name, city_iata)
            continue
        print()

    # db_session.commit()
    print("City Add Count:", city_add_count)
    return cities_added


def city_add_success_msg(country_name, city_name):
    print(
        "CITY ADD SUCCESS:",
        country_name,
        " ",
        city_name,
    )


def city_add_fail_msg(country_name, city_name):
    print(
        "CITY ADD FAILED:",
        country_name,
        " ",
        city_name,
    )
