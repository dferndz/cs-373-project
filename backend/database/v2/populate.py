import pickle
from models.country import Country, Exchange_rates
from models.city import City
from models.flights import Flights
from database import db_session
from isodate import parse_duration
from apis.flights import make_flight_obj


static_data = {
    "exchange_rates": "exchange.pickle",
    "countries": "countries.pickle",
    "cities": "cities.pickle",
    "iata": "iata.pickle",
    "flights": "flights.pickle",
}


def get_pickle(resource):
    return pickle.load(open(static_data[resource], "rb"))


_reserved_keywords = [
    "metadata",
    "query",
    "registry" "id",
]


class cache:
    def __init__(self, f):
        self.f = f
        self.data = None

    def __call__(self):
        if self.data is None:
            self.data = self.f()
        return self.data


def make_obj(obj, obj_class, exclude=None):
    exclude = set() if exclude is None else set(exclude)

    new_obj = obj_class()
    fields = [
        f
        for f in dir(obj)
        if not f.startswith("_")
        and f not in _reserved_keywords
        and not "_ins" in f
        and f not in exclude
    ]
    [setattr(new_obj, f, getattr(obj, f)) for f in fields]
    return new_obj


def update_obj_from_dict(obj, d):
    fields = [
        f
        for f in d
        if not f.startswith("_")
        and f not in _reserved_keywords
        and not "_ins" in f
        and f != "id"
    ]
    [setattr(obj, f, d[f]) for f in fields]
    return obj


def make_obj_from_dict(obj, obj_class):
    new_obj = obj_class()
    fields = [
        f
        for f in obj
        if not f.startswith("_") and f not in _reserved_keywords and not "_ins" in f
    ]
    [setattr(new_obj, f, obj[f]) for f in fields]
    return new_obj


def commit_all(elements):
    [db_session.add(e) for e in elements]
    db_session.commit()


@cache
def get_iata_codes():
    return get_pickle("iata")


def populate_exchange_rates():
    data = get_pickle("exchange_rates")
    rates = [make_obj(d, Exchange_rates) for d in data]
    commit_all(rates)


def populate_countries():
    data = get_pickle("countries")
    countries = [make_obj(d, Country) for d in data]
    commit_all(countries)


def populate_flights():
    import dill

    data = dill.load(open("flights.pickle", "rb"))
    flights = [
        make_obj(d, Flights, exclude=["num_segments", "carrier_code"]) for d in data
    ]
    flights = [c for c in flights if c is not None]
    for f in flights:
        f.duration_min = parse_duration(f.duration).total_seconds() // 60
    commit_all(flights)


def make_city_country_map(city):
    iata = get_iata_codes()

    if city.iata not in iata:
        return None

    _, country_code, _, _ = iata[city.iata]

    country = Country.query.filter_by(code=country_code).first()

    if country is None:
        return None

    city.country = country.id

    return city


def populate_cities():
    data = get_pickle("cities")
    cities = [make_obj(d, City) for d in data]
    cities = [c for c in cities if c is not None]
    for c in cities:
        c.quarantine_dur = (
            float(c.quarantine_dur) if "None" not in c.quarantine_dur else 0
        )
    commit_all(cities)


def populate_db():
    populate_exchange_rates()
    populate_countries()
    populate_cities()
    populate_flights()
