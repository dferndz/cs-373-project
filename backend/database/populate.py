# Methods for entering data entries into the database.
# This would happen upon DB initialization,
# and we could later implement a scheduled refresh to keep the data up-to-date
# The data would come from our public apis (which are interfaced with by the scripts in the public_api directory)

from sqlalchemy import func
from utils import constants


def add_country(db_session, country_names):
    from apis.country import get_country_data_all
    from models.country import Country

    country_data = get_country_data_all(country_names)
    if "currency" in country_data:
        add_exchange_rates(db_session, country_data["currency"])

    new_id = next_id(db_session, Country)
    if "name" in country_data:
        populate_table(new_id, db_session, country_data, Country)

    return new_id


def add_exchange_rates(db_session, currency):
    from models.country import Exchange_rates
    from apis.other import get_exchange_rates

    desired_rates = Exchange_rates.__table__.columns.keys()[1:]

    entry = Exchange_rates.query.filter_by(currency=currency.lower()).first()
    # Don't add already-existing entries
    if entry == None:
        entry = get_exchange_rates(currency, desired_rates)
        entry = {key.lower(): entry[key] for key in entry}
        entry["currency"] = currency.lower()
        db_session.add(Exchange_rates(**entry))
        db_session.commit()


# TODO: Refactor to no longer do anything with id - the db will handle it automatically
def next_id(db_session, table_class):
    max_id = db_session.query(func.max(table_class.id)).scalar()  # Edge case, is empty
    if max_id == None:
        return 1
    return max_id + 1


def add_city(db_session, country_name, city_name, amadeus_access_token, country_id):
    """
    Adds a city instance to the city model in the Postgres Database.

    Parameters
    ----------

    db_session:
        current database session
    country_name: str
        name of the country of city
    city_name: str
        name of the city
    city_data: dict
        metadata and travel restriction data of the city

    """

    from apis.covid import construct_city_data_obj
    from apis.covid import get_city_travel_restriction_data
    from models.city import City

    travel_restriction_data = get_city_travel_restriction_data(
        country_name, city_name, amadeus_access_token
    )

    # Check if city travel restriction data not available
    if (
        travel_restriction_data == constants.FAILED_TO_GET_TRAVEL_RESTRICTION_DATA
        or constants.TRAVEL_DATA_INCORRECT_WARNING in travel_restriction_data
    ):
        return constants.ADD_CITY_FAILED

    city_covid_data = construct_city_data_obj(
        country_id, city_name, travel_restriction_data
    )
    # populate_table(next_id(db_session, City), db_session, city_covid_data, City)
    return city_covid_data


# Populates the table in the database with data.
# db_session = the FlaskAlchemy db_session object
# data = a map of key/value pairs where the key is a column in the table
# keys not in the table are not added
# table = the table class you want to add the data to
def populate_table(id, db_session, data, table_class):
    col_names = table_class.__table__.columns.keys()
    # values = {"id": id} # NOTE: The DB has been reconfigured to automatically assign id now
    values = {}
    for col_name in col_names:
        if col_name in data:
            values[col_name] = data[col_name]
    print(values)
    country_row = table_class(**values)
    db_session.add(country_row)
    # db_session.commit()  # This will be moved later eventually
