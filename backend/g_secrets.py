from google.cloud import secretmanager
import os


def get_secret(secret_id):
    client = secretmanager.SecretManagerServiceClient()
    project_id = os.environ.get("PROJECT_ID")
    response = client.access_secret_version(
        request={"name": f"projects/{project_id}/secrets/{secret_id}/versions/latest"}
    )
    return response.payload.data.decode("UTF-8")
