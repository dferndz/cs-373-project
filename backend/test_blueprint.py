import unittest

# import app
import requests as r

from flask import Flask
from controllers.controller import blueprint


HOST = "http://localhost:5000"

"""
Listed endpoints (received using `flask routes`):

Methods (12)              Rule
------------------------  ------------------------------------------------------------------
...
GET                       /countries
GET                       /admin/fill-db-all
GET                       /admin/fill-db-small
GET                       /cities/<city_id>
GET                       /countries/<country_id>/cities
GET                       /countries/<country_id>/flights
GET                       /flights/<flight_id>
GET                       /cities/<city_id>/flights
GET                       /countries/<country_id>
GET                       /cities
GET                       /flights
GET                       /admin/reset-db
...
"""


class TestEndpoints(unittest.TestCase):
    @classmethod
    def setUpClass(self):

        app = Flask(__name__)
        app.register_blueprint(blueprint, url_prefix="")
        self.app = app.test_client()

        self.page_attrs = {
            "data",
            "first",
            "last",
            "next",
            "page",
            "page_size",
            "prev",
            "total_pages",
            "total_rows",
        }

        # pseudo-schema of all attributes
        # feel free to change these as more attributes are added / removed
        self.country_attrs = {
            "air_quality_index",
            "continent",
            "currency",
            "flag_url",
            "id",
            "name",
            "pic_url",
            "temp_feels_like",
            "temp_max",
            "temp_min",
            "tourists",
        }

        self.city_attrs = {
            "cases",
            "country",
            "country_name",
            "deaths",
            "id",
            "name",
            "pic_url",
            "quarantine_dur",
            "risk_level",
        }

        self.flights_attrs = {
            "arrival_city",
            "arrival_city_name",
            "arrival_country_name",
            "cost_currency",
            "cost_total",
            "departure_city",
            "departure_city_name",
            "departure_country_name",
            "departure_time",
            "duration",
            "id",
            "segments",
        }

    def check_dict_vals(self, keys, dict1, dict2):
        for k in keys:
            if k not in dict1 or k not in dict2 or dict1[k] != dict2[k]:
                return False
        return True

    # ------- #
    # Country #
    # ------- #

    """
  /countries
  """

    def test_country_endpoint(self):
        """
        Test page layout of /countries response
        """
        response = self.app.get(f"{HOST}/countries")
        self.assertEqual(response.status_code, 200)
        response = response.get_json()

        # make sure page layout is consistent
        self.assertSetEqual(set(response), self.page_attrs)
        self.assertTrue(isinstance(response, dict))
        self.assertTrue(isinstance(response["data"], list))

        # make sure page size is consistent with data length
        self.assertLessEqual(len(response["data"]), response["page_size"])
        self.assertLessEqual(len(response["data"]), response["total_rows"])

        # test format of data if available
        if len(response["data"]) > 0:
            # assert that all keys are in data
            response_keys = set(response["data"][0])
            self.assertTrue(self.country_attrs.issubset(response_keys))

    def test_country_page_size(self):
        """
        Tests whether the pagination of the countries page is working correctly
        """
        response = self.app.get(f"{HOST}/countries?page_size=1")
        self.assertEqual(response.status_code, 200)
        response = response.get_json()

        # make sure page is only 1 long
        self.assertEqual(len(response["data"]), 1)
        self.assertEqual(response["page_size"], 1)

    """
  /countries/<id>
  """

    def test_country_id(self):
        """
        Tests consistency of /countries/<id> by checking against /countries
        """
        all_response = self.app.get(f"{HOST}/countries?page_size=1")
        id_response = self.app.get(f"{HOST}/countries/1")
        self.assertEqual(all_response.status_code, 200)
        self.assertEqual(id_response.status_code, 200)

        all_response = all_response.get_json()
        id_response = id_response.get_json()

        # check a subset of attributes
        for k in self.country_attrs:
            self.assertIn(k, all_response["data"][0])
            self.assertIn(k, id_response)
            self.assertTrue(all_response["data"][0][k] == id_response[k])

    # ------ #
    # Cities #
    # ------ #

    """
  /cities
  """

    def test_cities_endpoint(self):
        """
        Test page layout of /cities response
        """
        response = self.app.get(f"{HOST}/cities")
        self.assertEqual(response.status_code, 200)
        response = response.get_json()

        # make sure page layout is consistent
        self.assertSetEqual(set(response), self.page_attrs)
        self.assertTrue(isinstance(response, dict))
        self.assertTrue(isinstance(response["data"], list))

        # make sure page size is consistent with data length
        self.assertLessEqual(len(response["data"]), response["page_size"])
        self.assertLessEqual(len(response["data"]), response["total_rows"])

        # test format of data if available
        if len(response["data"]) > 0:
            # assert that all keys are in data
            response_keys = set(response["data"][0])
            self.assertTrue(self.city_attrs.issubset(response_keys))

    def test_city_page_size(self):
        """
        Tests whether the pagination of the cities page is working correctly
        """
        response = self.app.get(f"{HOST}/cities?page_size=1")
        self.assertEqual(response.status_code, 200)
        response = response.get_json()

        # make sure page is only 1 long
        self.assertEqual(len(response["data"]), 1)
        self.assertEqual(response["page_size"], 1)

    """
  /cities/<id>
  """

    def test_cities_id(self):
        """
        Tests consistency of /cities/<id> by checking against /cities
        """
        all_response = self.app.get(f"{HOST}/cities?page_size=1&sort=id")
        id_response = self.app.get(f"{HOST}/cities/1")
        self.assertEqual(all_response.status_code, 200)
        self.assertEqual(id_response.status_code, 200)

        all_response = all_response.get_json()
        id_response = id_response.get_json()

        # check a subset of attributes
        for k in self.city_attrs:
            self.assertIn(k, all_response["data"][0])
            self.assertIn(k, id_response)
            self.assertTrue(all_response["data"][0][k] == id_response[k])

    # ------- #
    # Flights #
    # ------- #

    """
  /flights
  """

    def test_flights_endpoint(self):
        """
        Test page layout of /flights response
        """
        response = self.app.get(f"{HOST}/flights")
        self.assertEqual(response.status_code, 200)
        response = response.get_json()

        # make sure page layout is consistent
        self.assertSetEqual(set(response), self.page_attrs)
        self.assertTrue(isinstance(response, dict))
        self.assertTrue(isinstance(response["data"], list))

        # make sure page size is consistent with data length
        self.assertLessEqual(len(response["data"]), response["page_size"])
        self.assertLessEqual(len(response["data"]), response["total_rows"])

        # test format of data if available
        if len(response["data"]) > 0:
            # assert that all keys are in data
            response_keys = set(response["data"][0])
            self.assertTrue(self.flights_attrs.issubset(response_keys))

    def test_flights_page_size(self):
        """
        Tests whether the pagination of the flights page is working correctly
        """
        response = self.app.get(f"{HOST}/flights?page_size=1")
        self.assertEqual(response.status_code, 200)
        response = response.get_json()

        # make sure page is only 1 long
        self.assertEqual(len(response["data"]), 1)
        self.assertEqual(response["page_size"], 1)

    """
  /flights/<id>
  """

    def test_flight_id(self):
        """
        Tests consistency of /flights/<id> by checking against /flights
        """
        all_response = self.app.get(f"{HOST}/flights?page_size=1&sort=id")
        id_response = self.app.get(f"{HOST}/flights/1")

        self.assertEqual(all_response.status_code, 200)
        self.assertEqual(id_response.status_code, 200)

        all_response = all_response.get_json()
        id_response = id_response.get_json()

        # check a subset of all flight attributes
        for k in self.flights_attrs:
            self.assertIn(k, all_response["data"][0])
            self.assertIn(k, id_response)
            self.assertTrue(all_response["data"][0][k] == id_response[k])

    # ---------- #
    # Relational #
    # ---------- #

    """
  /countries/<country_id>/cities
  """

    def test_cities_in_country(self):
        """
        Check the consistency of data between /countries/<id>/cities, /cities/<id>, and country id
        """
        ID = 1
        nested_res = self.app.get(f"{HOST}/countries/{ID}/cities")
        self.assertTrue(nested_res.status_code, 200)
        nested_res = nested_res.get_json()

        # should be > 0, will return gracefully if not filled
        if len(nested_res) == 0:
            return

        nested_res = nested_res["data"][0]

        # get the response from the /cities/<id> endpoint
        id_res = self.app.get(f"{HOST}/cities/{nested_res['id']}")
        self.assertTrue(id_res.status_code, 200)
        id_res = id_res.get_json()

        # check to make sure all values are equal
        for k in self.city_attrs:
            self.assertIn(k, id_res)
            self.assertIn(k, nested_res)
            self.assertTrue(id_res[k] == nested_res[k])

        # country id should be consistent to original request
        self.assertTrue(id_res["country"], ID)

    """
  /countries/<country_id>/flights
  """

    def test_flights_to_country(self):
        """
        Check the consistency of data between /countries/<id>/flights, /flights/<id>, and country id
        """
        ID = 2
        nested_res = self.app.get(f"{HOST}/countries/{ID}/flights")
        self.assertTrue(nested_res.status_code, 200)
        nested_res = nested_res.get_json()

        # should be > 0, will return gracefully if not filled
        if len(nested_res) == 0:
            return

        nested_res = nested_res["data"][0]

        # get the response from the /flights/<id> endpoint
        id_res = self.app.get(f"{HOST}/flights/{nested_res['id']}")
        self.assertTrue(id_res.status_code, 200)
        id_res = id_res.get_json()

        # check to make sure all values are equal
        for k in self.flights_attrs:
            self.assertIn(k, id_res)
            self.assertIn(k, nested_res)
            self.assertTrue(id_res[k] == nested_res[k])

        # country id should be consistent to original request
        self.assertTrue(id_res["departure_country"], ID)

    """
  /cities/<id>/flights
  """

    def test_cities_to_flights(self):
        """
        Check the consistency of data between /cities/<id>/flights, /flights/<id>, and city id
        """
        ID = 1
        nested_res = self.app.get(f"{HOST}/cities/{ID}/flights")
        self.assertTrue(nested_res.status_code, 200)
        nested_res = nested_res.get_json()

        # should be > 0, will return gracefully if not filled
        if len(nested_res) == 0:
            return

        nested_res = nested_res["data"][0]

        # get the response from the /cities/<id> endpoint
        id_res = self.app.get(f"{HOST}/flights/{nested_res['id']}")
        self.assertTrue(id_res.status_code, 200)
        id_res = id_res.get_json()

        # check to make sure all values are equal
        for k in self.flights_attrs:
            self.assertIn(k, id_res)
            self.assertIn(k, nested_res)
            self.assertTrue(id_res[k] == nested_res[k])

        # country id should be consistent to original request

        self.assertTrue(id_res["departure_city"], ID)

    # ----- #
    # other #
    # ----- #

    def test_nonexistent_endpoint(self):
        response = self.app.get(f"{HOST}/nonexistent-endpoint/does-not-exist")
        self.assertEqual(response.status_code, 404)  # should not exist


if __name__ == "__main__":
    print("unit test main")
    unittest.main()
