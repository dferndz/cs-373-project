from sqlalchemy import Column, Integer, String, Float, ForeignKey, BigInteger
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import func
from database import Base
from sqlalchemy.ext.hybrid import hybrid_property
from models.city import City
from sqlalchemy_json import mutable_json_type
from sqlalchemy.dialects.postgresql import JSONB


class Flights(Base):
    __tablename__ = "flights"
    id = Column(Integer, primary_key=True)
    departure_city = Column(
        Integer, ForeignKey("cities.id"), unique=False, nullable=True
    )
    departure_time = Column(String(50), unique=False, nullable=True)
    arrival_city = Column(Integer, ForeignKey("cities.id"), unique=False, nullable=True)
    arrival_time = Column(String(50), unique=False, nullable=True)
    duration = Column(String(50), unique=False, nullable=True)
    cost_currency = Column(String(50), unique=False, nullable=True)
    cost_total = Column(Float, unique=False, nullable=True)
    service_class = Column(String(50), unique=False, nullable=True)
    segments = Column(mutable_json_type(dbtype=JSONB, nested=True))
    departure_city_ins = relationship("City", foreign_keys=[departure_city])
    arrival_city_ins = relationship("City", foreign_keys=[arrival_city])
    duration_min = Column(BigInteger())

    @hybrid_property
    def carrier_code(self):
        return self.segments[0]["carrier_code"]

    @carrier_code.expression
    def carrier_code(cls):
        return cls.segments[0]["carrier_code"].astext

    @hybrid_property
    def num_segments(self):
        return len(self.segments)

    @num_segments.expression
    def num_segments(cls):
        return func.jsonb_array_length(Flights.segments)


class Flight_Segments(Base):
    __tablename__ = "flight_segments"
    id = Column(Integer, primary_key=True)
    id_flight = Column(Integer, ForeignKey("flights.id"), unique=False, nullable=True)
    departure_iata = Column(String(50), unique=False, nullable=True)
    departure_city_name = Column(String(50), unique=False, nullable=True)
    departure_lat = Column(Float, unique=False, nullable=True)
    departure_lon = Column(Float, unique=False, nullable=True)
    departure_time = Column(String(50), unique=False, nullable=True)
    arrival_iata = Column(String(50), unique=False, nullable=True)
    arrival_city_name = Column(String(50), unique=False, nullable=True)
    arrival_lat = Column(Float, unique=False, nullable=True)
    arrival_lon = Column(Float, unique=False, nullable=True)
    carrier_code = Column(String(50), unique=False, nullable=True)
    flight_num = Column(Integer, unique=False, nullable=True)
    aircraft_code = Column(String(50), unique=False, nullable=True)
    oper_carrier_code = Column(String(50), unique=False, nullable=True)
    oper_carrier_name = Column(String(50), unique=False, nullable=True)
    oper_carrier_logo = Column(String(200), unique=False, nullable=True)


# UPDATE - we wont need this I don't think since it's a 1 to many mapping
# # Another association table
# # Check out the country_languages table in country.py for more detilas
# class Flight_To_Segments(Base):
#     __tablename__ = "flight_to_segment"
#     id                 = Column(Integer, primary_key=True)
#     flight_id          = Column(Integer,unique=False, nullable=True)
#     segment_id         = Column(Integer,unique=False, nullable=True)
