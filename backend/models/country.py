from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.orm import relationship
from database import Base


class Country(Base):
    __tablename__ = "countries"
    id = Column(Integer, primary_key=True)
    name = Column(
        String(200), unique=False, nullable=True
    )  # Probably want to make unique=True
    code = Column(String(8), unique=True)
    continent = Column(String(200), unique=False, nullable=True)
    subregion = Column(String(200), unique=False, nullable=True)
    air_quality_index = Column(Integer, unique=False, nullable=True)
    flag_url = Column(String(200), unique=False, nullable=True)
    pic_url = Column(String(200), unique=False, nullable=True)
    capital = Column(String(200), unique=False, nullable=True)
    population = Column(Integer, unique=False, nullable=True)
    num_vaccinated = Column(Integer, unique=False, nullable=True)
    tourists = Column(Integer, unique=False, nullable=True)
    currency = Column(String(200), unique=False, nullable=True)
    languages = Column(String(200), unique=False, nullable=True)
    temp = Column(Integer, unique=False, nullable=True)
    temp_min = Column(Integer, unique=False, nullable=True)
    temp_max = Column(Integer, unique=False, nullable=True)
    temp_feels_like = Column(Integer, unique=False, nullable=True)
    description = Column(String(1500), unique=False, nullable=True)
    lat = Column(Float, unique=False, nullable=True)
    lon = Column(Float, unique=False, nullable=True)

    cities_ins = relationship("City")


# Mapping: Country.currency = Exchange_rate.currency
class Exchange_rates(Base):
    __tablename__ = "exchange_rates"
    currency = Column(String(10), primary_key=True)
    usd = Column(Float, unique=False, nullable=True)
    eur = Column(Float, unique=False, nullable=True)
    cad = Column(Float, unique=False, nullable=True)
    cny = Column(Float, unique=False, nullable=True)


from models.city import City
