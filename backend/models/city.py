from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import relationship
from models.country import Country
from database import Base


class City(Base):
    __tablename__ = "cities"
    id = Column(Integer, primary_key=True)
    iata = Column(String(8), unique=True)
    name = Column(String(50), unique=False, nullable=True)
    country = Column(Integer, ForeignKey("countries.id"), unique=False, nullable=True)
    pic_url = Column(String(200), unique=False, nullable=True)
    population = Column(Integer, unique=False, nullable=True)
    temp = Column(Integer, unique=False, nullable=True)
    lat = Column(Float, unique=False, nullable=True)
    lon = Column(Float, unique=False, nullable=True)
    risk_level = Column(String(200), unique=False, nullable=True)
    deaths = Column(Integer, unique=False, nullable=True)
    cases = Column(Integer, unique=False, nullable=True)
    mask_policy = Column(String(200), unique=False, nullable=True)
    vaccine_policy = Column(String(200), unique=False, nullable=True)
    quarantine_dur = Column(Integer(), unique=False, nullable=True)
    restriction_summary = Column(String(1500), unique=False, nullable=True)

    country_ins = relationship("Country", back_populates="cities_ins")
