# Includes requests for getting data from our public APIs related to the country model
# Basic outlines for these API requests is already outlined in our Postman collections
# I believe python has a requests package that would work well for this.
# If so we would need to add the package to the requirements.txt


import requests
from apis.static_data import vaccine_data

# Returns a dict containing all country data from our public APIs
# Country names is a list of country name type (i.e: name, code) and tries different ones if one fails
def get_country_data_all(country_names):
    country_data = {}
    func_list = [
        get_RestCountry_data,
        get_Ninja_country_data,
        get_Ninja_air_data,
        get_Ninja_weather_data,
        get_country_pic,
        get_wiki_description,
        get_Hopkins_vaccine_data,
    ]

    for func in func_list:
        country_get_helper(func, country_data, country_names)
    return country_data


def country_get_helper(func, country_data, vals):
    for val in vals:
        try:
            func(val, country_data)
            return
        except:
            print("WARNING: " + func.__name__ + " failed with value: " + val)


def country_get_helper(func, country_data, vals):
    for val in vals:
        try:
            func(val, country_data)
            return
        except:
            print("WARNING: " + func.__name__ + " failed with value: " + val)


# Adds data to country_data from RestCountries
def get_RestCountry_data(country, country_data):
    # Get the response
    url = "https://restcountries.com/v2/name/" + country + "?fullText=true"
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code != 200:
        print("WARNING: Request to RestCountries/name failed")
    response = response.json()[0]

    # Store desired values
    country_data["name"] = response["name"]
    country_data["continent"] = response["region"]
    country_data["subregion"] = response["subregion"]
    country_data["flag_url"] = response["flags"]["png"]
    country_data["capital"] = response["capital"]
    country_data["population"] = response["population"]
    country_data["code"] = response["alpha2Code"]
    country_data["currency"] = response["currencies"][0]["code"]
    country_data["languages"] = ",".join(
        [language["name"] for language in response["languages"]]
    )
    # SPECIAL CASE: store the list of strings as 1 comma-separated string
    country_data["lat"] = response["latlng"][0]
    country_data["lon"] = response["latlng"][1]


# Adds data to country_data from NinjaAPI
def get_Ninja_country_data(country, country_data):
    url = "https://api.api-ninjas.com/v1/country?name=" + country
    payload = {}
    headers = {"X-Api-Key": "/daEioeJCpUiTYeLPBn7GA==PRcjftZpwRm96R9t"}
    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code != 200:
        print("WARNING: Request to Ninja/country failed")

    response = response.json()[0]
    country_data["tourists"] = response["tourists"]


# Adds data to country_data (air quality stats) from NinjaAPI
def get_Ninja_air_data(country, country_data):
    url = "https://api.api-ninjas.com/v1/airquality?city=" + country
    payload = {}
    headers = {"X-Api-Key": "/daEioeJCpUiTYeLPBn7GA==PRcjftZpwRm96R9t"}
    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code != 200:
        print("WARNING: Request to Ninja/airquality failed")

    response = response.json()
    country_data["air_quality_index"] = response["overall_aqi"]


# Adds data to country_data (weather/temp info) from NinjaAPI
def get_Ninja_weather_data(country, country_data):
    url = "https://api.api-ninjas.com/v1/weather?city=" + country
    payload = {}
    headers = {"X-Api-Key": "/daEioeJCpUiTYeLPBn7GA==PRcjftZpwRm96R9t"}
    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code != 200:
        print("WARNING: Request to Ninja/weather failed")

    response = response.json()
    country_data["temp"] = response["temp"]
    country_data["temp_min"] = response["min_temp"]
    country_data["temp_max"] = response["max_temp"]
    country_data["temp_feels_like"] = response["feels_like"]


def get_wiki_description(country, country_data):
    url = "https://en.wikipedia.org/api/rest_v1/page/summary/" + country
    response = requests.request("GET", url)
    if response.status_code != 200:
        print("WARNING: Request to Wiki/summary failed")

    response = response.json()
    # NOTE: extract_html includes bolding and stuff. Just use "extract" if you don't want that
    country_data["description"] = response["extract_html"]


def get_country_pic(country, country_data):
    from apis.other import get_plexels_pic

    country_data["pic_url"] = get_plexels_pic(country)


# Adds data to country_data (vaccine count) from John Hopkins' data (imported from static data)
def get_Hopkins_vaccine_data(country, country_data):
    country_data["num_vaccinated"] = vaccine_data[country.upper()]
