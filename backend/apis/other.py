# This file is for any other requests that may not be related to any other model,
# Static data that will be pulled once and referenced many times is in the data directory

import requests

# Should return the exchange rate from [curr_id_from] to each currency in curr_ids_to
# Returns a map of key=curr_id_to:val=exchange_rate
# NOTE: Can only get conversion to euro se we have to do some extra math
def get_exchange_rates(curr_id_from, curr_ids_to):
    from apis.static_data import euro_rates

    # Get the conversion from [curency] to euro
    url = (
        "http://api.exchangeratesapi.io/v1/latest?access_key=bbf116e58e95f7e9f674d950624c2410&symbols="
        + curr_id_from
    )
    payload = headers = {}
    try:
        rate = requests.request("GET", url, headers=headers, data=payload).json()[
            "rates"
        ]
        keys = list(rate.keys())
        assert len(keys) == 1
    except:
        print("Call to exchange rates api failed for: " + curr_id_from)
        return []

    val_for_1_euro = rate[keys[0]]

    # Do math and get exchange rates for all desired currencies
    # We already got their exchange to euro rates in ./data
    rates = {}
    for currency in euro_rates:
        val = val_for_1_euro
        if currency != "eur":
            val = euro_rates[currency] / val_for_1_euro
        rates[currency] = val
    return rates


# Returns a url to the first result of an image search for [search_query]
# The url contains 'l' and 'w' params that can be modified to set the dimensions of the pic
def get_plexels_pic(search_query):
    url = (
        "https://api.pexels.com/v1/search?query="
        + search_query
        + "&per_page=1&orientation=landscape"
    )
    payload = {}
    headers = {
        "Authorization": "563492ad6f917000010000010e5001bd930b4b658cf2a593042185c3"
    }
    try:
        response = requests.request("GET", url, headers=headers, data=payload).json()
        return response["photos"][0]["src"]["landscape"]
    except:
        print("plexels search failed for: " + search_query)


# Returns a url to the first result of an image search for [search_query]
# The url contains 'l' and 'w' params that can be modified to set the dimensions of the pic
def get_plexels_pic(search_query):
    url = (
        "https://api.pexels.com/v1/search?query="
        + search_query
        + "&per_page=1&orientation=landscape"
    )
    payload = {}
    headers = {
        "Authorization": "563492ad6f917000010000010e5001bd930b4b658cf2a593042185c3"
    }
    response = requests.request("GET", url, headers=headers, data=payload).json()
    return response["photos"][0]["src"]["landscape"]
