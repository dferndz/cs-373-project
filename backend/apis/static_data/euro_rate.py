# Get the conversion from euro to our desired countries
def get_eur_exchange_rates():
    import requests
    from models.country import Exchange_rates

    desired_rates = Exchange_rates.__table__.columns.keys()[1:]
    url = "http://api.exchangeratesapi.io/v1/latest?access_key=f692beb72a9743ac8db6f75dd273ff4c&symbols="
    url += ",".join(desired_rates)
    payload = headers = {}
    response = requests.request("GET", url, headers=headers, data=payload).json()
    return response["rates"]
