# Acquire a copy of the covid vaccine csv data and put it into a simplified map
def get_vaccination_data():
    import requests

    url = "https://raw.githubusercontent.com/govex/COVID-19/master/data_tables/vaccine_data/global_data/vaccine_data_global.csv"
    vaccineTable = []
    for line in requests.get(url).text.splitlines():
        vaccineTable.append(line.split(","))
    vaccineMap = {data[1].upper(): data[5] for data in vaccineTable if data[0] == ""}
    return vaccineMap
