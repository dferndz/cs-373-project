from . import vaccines, euro_rate

# Vaccine rates per country:  key=country_name:value=fully_vaccinated_individuals_count
vaccine_data = vaccines.get_vaccination_data()

# Exchange to euro per desired rate: key=currency:value=rate_for_1_euro
euro_rates = euro_rate.get_eur_exchange_rates()
