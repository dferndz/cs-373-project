# -*- coding: utf-8 -*-
# Gets flight information between cities
# including itinerary, flight prices, etc..

from apis import utils
import pickle
from apis import constants
import requests
from models.city import City
from models.flights import Flights, Flight_Segments
import json

flight_itenararies_data = {
    "type": "flight-offer",
    "id": "1",
    "source": "GDS",
    "instantTicketingRequired": False,
    "nonHomogeneous": False,
    "oneWay": False,
    "lastTicketingDate": "2022-03-10",
    "numberOfBookableSeats": 4,
    "itineraries": [
        {
            "duration": "PT16H10M",
            "segments": [
                {
                    "departure": {
                        "iataCode": "IAH",
                        "terminal": "C",
                        "at": "2022-03-15T18:40:00",
                    },
                    "arrival": {
                        "iataCode": "EWR",
                        "terminal": "C",
                        "at": "2022-03-15T23:04:00",
                    },
                    "carrierCode": "TP",
                    "number": "8529",
                    "aircraft": {"code": "739"},
                    "operating": {"carrierCode": "UA"},
                    "duration": "PT3H24M",
                    "id": "87",
                    "numberOfStops": 0,
                    "blacklistedInEU": False,
                },
                {
                    "departure": {
                        "iataCode": "EWR",
                        "terminal": "B",
                        "at": "2022-03-16T00:40:00",
                    },
                    "arrival": {
                        "iataCode": "LIS",
                        "terminal": "1",
                        "at": "2022-03-16T11:55:00",
                    },
                    "carrierCode": "TP",
                    "number": "204",
                    "aircraft": {"code": "339"},
                    "operating": {"carrierCode": "TP"},
                    "duration": "PT7H15M",
                    "id": "88",
                    "numberOfStops": 0,
                    "blacklistedInEU": False,
                },
                {
                    "departure": {
                        "iataCode": "LIS",
                        "terminal": "1",
                        "at": "2022-03-16T13:20:00",
                    },
                    "arrival": {
                        "iataCode": "ORY",
                        "terminal": "3",
                        "at": "2022-03-16T16:50:00",
                    },
                    "carrierCode": "TP",
                    "number": "432",
                    "aircraft": {"code": "319"},
                    "operating": {"carrierCode": "TP"},
                    "duration": "PT2H30M",
                    "id": "89",
                    "numberOfStops": 0,
                    "blacklistedInEU": False,
                },
            ],
        }
    ],
    "price": {
        "currency": "EUR",
        "total": "512.58",
        "base": "357.00",
        "fees": [
            {"amount": "0.00", "type": "SUPPLIER"},
            {"amount": "0.00", "type": "TICKETING"},
        ],
        "grandTotal": "512.58",
    },
    "pricingOptions": {"fareType": ["PUBLISHED"], "includedCheckedBagsOnly": False},
    "validatingAirlineCodes": ["TP"],
    "travelerPricings": [
        {
            "travelerId": "1",
            "fareOption": "STANDARD",
            "travelerType": "ADULT",
            "price": {"currency": "EUR", "total": "512.58", "base": "357.00"},
            "fareDetailsBySegment": [
                {
                    "segmentId": "87",
                    "cabin": "ECONOMY",
                    "fareBasis": "LUSDSI0E",
                    "class": "L",
                    "includedCheckedBags": {"quantity": 0},
                },
                {
                    "segmentId": "88",
                    "cabin": "ECONOMY",
                    "fareBasis": "LUSDSI0E",
                    "class": "L",
                    "includedCheckedBags": {"quantity": 0},
                },
                {
                    "segmentId": "89",
                    "cabin": "ECONOMY",
                    "fareBasis": "LUSDSI0E",
                    "brandedFare": "DISCOUNT",
                    "class": "L",
                    "includedCheckedBags": {"quantity": 0},
                },
            ],
        }
    ],
}


def get_iata(city, access_token):
    url = constants.AMADEUS_LOCATION_SEARCH_URL.format(city=city)
    headers = {
        "Authorization": "Bearer {access_token}".format(access_token=access_token)
    }
    payload = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    response = response.json()["data"][0]

    return response["iataCode"]


def load_city_stuff():
    response = requests.request(
        "GET",
        "https://airlabs.co/api/v9/cities?api_key=7173b7d3-a3d0-4666-a64b-96f8cf541d71",
    )
    response = response.json()["response"]
    return response


def load_airport_stuff():
    response = requests.request(
        "GET",
        "https://airlabs.co/api/v9/airports?api_key=7173b7d3-a3d0-4666-a64b-96f8cf541d71",
    )
    response = response.json()["response"]
    return response


def load_airline_stuff():
    response = requests.request(
        "GET",
        "https://airlabs.co/api/v9/airlines?api_key=7173b7d3-a3d0-4666-a64b-96f8cf541d71",
    )
    response = response.json()["response"]
    return response


def get_flight_travel_data(origin_city, destination_city, departure_date, access_token):
    """
        Gets the flights information between passed origin city
        and destination city from the Amadeus API

    <<<<<<< HEAD
    =======
        Parameters
        ----------

    >>>>>>> d0312df04fe664a93d36fe04f095198d0fdcaa36
        origin_city: str
            IATA metropolitan code of the city to fly form
        destination_city: str
            IATA metropolitan code of the city to fly to
        departure_date: str
            date of flight departure in format: YYYY-MM-DD
        access_token: str
            Amadeus API access token

        Returns
        --------

        dict
         a dict response from the Amadeus travel API
    """
    url = constants.AMADEUS_FLIGHT_OFFER_ENDPOINT_URL.format(
        origin_location_code=origin_city,
        destination_location_code=destination_city,
        departure_date=departure_date,
    )
    headers = {
        "Authorization": "Bearer {access_token}".format(access_token=access_token)
    }

    payload = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    response = response.json()["data"]

    return response


iata = pickle.load(open("iata.pickle", "rb"))
airlines = pickle.load(open("airlines.pickle", "rb"))
airports = pickle.load(open("airports.pickle", "rb"))


def make_segments(segments):
    vals = []

    for seg in segments:
        d_iata = seg["departure"]["iataCode"]
        a_iata = seg["arrival"]["iataCode"]
        d_name, d_country, d_lat, d_lng = airports[d_iata]
        a_name, a_country, a_lat, a_lng = airports[a_iata]

        airline_code = seg["carrierCode"]

        if airline_code not in airlines:
            if len(airline_code) == 3:
                airline_code[2] = "*"
                if airline_code not in airlines:
                    continue
            elif len(airline_code) == 2:
                airline_code += "*"
                if airline_code not in airlines:
                    continue
            else:
                continue

        airline_name = airlines[airline_code]

        v = {
            "departure_iata": d_iata,
            "arrival_iata": a_iata,
            "departure_name": d_name,
            "arrival_name": a_name,
            "departure_time": seg["departure"]["at"],
            "arrival_time": seg["arrival"]["at"],
            "duration": seg["duration"],
            "departure_lat": d_lat,
            "departure_lng": d_lng,
            "arrival_lat": a_lat,
            "arrival_lng": a_lng,
            "carrier_code": seg["carrierCode"],
            "flight_num": seg["number"],
            "aircraft_code": seg["aircraft"]["code"],
            "oper_carrier_code": airline_code,
            "oper_carrier_name": airline_name,
            "oper_carrier_logo": f"https://daisycon.io/images/airline/?width=300&height=150&color=ffffff&iata={airline_code}",
        }
        vals.append(v)
    return vals


def make_flight_obj(data):
    segments = data["itineraries"][0]["segments"]
    first_seg = segments[0]
    last_seg = segments[len(segments) - 1]

    origin = data["departure_city_iata"]
    destination = data["arrival_city_iata"]

    origin = City.query.filter_by(iata=origin).first()
    destination = City.query.filter_by(iata=destination).first()

    if origin is None or destination is None:
        return None

    f = Flights()
    f.departure_city = origin.id
    f.arrival_city = destination.id
    f.departure_time = first_seg["departure"]["at"]
    f.arrival_time = last_seg["arrival"]["at"]
    f.duration = data["itineraries"][0]["duration"]
    f.cost_currency = data["travelerPricings"][0]["price"]["currency"]
    f.cost_total = data["travelerPricings"][0]["price"]["total"]
    f.service_class = data["travelerPricings"][0]["fareDetailsBySegment"][0]["cabin"]
    f.segments = make_segments(data["itineraries"][0]["segments"])
    return f


def get_flights(flights, cities):
    import random

    api_key = "5FCeySWtURoGG39XTX4Jmebv5P6L"

    def get_date():
        a = str(random.randint(5, 12))
        b = str(random.randint(1, 28))
        if len(a) < 2:
            a = f"0{a}"
        if len(b) < 2:
            b = f"0{b}"
        return f"2022-{a}-{b}"

    for c1 in cities:
        for i in range(10):
            c2 = c1
            while c2.id == c1.id:
                c2 = random.choice(cities)
            try:
                f = get_flight_travel_data(c1.iata, c2.iata, get_date(), api_key)
                for i in f:
                    i["departure_city_iata"] = c1.iata
                    i["arrival_city_iata"] = c2.iata
                flights.append(f)
                print(f"got {c1.iata} -> {c2.iata}")
            except Exception:
                print(f"failed {c1.iata} -> {c2.iata}")
                continue
