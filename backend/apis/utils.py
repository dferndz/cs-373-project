# Contains util functions for
# the public APIs

from apis import constants
import requests

AMADEUS_ACCESS_TOKEN_ENDPOINT_URL = (
    "https://test.api.amadeus.com/v1/security/oauth2/token"
)
AMADEUS_API_GRANT_TYPE = "client_credentials"
AMADEUS_API_KEY = "bAMuaQAFbz30MXOGUSGdrtjuceURG9BF"
AMADEUS_API_SECRET = "gI5ATyucSaADfUJj"

AMADEUS_FLIGHT_OFFER_ENDPOINT_URL = "https://test.api.amadeus.com/v2/shopping/flight-offers?originLocationCode={origin_location_code}&destinationLocationCode={destination_location_code}&departureDate={departure_date}&adults=1&nonStop=false&max=250"


# Gets an access token to use the
# Amadeus public API for city and flight
# data

#
# {
#     "type": "amadeusOAuth2Token",
#     "username": "saranjith.chockan@gmail.com",
#     "application_name": "SafeTravels",
#     "client_id": "OlVGXpjdP8aO44asOGp35wxpmAnQH5iw",
#     "token_type": "Bearer",
#     "access_token": "G0EcdQisIxEBDFitBI14WwsBusbK",
#     "expires_in": 1799,
#     "state": "approved",
#     "scope": "",
# }
def get_amadeus_api_access_token():
    """
    Gets a generated access token
    to use the Amadeus Travel Restrictions API.

    Returns
    -------
    dict
        a dict of information about the generated access token
        {
            "type": "amadeusOAuth2Token",
            "username": "saranjith.chockan@gmail.com",
            "application_name": "SafeTravels",
            "client_id": "OlVGXpjdP8aO44asOGp35wxpmAnQH5iw",
            "token_type": "Bearer",
            "access_token": "G0EcdQisIxEBDFitBI14WwsBusbK",
            "expires_in": 1799,
            "state": "approved",
            "scope": "",
        }
    """
    headers = {"Content-type": "application/x-www-form-urlencoded"}
    data = {
        "grant_type": AMADEUS_API_GRANT_TYPE,
        "client_id": AMADEUS_API_KEY,
        "client_secret": AMADEUS_API_SECRET,
    }
    response = requests.request(
        "POST", AMADEUS_ACCESS_TOKEN_ENDPOINT_URL, headers=headers, data=data
    )

    return response.json()["access_token"]
