import click
from flask import Flask, jsonify
from flask_restless import APIManager
from database import db_session, init_db
from database import clear_db
from database.initialize import add_countries_small as acs, add_countries_all as aca
from models.country import *
from controllers.controller import blueprint
from controllers.exceptions import APIException
from flask_cors import CORS
from database.v2.populate import populate_db

# TODO: create api methods for city and flights

init_db()

app = Flask(__name__)
app.register_blueprint(blueprint)
CORS(app)

# These two lines may be obsolete since we are writing the responses by hand now
api_manager = APIManager(app, session=db_session)
api_manager.create_api(Country, methods=["GET", "POST", "PATCH"])


def get_context():
    return app


@app.cli.command("reset-db")
def reset_db():
    print("Resetting Database...")
    clear_db()
    print("DB Reset!")


@app.cli.command("fill-db-small")
def fill_db_small():
    countries_added = acs(db_session)
    print("Added countries: " + countries_added)


@app.cli.command("fill-db-all")
@click.option("--n_countries", default=3, type=int)
def fill_db_all(n_countries):
    populate_db()


@app.cli.command("add-city-all")
def add_city_all():
    from database.initialize import add_cities_all_v2 as populate_add_cities_all
    import pickle

    # cities_db = pickle.load(open("cities_populate_v2_3.pickle", "rb"))
    city_map_data = pickle.load(open("city_map.pickle", "rb"))
    popular_cities = pickle.load(open("popular_cities_of_countries.pickle", "rb"))

    cities_added = populate_add_cities_all(db_session, city_map_data, popular_cities)

    cities_added = cities_added

    pickle.dump(cities_added, open("cities_populate_v3.pickle", "wb"))
    print("Cities Added")
    print(cities_added)


@app.cli.command("convert-cities-data-to-pickle-obj")
def convert_cities_data_to_pickle_obj():
    import pickle
    from models.city import City

    cities = City.query.all()
    pickle.dump(cities, open("cities.pickle", "wb"))


@app.cli.command("get-popular-cities")
def get_popular_cities_of_each_country():

    popular_cities_of_countries = {}

    import pickle
    from models.city import Country
    import re

    countries = Country.query.all()

    for country in countries:
        # TODO: Iterate through countries db
        country_name = country.name
        import requests

        url = "https://countriesnow.space/api/v0.1/countries/population/cities/filter"
        headers = {}
        payload = {
            "limit": 3,
            "order": "dsc",
            "orderBy": "population",
            "country": country_name,
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        response = response.json()
        cities = []
        if "data" in response:
            for city in response["data"]:
                city_name = re.sub("[\(\[].*?[\)\]]", "", city["city"])
                city_name = city_name.rstrip()
                cities.append(city_name)
            popular_cities_of_countries[country_name] = cities

    pickle.dump(
        popular_cities_of_countries, open("popular_cities_of_countries.pickle", "wb")
    )


@app.cli.command("get-cities-already-added")
def get_cities_already_added():
    import pickle

    d = set()
    from models.city import City

    cities = db_session.query(City).all()
    for city in cities:
        d.add(city.iata)

    pickle.dump(d, open("cities_already_added.pickle", "wb"))

    pass


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.errorhandler(APIException)
def handle_exception(exception):
    res = jsonify(exception.to_dict())
    res.status_code = exception.status_code
    return res
