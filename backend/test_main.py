#!/usr/bin/env python3

# --------
# tests.py
# --------

# -------------------------
# Entry point for all tests
# -------------------------

import unittest
import argparse as ap
import os


def passing_condition(results):
    return int(len(results.errors) != 0 or len(results.failures) != 0)


def test_file(args):

    runner = unittest.TextTestRunner()
    exit_code = 0
    try:
        suites = unittest.TestLoader().loadTestsFromName(args.file_name)
        # suites = unittest.TestLoader().discover(
        #     start_dir=args.module_name, pattern=args.file_name
        # )
        print(f"Found file '{args.file_name}' with {suites.countTestCases ()} test(s)")
        results = runner.run(suites)

        exit_code = passing_condition(results)

    except ImportError as err:
        print("An error occurred")
        print(err)
        exit_code = 1

    return exit_code


def test_all():
    current_dir = os.path.dirname(os.path.realpath(__file__))
    runner = unittest.TextTestRunner()
    suites = unittest.TestLoader().discover(current_dir, pattern="test_*.py")
    results = runner.run(suites)

    return passing_condition(results)


# entry point for testing
if __name__ == "__main__":

    parser = ap.ArgumentParser(description="Entry point for calling backend tests")
    parser.add_argument("--file_name", "-f", type=str, help="run a specific file")
    parser.add_argument(
        "--all",
        "-a",
        action="store_true",
        help="run all tests, same as running without parameter",
    )
    args = parser.parse_args()

    print("Beginning testing routine...")
    exit_code = 0
    if args.file_name:
        exit_code = test_file(args)
    else:
        exit_code = test_all()
    print("Stopping testing routine...")
    exit(exit_code)
