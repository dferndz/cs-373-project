_reserved_keywords = {"metadata", "query", "registry"}


def _serialize(obj, fields, additional_fields=None):
    d = dict()

    if fields is None:
        fields = [
            f for f in dir(obj) if not f.startswith("_") and f not in _reserved_keywords
        ]

    if additional_fields is not None:
        fields += additional_fields

    for f in fields:

        if isinstance(f, tuple):
            if len(f) > 1:
                field_name = f[1]
                f = f[0]
            else:
                field_name = f[0]
                f = f[0]
        else:
            field_name = f

        tokens = f.split("__")
        ins = obj
        while len(tokens) > 1:
            ins = ins.__getattribute__(tokens.pop(0))

        if "_ins" in tokens[0]:
            continue

        d[field_name] = ins.__getattribute__(tokens.pop(0))
    return d


def _serialize_many(objs, fields):
    return [_serialize(obj, fields) for obj in objs]


class ModelSerializer:
    list_fields = None
    additional_fields = None
    fields = None

    def __int__(self):
        if self.list_fields is None:
            self.list_fields = self.fields

    def serialize(self, obj, many=False):
        if many:
            return _serialize_many(obj, self.list_fields)
        else:
            return _serialize(obj, self.fields, self.additional_fields)


class MixinSerializer:
    def __init__(self, serializer_classes: dict):
        self.serializers = {
            model: serializer_class()
            for model, serializer_class in serializer_classes.items()
        }

    def serialize(self, data: dict):
        return {
            model: self.serializers[model].serialize(res, many=True)
            for model, res in data.items()
        }


class CountrySerializer(ModelSerializer):
    list_fields = [
        "id",
        "name",
        "continent",
        "currency",
        "air_quality_index",
        ("temp", "temp_average"),
        "temp_max",
        "temp_min",
        "temp_feels_like",
        "currency",
        "tourists",
        "flag_url",
        "pic_url",
    ]


class CitySerializer(ModelSerializer):
    list_fields = [
        "id",
        "name",
        ("country_ins__name", "country_name"),
        ("country_ins__code", "country_code"),
        ("country_ins__flag_url", "flag_url"),
        "country",
        "risk_level",
        "deaths",
        "cases",
        "quarantine_dur",
        "pic_url",
        "vaccine_policy",
        "mask_policy",
    ]

    additional_fields = [
        ("country_ins__name", "country_name"),
        ("country_ins__continent", "country_continent"),
        ("country_ins__flag_url", "flag_url"),
    ]


class FlightSerializer(ModelSerializer):
    list_fields = [
        "id",
        "departure_city",
        ("departure_city_ins__name", "departure_city_name"),
        ("arrival_city_ins__name", "arrival_city_name"),
        ("departure_city_ins__country_ins__name", "departure_country_name"),
        ("arrival_city_ins__country_ins__name", "arrival_country_name"),
        "arrival_city",
        "departure_time",
        "duration",
        "cost_total",
        "cost_currency",
        "service_class",
        "segments",
    ]

    additional_fields = [
        ("departure_city_ins__name", "departure_city_name"),
        ("arrival_city_ins__name", "arrival_city_name"),
        ("departure_city_ins__country_ins__name", "departure_country_name"),
        ("arrival_city_ins__country_ins__name", "arrival_country_name"),
        ("departure_city_ins__country", "departure_country"),
        ("arrival_city_ins__country", "arrival_country"),
        ("departure_city_ins__country_ins__flag_url", "departure_country_flag_url"),
        ("arrival_city_ins__country_ins__flag_url", "arrival_country_flag_url"),
        ("departure_city_ins__country_ins__continent", "departure_country_continent"),
        ("arrival_city_ins__country_ins__continent", "arrival_country_continent"),
        ("departure_city_ins__pic_url", "departure_city_pic_url"),
        ("arrival_city_ins__pic_url", "arrival_city_pic_url"),
    ]
