from flask import jsonify
from controllers.exceptions import BadRequest, NotFound


class Paginator:
    DEFAULT_PAGE_SIZE = 15

    @staticmethod
    def default_make_response(page, page_size, total_pages, total_rows, data):
        next_page = page + 1 if page + 1 <= total_pages else None
        prev_page = page - 1 if page - 1 >= 1 else None

        return {
            "page": page,
            "page_size": page_size,
            "data": data,
            "total_pages": total_pages,
            "total_rows": total_rows,
            "next": next_page,
            "prev": prev_page,
            "first": 1,
            "last": total_pages,
        }

    def __init__(self, page_size, make_response=None):
        self.page_size = page_size
        self.make_response = (
            make_response
            if make_response is not None
            else Paginator.default_make_response
        )

    def validate(self, **kwargs):
        page = int(kwargs.get("page", 1))
        page_size = int(kwargs.get("page_size", self.page_size))

        if page < 1:
            raise BadRequest("page must be greater or equal to 1")

        return kwargs

    def get_page(self, query, **kwargs):
        valid = self.validate(**kwargs)

        page = int(valid.get("page", 1))
        page_size = int(valid.get("page_size", self.page_size))

        data = query.limit(page_size).offset((page - 1) * page_size).all()

        total_rows = query.count()
        total_pages = (total_rows + page_size - 1) // page_size if total_rows > 0 else 1

        if page > total_pages:
            raise NotFound("page not found")

        return self.make_response(
            page=page,
            page_size=page_size,
            total_pages=total_pages,
            total_rows=total_rows,
            data=data,
        )

    def get_response(self, query, serializer_class, **kwargs):
        serializer = serializer_class()
        page = self.get_page(query, **kwargs)
        page["data"] = serializer.serialize(page["data"], many=True)

        return jsonify(page)
