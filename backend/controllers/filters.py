from sqlalchemy import or_, desc
from sqlalchemy.orm import Query
from models.country import Country
from models.city import City
from models.flights import Flights
from database import Base
import re


_reserved_keywords = {"metadata", "query", "registry"}

_funcs = {
    "max": lambda a, b: a <= float(b),
    "min": lambda a, b: a >= float(b),
    "eq": lambda a, b: a == b,
}


class Filter:
    fields = None
    search_fields = None
    model: Base
    query: Query

    def __init__(self):
        if self.fields is None:
            self.fields = [
                f
                for f in dir(self.model)
                if not f.startswith("_") and f not in _reserved_keywords
            ]

        self.query = self.model.query

    def get_search_fields(self, token):
        return []

    def _build_search_filter(self, string):
        field_set = set(self.search_fields)
        tokens = re.split(r"\s+", string)
        base_search_fields = [
            self._get_field(f).ilike(f"%{token}%")
            for f in field_set
            for token in tokens
        ]
        additional_search_fields = [
            f for token in tokens for f in self.get_search_fields(token)
        ]

        return [*base_search_fields, *additional_search_fields]

    def _get_func(self, string):
        tokens = string.split("___")
        if len(tokens) == 1:
            return tokens[0], _funcs["eq"]
        return tokens[0], _funcs[tokens[1]]

    def _get_field(self, name):
        return getattr(self.model, name)

    def _build_filter_query(self, params):
        field_set = set(self.fields)
        q = []
        for name in params:
            value = params[name]
            if not value:
                continue

            values = str(value).split(",")
            if not len(values):
                continue

            name, func = self._get_func(name)
            if name not in field_set:
                continue

            q.append(or_(*[func(self._get_field(name), v) for v in values]))

        return q

    def get(self, limit=None, **kwargs):
        query = self.filter(self.query, **kwargs)
        if limit:
            query = query.limit(limit)
        return query

    def filter(self, query, **kwargs):
        search = kwargs.get("search", None)
        sort_field = kwargs.get("sort", None)

        if search:
            query = query.filter(or_(*self._build_search_filter(search)))

        filters = self._build_filter_query(kwargs)

        if filters:
            query = query.filter(*filters)

        if sort_field:
            if "desc" in kwargs:
                query = query.order_by(desc(self._get_field(sort_field)))
            elif sort_field.startswith("-"):
                query = query.order_by(desc(self._get_field(sort_field[1:])))
            else:
                query = query.order_by(self._get_field(sort_field))

        return query


class FilterMixin:
    def __init__(self, filters: dict):
        self.filters = {model: f() for model, f in filters.items()}

    def get(self, **kwargs):
        limit = kwargs.pop("limit", 10)
        return {
            model: f.get(**kwargs, limit=limit) for model, f in self.filters.items()
        }


class CountryFilter(Filter):
    model = Country
    search_fields = ["name", "continent", "subregion", "capital", "code"]

    def get_search_fields(self, token):
        return [Country.cities_ins.any(City.name.ilike(f"%{token}%"))]


class CityFilter(Filter):
    model = City
    search_fields = [
        "iata",
        "name",
        "mask_policy",
        "risk_level",
    ]

    def get_search_fields(self, token):
        return [City.country_ins.has(Country.name.ilike(f"%{token}%"))]


class FlightsFilter(Filter):
    model = Flights
    search_fields = []

    def get_search_fields(self, token):
        return [
            Flights.departure_city_ins.has(City.name.ilike(f"%{token}%")),
            Flights.departure_city_ins.has(
                City.country_ins.has(Country.name.ilike(f"%{token}%"))
            ),
            Flights.arrival_city_ins.has(City.name.ilike(f"%{token}%")),
            Flights.arrival_city_ins.has(
                City.country_ins.has(Country.name.ilike(f"%{token}%"))
            ),
            Flights.segments[0]["flight_num"].has_key(token),
        ]
