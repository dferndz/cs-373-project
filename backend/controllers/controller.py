# The files in this directory would handle API calls made to the backend.
# We only have 6 endpoints so 1 file might end up being all we need for handling those
# The data to return will be obtained from our database which we interface with
# Using the methods in data>database>ingest.py

# Could just make this the country controller? or put them all in this file
from database import db_session
from database.initialize import add_countries_small as acs, add_countries_all as aca
from sqlalchemy import or_
import os

from database.v2.populate import populate_db
from models.country import Country
from models.city import City
from models.flights import Flights
from controllers.exceptions import NotFound, AuthError
from g_secrets import get_secret
from database import clear_db
from flask import Blueprint, jsonify, request
from controllers.paginator import Paginator
from controllers.serializers import (
    CountrySerializer,
    CitySerializer,
    FlightSerializer,
    MixinSerializer,
)
from models.country import Exchange_rates
from controllers.filters import CountryFilter, CityFilter, FlightsFilter, FilterMixin


blueprint = Blueprint("test_blueprint", __name__)
paginator = Paginator(Paginator.DEFAULT_PAGE_SIZE)
search_mixin = FilterMixin(
    {"cities": CityFilter, "countries": CountryFilter, "flights": FlightsFilter}
)
search_serializer = MixinSerializer(
    {
        "cities": CitySerializer,
        "countries": CountrySerializer,
        "flights": FlightSerializer,
    }
)


@blueprint.route("/search")
def search():
    results = search_mixin.get(**request.args)
    return jsonify(search_serializer.serialize(results))


@blueprint.route("/flights")
def list_flights():
    flight_filter = FlightsFilter()
    return paginator.get_response(
        flight_filter.get(**request.args), FlightSerializer, **request.args
    )


@blueprint.route("/flights/<flight_id>")
def get_flight(flight_id):
    flight = Flights.query.get(flight_id)

    if flight is None:
        raise NotFound("flight not found")

    return jsonify(FlightSerializer().serialize(flight))


@blueprint.route("/cities")
def list_cities():
    city_filter = CityFilter()
    return paginator.get_response(
        city_filter.get(**request.args), CitySerializer, **request.args
    )


@blueprint.route("/cities/<city_id>")
def get_city(city_id):
    city = City.query.get(city_id)

    if city is None:
        raise NotFound("city not found")

    return jsonify(CitySerializer().serialize(city))


@blueprint.route("/cities/<city_id>/flights")
def get_flights_for_city(city_id):
    city = City.query.get(city_id)

    if city is None:
        raise NotFound("city not found")

    flights = Flights.query.filter(
        or_(Flights.departure_city == city.id, Flights.arrival_city == city.id)
    )

    params = dict(**request.args)
    if "page_size" not in params:
        params["page_size"] = 10

    return paginator.get_response(flights, FlightSerializer, **params)


@blueprint.route("/countries")
def all_countries():
    country_filter = CountryFilter()
    return paginator.get_response(
        country_filter.get(**request.args), CountrySerializer, **request.args
    )


@blueprint.route("/countries/<country_id>/cities")
def get_country_cities(country_id):
    country = Country.query.get(country_id)

    if country is None:
        raise NotFound("country not found")

    cities = City.query.filter_by(country=country.id)

    return paginator.get_response(cities, CitySerializer, **request.args)


@blueprint.route("/countries/<country_id>/flights")
def get_country_inbound_flights(country_id):
    country = Country.query.get(country_id)

    if country is None:
        raise NotFound("country not found")

    cities = City.query.filter_by(country=country_id)
    cities = [city.id for city in cities]

    flights = Flights.query.filter(
        or_(Flights.arrival_city.in_(cities), Flights.departure_city.in_(cities))
    )

    params = dict(**request.args)
    if "page_size" not in params:
        params["page_size"] = 10

    return paginator.get_response(flights, FlightSerializer, **params)


@blueprint.route("/countries/<country_id>")
def id_country(country_id):
    country = Country.query.filter_by(id=country_id).first()

    if country is None:
        raise NotFound("country not found")

    # Get exchange rate sub-table as a dict
    exchange_response = Exchange_rates.query.filter_by(
        currency=country.currency.lower()
    ).first()
    assert exchange_response is not None
    er = exchange_response.__dict__
    rate_codes = Exchange_rates.__table__.columns.keys()[1:]
    rates = {key: er[key] for key in er if key in rate_codes}

    response = {
        "id": country.id,
        "name": country.name,
        "code": country.code,
        "continent": country.continent,
        "subregion": country.subregion,
        "geoData": {"lat": country.lat, "lon": country.lon},
        "air_quality_index": country.air_quality_index,
        "flag_url": country.flag_url,
        "pic_url": country.pic_url,
        "capital": country.capital,
        "population": country.population,
        "num_vaccinated": country.num_vaccinated,
        "tourists": country.tourists,
        "languages": country.languages.split(","),
        "currency": country.currency,
        "exchange_rates": rates,
        "temp": country.temp,
        "temp_min": country.temp_min,
        "temp_max": country.temp_max,
        "temp_feels_like": country.temp_feels_like,
        "description": country.description,
    }
    return jsonify(response)


def validate_admin_key(args):
    code = os.environ.get("MANAGEMENT_CODE")
    if code.startswith("secret"):
        _, name = code.split("/")
        code = get_secret(name)

    user_code = args.get("code", None)

    if code != user_code:
        raise AuthError("you are not authorized to perform this action")


@blueprint.route("/admin/reset-db")
def reset_db():
    validate_admin_key(request.args)
    clear_db()
    return jsonify({"message": "reset successful"})


@blueprint.route("/admin/fill-db-all")
def fill_db_all():
    validate_admin_key(request.args)
    populate_db()
    return jsonify({"message": "success"})


@blueprint.route("/admin/fill-db-small")
def fill_db_small():
    validate_admin_key(request.args)
    countries_added = acs(db_session)
    return jsonify({"message": f"added {countries_added} countries"})
