import unittest
from flask import Flask
from controllers.controller import blueprint
from controllers.filters import CountryFilter, CityFilter, FlightsFilter


class TestFilters(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        app = Flask(__name__)
        app.register_blueprint(blueprint, url_prefix="")
        cls.app = app.test_client()

        cls.flights_params = {
            "departure_city": 2,
        }

        cls.country_params = {
            "air_quality_index___min": 80,
            "air_quality_index___max": 120,
        }

        cls.city_params = {"country": 2}

    def test_flights_filter(self):
        flights_filter = FlightsFilter()
        result_query = flights_filter.get(**self.flights_params)
        for i in result_query.all():
            self.assertEqual(i.departure_city, 2)

    def test_country_filter(self):
        country_filter = CountryFilter()
        result_query = country_filter.get(**self.country_params)
        for i in result_query.all():
            self.assertGreaterEqual(i.air_quality_index, 80)
            self.assertLessEqual(i.air_quality_index, 120)

    def test_city_filter(self):
        city_filter = CityFilter()
        result_query = city_filter.get(**self.city_params)
        for i in result_query.all():
            self.assertEqual(i.country, 2)
