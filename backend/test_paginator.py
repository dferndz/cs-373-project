import unittest
from flask import Flask
from controllers.controller import blueprint
from controllers.paginator import Paginator
from models.country import Country


class TestPaginator(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        app = Flask(__name__)
        app.register_blueprint(blueprint, url_prefix="")
        cls.app = app.test_client()
        cls.paginator = Paginator(Paginator.DEFAULT_PAGE_SIZE)

        cls.params = [{"page_size": 5, "page": 2}, {"page_size": 15, "page": 4}]

    def test_paginator(self):
        for param in self.params:
            page = self.paginator.get_page(Country.query, **param)
            self.assertEqual(page["page"], param["page"])
            self.assertEqual(page["page_size"], param["page_size"])
            self.assertEqual(page["page_size"], len(page["data"]))
