# Project Proposal

- Group number: 11 AM Group 2

- Team members: Dillon Samra, Jay Acosta, Saran Chockan, Willee Johnston, Daniel Fernandez

- Project name: SafeTravels

- Project site: https://www.plansafetrips.me/

- Gitlab URL: https://gitlab.com/dferndz/cs-373-project

- Proposal:
  - A website that allows users to plan the location of their next trip by providing information on a country, the COVID data and restrictions for cities in that country, and flight costs/restrictions to fly there.

- Data source APIs:
    - Covid data: 
      - https://developers.amadeus.com/self-service/category/covid-19-and-travel-safety/api-doc/travel-restrictions/api-reference
      - https://www.mapbox.com/
    - Flight Information: https://flightaware.com/aeroapi/portal/documentation#overview
    - Country 
      - https://restcountries.com/
      - https://api-ninjas.com/api/country
      
- Models:
    - Country
    - Flight
    - CityCovidInfo

- Instances:
    - Country: about 200
    - Flight: about 300
    - CityCovidInfo: about 1000

- Attributes:
    - Country
        - Filter/sort:
          - Continent
          - Air Quality
          - Temperature
          - Currency
          - Number of tourists
        - Search:  
          - Name
          - Capital City
          - Language
          - Region
          - Description

    - Flight
        - Filter/sort:  
            - Price
            - Flight Duration
            - Number of stops
            - Airline
            - Service class
        - Search:    
            - Departure city
            - Destination city
            - Departure time
            - Airport
            - Aircraft Type
      
    - CityCovidInfo
        - Filter/sort:  
            - Total covid cases
            - Total covid deaths
            - Risk level
            - Quarantine duration
            - Mask requirement
            - Vaccine required
        - Search: 
            - Restriction description
            - City name
            - Restriction start date
            - Restriction end date
            - Country/Region name

- Model relations:
  - Country: 
    - relates to Flight by departure/arrival location
    - relates to CovidCityInfo by city in that country
  - Flight:
    - relates to departure/arrival country
    - relates to departure/arrival city covid info
  - CityCovidInfo:
    - relates to country where the city with restrictions is
    - relates to flight by departure/arrivals into that city

- Media types:
    - Country:
      - Country flag (image)
      - Country location (map) 
      - Country description (text)
    - Flight:
      - Airline logo (image)
      - Route (map)
      - Flight information (text)
    - CityCovidInfo:
      - City location (map)
      - Restriction information (text)


- What three questions will you answer due to doing this data synthesis on your site?
    - What cities in a certain country are open to tourism and are safe to visit?
    - What are the best prices to travel to those locations?
    - What are the covid requirements and restrictions of flying and visiting a city?

- Resources:
   - City IATA codes: https://www.iata.org/en/publications/directories/code-search/?airport.search=beijing
     This is for the Amadeus Covid API. Use city code for the metropolitan area
