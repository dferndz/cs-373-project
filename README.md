# CS 373 Software Engineering - SafeTravels

## Members

| Name                 | UTEID   | GitLab ID       |
| -------------------- | ------- | --------------- |
| Daniel Fernandez     | def846  | @dferndz        |
| Jay Acosta           | jaa6632 | @jayacosta      |
| Dillon Samra         | dws2557 | @dillonsamra9   |
| Saran Chockan        | sc59852 | @saranchockan   |
| Willee Johnston      | wlj394  | @willeewaco     |

## Project Leader

Responsibility of a project leader: A project leader should supervise the work of the group members and ensure all features implemented by the phase deadline.

Phase 1: Daniel Fernandez

Phase 2: Dillon Samra

Phase 3: Saran Chockan

Phase 4: Willee Johnston

## Git SHA

Phase 1: e049b3dc95af3346c604c34347453a0f217e2bab

Phase 2: f4e23d9f20e9419928c8111ebf3e9033aab1d71e

Phase 3: f57b80c635ec0af0a05803a1ac25dac33976cb7e

Phase 4: 7ae3088c040decb81a7b0333455a63a2939c8e19

## Gitlab Pipelines

https://gitlab.com/dferndz/cs-373-project/-/pipelines

## Website Link

https://www.plansafetrips.me/

## API Documentation Link

https://documenter.getpostman.com/view/19718978/UVkqsvCG

## API Base URL

https://api.plansafetrips.me/

## Completion Times

### Phase 1

| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Daniel Fernandez     | 12        | 16     |
| Jay Acosta           | 10        | 13     |
| Dillon Samra         | 14        | 16     |
| Saran Chockan        | 13        | 14     |
| Willee Johnston      | 10        | 13     |

### Phase 2

| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Daniel Fernandez     | 20        | 30     |
| Jay Acosta           | 15        | 23     |
| Dillon Samra         | 20        | 30     |
| Saran Chockan        | 15        | 28     |
| Willee Johnston      | 19        | 23     |

### Phase 3

| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Daniel Fernandez     | 15        | 24     |
| Jay Acosta           | 13        | 14     |
| Dillon Samra         | 20        | 16     |
| Saran Chockan        | 15        | 20     |
| Willee Johnston      | 15        | 16     |

### Phase 4

| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Daniel Fernandez     | 10        | 20     |
| Jay Acosta           | 8         | 14     |
| Dillon Samra         | 15        | 16     |
| Saran Chockan        | 12        | 15     |
| Willee Johnston      | 13        | 16     |


## Additional Comments

### Phase 1
Pulling data from GitLab API is partially based off of Texas Votes repository code. Icons of commits, tests, and issues borrowed from Bevo's Course Guide repository.

### Phase 2
Selenium test code is adapted from the texasvotes repository.

### Phase 3
No comments.

### Phase 4
Some visualizations use code from the recharts documentation. Those segments with borrowed code have a comment.
