import React from "react";
import { Box, Card, CardContent } from "@mui/material";

const Sidebar = ({ children, ...props }) => {
  return (
    <Box {...props}>
      <Card>
        <CardContent>{children}</CardContent>
      </Card>
    </Box>
  );
};

export { Sidebar };
