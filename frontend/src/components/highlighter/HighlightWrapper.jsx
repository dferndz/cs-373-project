import React from "react";
import Highlighter from "react-highlight-words";
import { useLocation } from "react-router";
import { findHighlighterWords } from "../../utils/functions/findHighlightWords";

const HighlightWrapper = ({ text, children }) => {
  const { search } = useLocation();
  const highlighterStyle = {
    "background-color": "rgba(255,255,56,0.4)",
    "border-radius": "6px",
    padding: "3px 0px",
  };

  return (
    <Highlighter
      highlightStyle={highlighterStyle}
      textToHighlight={text}
      searchWords={findHighlighterWords(search)}
    >
      <div>{children}</div>
    </Highlighter>
  );
};

export default HighlightWrapper;
