import React from "react";
import {
  Table,
  TableRow,
  TableHead,
  TableCell,
  TableBody,
  TableContainer,
} from "@mui/material";
import { observer } from "mobx-react";

const CurrencyExchangeTable = observer(({ rates }) => {
  const formatter = Intl.NumberFormat("en", {
    maximumFractionDigits: 4,
    minimumFractionDigits: 4,
  });
  const rates_keys = Object.keys(rates);

  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Exchange rates</TableCell>
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {rates_keys.map((value, key) => (
            <TableRow key={key}>
              <TableCell>{value}</TableCell>
              <TableCell>{formatter.format(rates[value])}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
});

export { CurrencyExchangeTable };
