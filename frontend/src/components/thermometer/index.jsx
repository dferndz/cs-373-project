import EcotropyThermometer from "react-thermometer-ecotropy";
import { Grid, Typography } from "@mui/material";
import React from "react";

function convertTofahrenheit(value) {
  return String(Math.floor((value * 9) / 5 + 32));
}

const Item = ({ label, value, isTemp = true }) => {
  return (
    <Typography fontSize={14} fontWeight="lighter" variant="h6">
      {value &&
        `${label}: ${value} ${isTemp ? "°C" : ""} ${
          isTemp ? " / " + convertTofahrenheit(value) + " °F" : ""
        } `}
    </Typography>
  );
};

const Thermometer = ({ temp, text, air, ...props }) => {
  const { average, high, low, feelsLike } = temp;

  return (
    <Grid container {...props}>
      {text && (
        <Grid item xs={12}>
          <h3>{text}</h3>
        </Grid>
      )}
      <Grid item xs={4}>
        <EcotropyThermometer
          theme="light"
          value={average}
          max="50"
          format="°C"
          size="large"
          height="300"
        />
      </Grid>
      <Grid item xs={8}>
        <Item label="Low" value={low} />
        <Item label="High" value={high} />
        <Item label="Feels like" value={feelsLike} />
        <Item label="Air quality" value={air} isTemp={false} />
      </Grid>
    </Grid>
  );
};

export { Thermometer };
