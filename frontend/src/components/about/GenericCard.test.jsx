import React from "react";
import { screen } from "@testing-library/react";
import { GenericCard } from "./GenericCard";
import { tools } from "../../assets/data/tools";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";

jest.mock("../../assets/data/tools", () => {
  return {
    tools: [
      {
        title: "Has Everything",
        description: "Best description in the world",
        link: "https://random.org",
        icon: null,
      },
      {
        title: "No Description!",
        description: "Test description",
        link: "https://random.org",
        icon: null,
      },
      {
        title: "No link",
        description: "Not going anywhere",
        link: "sample link",
        icon: null,
      },
    ],
  };
});

describe("Renders a GenericCard component", () => {
  test("renders the first tool using a GenericCard", () => {
    expect(tools.length > 0);
    renderWithHistory(<GenericCard {...tools[0]} />);

    const title = screen.getByText(tools[0].title);
    const description = screen.getByText(tools[0].description);
    expect(title).toBeInTheDocument();
    expect(description).toBeInTheDocument();
  });

  test("no description provided to GenericCard", () => {
    expect(tools.length > 1);
    renderWithHistory(<GenericCard {...tools[1]} description={undefined} />);

    const description = screen.queryByText(tools[1].description);
    expect(description).toBeNull();
  });

  test("no link provided to GenericCard", () => {
    expect(tools.length > 1);
    renderWithHistory(<GenericCard {...tools[1]} link={undefined} />);

    const link = screen.queryByText(tools[1].link);
    expect(link).toBeNull();
  });
});
