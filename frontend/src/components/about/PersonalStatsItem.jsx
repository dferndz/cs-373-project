import React from "react";
import { Box, Grid, Typography } from "@mui/material";

const PersonalStatsItem = ({subject, icon, number}) => {
  return (
    <Grid item
      sx={{
        maxWidth: 90
      }}>
      <Box
        component="img"
        sx={{
          height: 50,
          width: 50,
        }}
        alt="Issues Icon"
        src={icon}>
      </Box>
      <Typography
        sx={{display: "hidden"}}
        variant="body2">
        {subject}: {number}
      </Typography>
    </Grid>
   )
}

export { PersonalStatsItem };
