import React from "react";
import { Box, Card, CardActionArea, CardMedia, CardContent, Typography } from "@mui/material";

const CardContents = (props) => {

  const {
    title,
    description,
    icon,
    width,
    height
  } = props;

  return (
    <Box>
      <Box
        sx={{
          padding: 2,
          height: (height - 4),
          display: "flex",
          flexDirection: "column",
          justifyContent: "center"
        }}>
        <CardMedia
          component="img"
          width={width}
          image={icon}>
        </CardMedia>
      </Box>
      <CardContent>
        <Typography
          fontWeight="bold"
          variant="h5"
          paddingBottom={1}>
          {title}
        </Typography>
        {
          description ?
            <Typography
              variant="body2">
              {description}
            </Typography> :
            <></>
        }
      </CardContent>
    </Box>
  )
}

const GenericCard = (props) => {

  const {
    link,
    width
  } = props;

  return (
    <Card
      sx={{
        width: width,
        margin: 1,
        textAlign: "center"
      }}>
      {
        link?
        <CardActionArea sx={{ height: "100%" }} href={link}>
          <CardContents {...props} />
        </CardActionArea>:
        <CardContents {...props} />
      }
    </Card>
  )
}

export { GenericCard };