import React from "react";
import {
  Box,
  Card,
  CardMedia,
  CardContent,
  Divider,
  Grid,
  Typography,
} from "@mui/material";
import CommitIcon from "../../assets/images/icons/CommitIcon.svg";
import TestIcon from "../../assets/images/icons/TestIcon.svg";
import IssuesIcon from "../../assets/images/icons/IssuesIcon.svg";
import { PersonalStatsItem } from "./PersonalStatsItem";

const BioCard = (props) => {
  const {
    name,
    headshot,
    description,
    role,
    phaseleader,
    issues,
    commits,
    tests,
  } = props;

  return (
    <Card
      sx={{
        height: 590,
        maxWidth: 300,
        boxShadow: 5,
        margin: 1,
        position: "relative",
      }}
    >
      <CardMedia
        component="img"
        height="250"
        image={headshot}
        alt={`picture of ${name}`}
      />
      <CardContent align="center">
        <Typography gutterBottom fontWeight="bold" variant="h5">
          {name}
        </Typography>
        <Typography gutterBottom variant="h6">
          {role}
        </Typography>
        <Typography sx={{ fontStyle: "italic" }} variant="body1" gutterBottom>
          {phaseleader}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
        <Box
          sx={{
            position: "absolute",
            bottom: 0,
            left: 0,
            margin: 0,
            width: "100%",
          }}
        >
          <Divider sx={{ pb: 2 }} />
          <Grid
            container
            sx={{
              p: 1,
              flexDirection: "row",
              alignContent: "center",
              justifyContent: "space-around",
            }}
          >
            <PersonalStatsItem
              subject="Issues"
              number={issues}
              icon={IssuesIcon}
            />
            <PersonalStatsItem
              subject="Commits"
              number={commits}
              icon={CommitIcon}
            />
            <PersonalStatsItem subject="Tests" number={tests} icon={TestIcon} />
          </Grid>
        </Box>
      </CardContent>
    </Card>
  );
};

export { BioCard };
