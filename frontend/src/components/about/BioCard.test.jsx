import React from "react";
import { screen } from "@testing-library/react";
import { BioCard } from "./BioCard";
import { bios } from "../../assets/data/bios";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";

jest.mock("../../assets/data/bios", () => {
  return {
    bios: [
      {
        name: "Joe",
        gitlabNames: ["joe", "glenn"],
        role: "Full-stack Developer",
        headshot: null,
        tests: 0,
        commits: 0,
        issues: 0,
        description: "I like long walks on the beach.",
      },
    ],
  };
});

describe("Renders a BioCard component", () => {
  test("renders a sample BioCard", () => {
    expect(bios.length > 0);
    renderWithHistory(<BioCard {...bios[0]} />);

    const name = screen.getByText(bios[0].name);
    const description = screen.getByText(bios[0].description);
    expect(name).toBeInTheDocument();
    expect(description).toBeInTheDocument();
  });

  test("renders BioCard with stats preloaded", () => {
    expect(bios.length > 0);

    bios[0].tests = 300;
    bios[0].issues = 400;
    bios[0].commits = 500;

    renderWithHistory(<BioCard {...bios[0]} />);

    const numTests = screen.queryByText(`Tests: ${300}`);
    const numIssues = screen.queryByText(`Issues: ${400}`);
    const numCommits = screen.queryByText(`Commits: ${500}`);

    expect(numTests).toBeInTheDocument();
    expect(numIssues).toBeInTheDocument();
    expect(numCommits).toBeInTheDocument();

    bios[0].tests = 0;
    bios[0].issues = 0;
    bios[0].commits = 0;
  });

  test("renders BioCard headshot correct alternate text", () => {
    expect(bios.length > 0);

    renderWithHistory(<BioCard {...bios[0]} />);

    const image = screen.queryByAltText(`picture of ${bios[0].name}`);

    expect(image).toBeInTheDocument();
  });
});
