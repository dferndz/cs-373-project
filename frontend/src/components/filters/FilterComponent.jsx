import React, { useCallback, useEffect, useMemo, useState } from "react";
import {
  Select,
  MenuItem,
  Grid,
  FormControl,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import { useLocation, useNavigate } from "react-router";
import qs from "query-string";

// Code is adapted from mui's menu, select, and formControl documentation.
//  https://mui.com/material-ui/react-select/

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const FilterComponent = ({ filter }) => {
  const navigate = useNavigate();
  const { pathname, search } = useLocation();
  const params = useMemo(() => qs.parse(search), [search]);

  // If we are dealing with a multi select, then selected needs to
  //  be an array to handle multiple choices.
  const [selected, setSelected] =
    filter.type === "multi" ? useState([]) : useState("");
  const clearFilters = useCallback(() => {
    filter.type === "multi" ? setSelected([]) : setSelected("");
  }, [setSelected, filter]);

  useEffect(() => {
    const newFilters = qs.parse(search);
    let fields = [filter.field_name];

    if (filter.type === "range") {
      fields = [
        ...fields,
        `${filter.field_name}___max`,
        `${filter.field_name}___min`,
      ];
    }

    const clearFlags = fields.map((f) => {
      if (!(f in newFilters)) return true;
      return !newFilters[f];
    });

    if (clearFlags.every((i) => i)) {
      clearFilters();
    } else {
      if (filter.field_name in params) {
        const vals = params[filter.field_name];
        const elements = typeof vals === "string" ? vals.split(",") : vals;
        setSelected(elements);
      }
    }
  }, [search]);

  const changeFilters = useCallback(
    (elements) => {
      if (filter.type === "range") {
        navigate({
          pathname: pathname,
          search: qs.stringify({
            ...params,
            page: 1,
            [filter.field_name + "___min"]: elements[0],
            [filter.field_name + "___max"]: elements[1],
          }),
        });
      } else {
        navigate({
          pathname: pathname,
          search: qs.stringify({
            ...params,
            page: 1,
            [filter.field_name]: String(elements),
          }),
        });
      }
    },
    [navigate, params]
  );

  const handleSelectChange = (event) => {
    event.preventDefault();
    const {
      target: { value },
    } = event;

    const elements = typeof value === "string" ? value.split(",") : value;
    setSelected(elements);
    changeFilters(elements);
  };

  return filter.type == "multi" ? (
    <React.Fragment>
      <Grid item>
        <FormControl sx={{ width: 200 }}>
          <InputLabel id={filter.field_name + "_label"}>
            {filter.display_name}
          </InputLabel>
          <Select
            labelId={filter.field_name + "_label"}
            id={filter.field_name}
            multiple
            value={selected}
            onChange={handleSelectChange}
            input={<OutlinedInput label={filter.display_name} />}
            MenuProps={MenuProps}
          >
            {filter.menu_options.map((option) => (
              <MenuItem key={option.name} value={option.value}>
                {option.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
    </React.Fragment>
  ) : (
    <React.Fragment>
      <Grid item>
        <FormControl sx={{ width: 200 }}>
          <InputLabel id={filter.field_name + "_label"}>
            {filter.display_name}
          </InputLabel>
          <Select
            labelId={filter.field_name + "_label"}
            id={filter.field_name}
            value={selected}
            onChange={handleSelectChange}
            input={<OutlinedInput label={filter.display_name} />}
            MenuProps={MenuProps}
          >
            {filter.menu_options.map((option) => (
              <MenuItem key={option.name} value={option.value}>
                {option.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
    </React.Fragment>
  );
};

export { FilterComponent };
