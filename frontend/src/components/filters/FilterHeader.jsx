import React, { useCallback } from "react";
import { Grid, Divider, Button } from "@mui/material";
import { useLocation, useNavigate } from "react-router";
import { FilterComponent } from "./FilterComponent";
import { SliderComponent } from "./SliderComponent";
import qs from "query-string";

const FilterHeader = ({ filterData, ...props }) => {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const handleClick = useCallback(() => {
    navigate({
      pathname: pathname,
      search: qs.stringify({
        page: 1,
      }),
    });
  }, [navigate]);

  return (
    <React.Fragment>
      <Grid
        container
        item
        spacing={2}
        mb={1}
        alignItems="center"
        sx={{ justifyContent: "center" }}
        {...props}
      >
        {filterData.map((filter, index) =>
          filter.type == "slider" ? (
            <SliderComponent key={index} filter={filter} />
          ) : (
            <FilterComponent key={index} filter={filter} />
          )
        )}
        <Grid item>
          <Button
            variant="contained"
            onClick={handleClick}
            sx={{ width: 200, height: 56 }}
          >
            Clear Filters
          </Button>
        </Grid>
      </Grid>
      <Divider />
    </React.Fragment>
  );
};

export { FilterHeader };
