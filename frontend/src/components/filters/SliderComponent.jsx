import React, { useEffect, useMemo, useState } from "react";
import { Grid, Slider, Typography, Box } from "@mui/material";
import { useLocation, useNavigate } from "react-router";
import qs from "query-string";

// Code is adapted from mui's menu/slider documentation.
//  https://mui.com/material-ui/react-slider/

const MIN_DISTANCE = 10;

const SliderComponent = ({ filter }) => {
  const navigate = useNavigate();
  const { pathname, search } = useLocation();
  const params = useMemo(() => qs.parse(search), [search]);
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    if (filter.type == "slider") {
      navigate({
        pathname: pathname,
        search: qs.stringify({
          ...params,
          page: 1,
          [filter.field_name + "___min"]: selected[0],
          [filter.field_name + "___max"]: selected[1],
        }),
      });
    }
  }, [navigate, selected]);

  const handleSliderChange = (event, newValue, activeThumb) => {
    event.preventDefault();
    if (!Array.isArray(newValue)) {
      return;
    }

    if (newValue[1] - newValue[0] < MIN_DISTANCE) {
      if (activeThumb === 0) {
        const clamped = Math.min(newValue[0], 100 - MIN_DISTANCE);
        setSelected([clamped, clamped + MIN_DISTANCE]);
      } else {
        const clamped = Math.max(newValue[1], MIN_DISTANCE);
        setSelected([clamped - MIN_DISTANCE, clamped]);
      }
    } else {
      setSelected(newValue);
    }
  };

  return (
    <React.Fragment>
      <Grid item>
        <Box sx={{ width: 150 }} textAlign="center">
          <Typography
            varient="h6"
            justifyContent="center"
            alignItems="center"
            mb={-1}
          >
            {filter.display_name}
          </Typography>
          <Slider
            aria-label={filter.field_name + "_label"}
            valueLabelDisplay="auto"
            onChangeCommitted={handleSliderChange}
            defaultValue={[filter.min, filter.max]}
            min={filter.min}
            max={filter.max}
            disableSwap
          />
        </Box>
      </Grid>
    </React.Fragment>
  );
};

export { SliderComponent };
