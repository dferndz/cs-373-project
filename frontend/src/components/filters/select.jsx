import React from "react";
import { Select, MenuItem } from "@mui/material";

const SelectFilter = ({ options, onChange, value }) => {
  return (
    <Select value={value} onChange={onChange}>
      {options.map((op, index) => (
        <MenuItem key={index} value={op.value}>
          {op.name}
        </MenuItem>
      ))}
    </Select>
  );
};

export { SelectFilter };
