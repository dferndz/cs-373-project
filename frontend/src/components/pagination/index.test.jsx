import React from "react";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";
import { Paginator } from "./index";

describe("paginator", () => {
  test("renders a paginator", () => {
    const pagination = {
      currentPage: 3,
      totalPages: 10,
      totalInstances: 2000,
      pageSize: 50,
    };

    const { element } = renderWithHistory(
      <Paginator pagination={pagination} />
    );

    expect(element.getByText(pagination.currentPage)).toBeInTheDocument();
  });
});
