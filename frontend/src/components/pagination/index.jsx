import React, { useCallback, useMemo } from "react";
import {
  Grid,
  Pagination,
  Typography,
  FormControl,
  InputLabel,
} from "@mui/material";
import { useLocation, useNavigate } from "react-router";
import qs from "query-string";
import { SelectFilter } from "../filters/select";
import { PAGE_SIZES } from "../../constants";

const Paginator = ({ pagination, showPageSize = true, ...props }) => {
  const { totalPages, currentPage, pageSize, totalInstances, thisPageSize } =
    pagination;
  const { pathname, search } = useLocation();
  const navigate = useNavigate();
  const params = useMemo(() => qs.parse(search), [search]);

  const handlePageChange = useCallback(
    (event, value) => {
      navigate({
        pathname: pathname,
        search: qs.stringify({
          ...params,
          page: value,
        }),
      });
    },
    [params, pathname]
  );

  const handlePageSizeChange = useCallback(
    (event) => {
      navigate({
        pathname: pathname,
        search: qs.stringify({
          ...params,
          page: 1,
          page_size: event.target.value,
        }),
      });
    },
    [params, pathname]
  );

  return (
    <Grid container {...props}>
      {showPageSize && (
        <Grid item>
          <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
            <InputLabel>Page size</InputLabel>
            <SelectFilter
              onChange={handlePageSizeChange}
              options={PAGE_SIZES}
              value={pageSize}
            />
          </FormControl>
        </Grid>
      )}
      <Grid item>
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
        />
      </Grid>
      <Grid item>
        <Typography
          mt={0.5}
          fontSize={14}
          fontWeight="lighter"
          fontStyle="italic"
        >
          Showing {thisPageSize} out of {totalInstances} records.
        </Typography>
      </Grid>
    </Grid>
  );
};

export { Paginator };
