import React from "react";
import { Container } from "@mui/material";
import { CircleLoad } from "./CircleLoad";

const Fallback = () => {
  return (
    <Container>
      <CircleLoad height="100vh" />
    </Container>
  );
};

export { Fallback };
