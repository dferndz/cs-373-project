import React from "react";
import { Box } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";

const CircleLoad = (props) => {
  const { height } = props;

  return (
    <Box
      sx={{
        minHeight: height,
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
      }}
    >
      <CircularProgress color="primary" />
    </Box>
  );
};

export { CircleLoad };
