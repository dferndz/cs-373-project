import React from "react";
import RoomIcon from "@mui/icons-material/Room";
import { red } from "@mui/material/colors";

const Marker = () => {
  return (
    <React.Fragment>
      <RoomIcon fontSize={"large"} sx={{ color: red[500] }} />
    </React.Fragment>
  );
};

export { Marker };
