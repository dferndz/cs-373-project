import React from "react";
import GoogleMapReact from "google-map-react";

const GOOGLE_MAPS_KEY = process.env.REACT_APP_GOOGLE_MAPS_API_KEY || "";

const Map = ({ location, height, width, zoom }) => {
  const handleApiLoaded = (map, maps) => {
    new maps.Marker({
      position: location,
      map,
    });
  };
  return (
    <div style={{ height: height, width: width }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
        defaultCenter={location}
        defaultZoom={zoom}
        onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
      />
    </div>
  );
};

export { Map };
