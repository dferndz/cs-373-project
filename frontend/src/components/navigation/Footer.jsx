import React from "react";
import { theme } from "../../theme";
import { Box, Container, Typography, ThemeProvider } from "@mui/material";

const styles = {
  FooterContainer: {
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
    minHeight: "15vh",
    backgroundColor: "#1976d2",
  },

  FooterContentContainer: {
    textAlign: "center",
    position: "absolute",
    top: "50%",
    left: "50%",
    color: "#fff",
    transform: "translate(-50%, -50%)",
  },
};

const Footer = () => {
  return (
    <ThemeProvider theme={theme}>
      <Box style={styles.FooterContainer}>
        <Container style={styles.FooterContentContainer}>
          <Typography variant="h6">SafeTravels</Typography>
          <Typography variant="p">Copyright &copy; 2022 SafeTravels</Typography>
        </Container>
      </Box>
    </ThemeProvider>
  );
};

export { Footer };
