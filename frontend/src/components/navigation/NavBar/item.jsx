import React, { useCallback } from "react";
import { Button } from "@mui/material";
import { useNavigate } from "react-router";

const NavBarItem = ({ page }) => {
  const navigate = useNavigate();
  const handleClick = useCallback(() => {
    navigate(page.url);
  }, [navigate, page]);
  return (
    <Button
      onClick={handleClick}
      sx={{ my: 2, color: "#fff", display: "block" }}
    >
      {page.title}
    </Button>
  );
};

export { NavBarItem };
