import React from "react";
import { screen } from "@testing-library/react";
import { NavBar } from "./index";
import {
  MockThemeProvider,
  mockTheme,
} from "../../../utils/tests/MockThemeProvider";
import { pages } from "../../../pages";
import { renderWithHistory } from "../../../utils/tests/renderWithHistory";

jest.mock("../../../pages", () => {
  return {
    pages: [
      {
        title: "MockPage1",
        showInMenu: true,
      },
      {
        title: "MockPage2",
        showInMenu: true,
      },
      {
        title: "MockPage3-hidden",
        showInMenu: false,
      },
    ],
  };
});

describe("Renders a navigation bar", () => {
  test("renders a navigation bar with site title", () => {
    renderWithHistory(
      <MockThemeProvider>
        <NavBar />
      </MockThemeProvider>
    );

    const title = screen.getByText(mockTheme.site.title);

    expect(title).toBeInTheDocument();
  });

  test("renders all menu items", () => {
    renderWithHistory(
      <MockThemeProvider>
        <NavBar />
      </MockThemeProvider>
    );

    pages
      .filter((page) => page.showInMenu)
      .forEach((page) => {
        expect(screen.queryByText(page.title)).toBeInTheDocument();
      });

    pages
      .filter((page) => !page.showInMenu)
      .forEach((page) => {
        expect(screen.queryByText(page.title)).not.toBeInTheDocument();
      });
  });
});
