import React from "react";
import { screen } from "@testing-library/react";
import { NavBarItem } from "./item";
import { renderWithHistory } from "../../../utils/tests/renderWithHistory";

const mockedNavigate = jest.fn();

jest.mock("react-router", () => ({
  ...jest.requireActual("react-router"),
  useNavigate: () => mockedNavigate,
}));

describe("Navbar menu item", () => {
  test("renders a menu button", () => {
    const mockPage = {
      title: "Mock page",
      url: "/mock",
    };

    renderWithHistory(<NavBarItem page={mockPage} />);

    const button = screen.getByText(mockPage.title);
    expect(button).toBeInTheDocument();
  });

  test("changes url when clicked on it", () => {
    const mockPage = {
      title: "Mock page",
      url: "/mock",
    };

    renderWithHistory(<NavBarItem page={mockPage} />);

    const button = screen.getByText(mockPage.title);
    button.click();

    expect(mockedNavigate).toHaveBeenLastCalledWith(mockPage.url);
  });
});
