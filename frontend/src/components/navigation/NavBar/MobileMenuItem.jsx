import React, { useCallback } from "react";
import MenuItem from "@mui/material/MenuItem";
import { useNavigate } from "react-router";

const MobileMenuItem = ({ page }) => {
  const navigate = useNavigate();

  const handleClick = useCallback(() => {
    navigate(page.url);
  }, [navigate, page]);

  return <MenuItem onClick={handleClick}>{page.title}</MenuItem>;
};

export { MobileMenuItem };
