import React, { useCallback } from "react";
import { AppBar, Toolbar, Box, Container, useTheme } from "@mui/material";
import { pages } from "../../../pages";
import { NavBarItem } from "./item";
import { MobileMenu } from "./MobileMenu";
import { Link, useMediaQuery } from "@mui/material";
import { useNavigate } from "react-router";

const NavBar = () => {
  const theme = useTheme();
  const navigate = useNavigate();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  const handleGoToHome = useCallback(
    (event) => {
      event.preventDefault();
      navigate("/");
    },
    [navigate]
  );

  return (
    <React.Fragment>
      <AppBar position="fixed">
        <Container>
          <Toolbar>
            <Link
              href="/"
              onClick={handleGoToHome}
              variant="h6"
              noWrap
              sx={{ mr: 2, color: "#fff", textDecoration: "none" }}
            >
              {theme.site?.title}
            </Link>

            {isMobile ? (
              <MobileMenu />
            ) : (
              <Box
                sx={{
                  flexGrow: 1,
                  justifyContent: "flex-end",
                  alignItems: "center",
                  display: { xs: "flex" },
                }}
              >
                {pages
                  .filter((page) => page.showInMenu)
                  .map((page, number) => (
                    <NavBarItem key={number} page={page} />
                  ))}
              </Box>
            )}
          </Toolbar>
        </Container>
      </AppBar>
      <Toolbar />
    </React.Fragment>
  );
};

export { NavBar };
