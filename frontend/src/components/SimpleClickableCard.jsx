import React, { useCallback } from "react";
import {
  Avatar,
  ButtonBase,
  Card,
  CardHeader,
  // Divider,
  // Typography,
  Box,
} from "@mui/material";
import { observer } from "mobx-react";
import { useNavigate } from "react-router";

const SimpleClickableCard = observer(
  ({ model, id, name, desc, img, ...props }) => {
    const navigate = useNavigate();

    const handleClick = useCallback(() => {
      navigate(`/${model}/${id}`);
    }, [navigate]);

    return (
      <Box {...props}>
        <ButtonBase
          onClick={handleClick}
          style={{
            display: "block",
            textAlign: "initial",
            minWidth: "100%",
            marginTop: "10px",
          }}
        >
          <Card data-testid="city-small-list-card">
            <CardHeader
              title={name}
              titleTypographyProps={{ variant: "body1" }}
              subheader={desc}
              subheaderTypographyProps={{ variant: "caption" }}
              avatar={<Avatar src={img} />}
            />
          </Card>
        </ButtonBase>
      </Box>
    );
  }
);

export { SimpleClickableCard };
