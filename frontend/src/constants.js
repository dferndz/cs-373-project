const PAGE_SIZES = [
  { name: "10", value: 10 },
  { name: "15", value: 15 },
  { name: "25", value: 25 },
];

const DEFAULT_PAGE_SIZE = 15;

export { PAGE_SIZES, DEFAULT_PAGE_SIZE };
