import React from "react";
import { render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { MemoryRouter } from "react-router-dom";

const renderWithHistory = (component, initialUris = []) => {
  const mockHistory = createMemoryHistory({
    initialEntries: initialUris,
  });

  const element = render(
    <MemoryRouter history={mockHistory}>{component}</MemoryRouter>
  );

  return { element, mockHistory };
};

export { renderWithHistory };
