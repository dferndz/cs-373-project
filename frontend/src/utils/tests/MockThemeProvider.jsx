import React from "react";
import { createTheme, ThemeProvider } from "@mui/material";

const mockTheme = createTheme({
  site: {
    title: "MockSiteTitle",
  },
});

const MockThemeProvider = ({ children }) => {
  return <ThemeProvider theme={mockTheme}>{children}</ThemeProvider>;
};

export { MockThemeProvider, mockTheme };
