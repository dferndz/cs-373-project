const findHighlighterWords = (search) => {
  const urlParams = new URLSearchParams(search);
  const searchParam = urlParams.get("search");
  return searchParam ? searchParam.split(/\s/) : [];
};

export { findHighlighterWords };
