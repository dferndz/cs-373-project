const groupByKey = (iterable, key) => {
  const d = {};

  iterable.forEach((i) => {
    if (i[key] in d) {
      d[i[key]].push(i);
    } else {
      d[i[key]] = [i];
    }
  });

  return d;
};

const aggregateByKey = (iterable, key, containerFunc, func) => {
  const d = {};

  iterable.forEach((i) => {
    if (i[key] in d) {
      d[i[key]] = func(d[i[key]], containerFunc(i));
    } else {
      d[i[key]] = containerFunc(i);
    }
  });

  return d;
};

export { groupByKey, aggregateByKey };
