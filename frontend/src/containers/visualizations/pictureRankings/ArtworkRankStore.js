import { cacheProvider } from "../../about/RequestCache";
import { groupByKey } from "../../../utils/functions/dataManagement";
import { action, makeAutoObservable } from "mobx";

const artworkUrl = "https://api.gallerydb.me/artworks";

class ArtworkRankStore {
  data = {};
  loading = false;
  TOP_N = 50;

  constructor() {
    makeAutoObservable(this);
  }

  fetchData = () => {
    this.loading = true;
    cacheProvider.get(artworkUrl).then(
      action((payload) => {
        this.data = this.reduceData(payload);
        this.loading = false;
      })
    );
  };

  reduceData = (payload) => {
    let request = groupByKey(payload.artworks, "popularity_ranking");
    let saved = [];
    for (let i = 1; i <= this.TOP_N; i++) {
      saved.push(request[i][0]);
    }

    return this.modifyRequest(saved);
  };

  modifyRequest = (req) => {
    let num = req.length; // get number of artworks

    return req.map((e) => {
      return {
        name: e.name,
        provider_id: e.id,
        value: num - e.popularity_ranking + 1,
        url: e.image,
      };
    });
  };
}

export { ArtworkRankStore };
