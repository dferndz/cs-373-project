import React, { useState, useEffect } from "react";
import { observer } from "mobx-react";
import { Divider, Grid, Typography } from "@mui/material";
import { Tooltip, Treemap } from "recharts";
import { Fallback } from "../../../components/suspense";
import { ArtworkRankStore } from "./ArtworkRankStore";
import ImageContainer from "./ImageContainer";
import ReactTooltip from "react-tooltip";

const CustomTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    const { name, index } = payload[0].payload;
    return (
      <div styles={{ backgroundColor: "hsla(0, 0 %, 100 %, .8)" }}>
        <p>{`${index + 1} - ${name}`}</p>
      </div>
    );
  }

  return null;
};

const RatingsRanking = observer(() => {
  const [dataStore] = useState(() => new ArtworkRankStore());

  useEffect(() => {
    dataStore.fetchData();
  }, []);

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12}>
        <Divider />
        <Typography
          variant="h5"
          textAlign="center"
          fontWeight="lighter"
          mb={4}
          mt={4}
        >
          Top 50 Artworks by Popularity
        </Typography>
      </Grid>
      {dataStore.loading ? (
        <Fallback />
      ) : (
        <Treemap
          content={<ImageContainer />}
          title={"Rankings"}
          width={500}
          height={500}
          padding={2}
          margin={0}
          cursorStyle={"click"}
          data={dataStore.data}
        >
          <Tooltip
            wrapperStyle={{
              backgroundColor: "rgba(255, 255, 255, .8)",
              paddingLeft: 10,
              paddingRight: 10,
            }}
            content={<CustomTooltip />}
          />
        </Treemap>
      )}
      <ReactTooltip />
    </Grid>
  );
});

export { RatingsRanking };
