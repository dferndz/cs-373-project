import React from "react";
import ReactTooltip from "react-tooltip";

const ImageContainer = (props) => {
  const { x, y, url, width, height, provider_id } = props;

  const handleRebuild = () => () => {
    ReactTooltip.rebuild();
  };

  return (
    <g>
      <a href={`https://gallerydb.me/artworks/${provider_id}`}>
        <image
          x={x}
          y={y}
          href={url}
          width={width}
          height={height}
          preserveAspectRatio="none"
          onMouseEnter={() => handleRebuild()}
          onMouseLeave={() => handleRebuild()}
        />
      </a>
    </g>
  );

  // return (
  //   <div
  //     style={{
  //       backgroundColor: "black",
  //       display: "flex",
  //       justifyContent: "center",
  //       alignItems: "center",
  //     }}
  //   >
  //     <a
  //       href={`https://gallerydb.me/artworks/${id}`}
  //       target="_blank"
  //       rel="noreferrer"
  //     >
  //       <img
  //         data-tip={`${popularity_ranking} - ${name}`}
  //         src={image}
  //         style={{
  //           position: "relative",
  //           objectFit: "fill",
  //           display: "inline-block",
  //           height: Math.min(
  //             width * 6 || Infinity,
  //             height * 6 || Infinity,
  //             120
  //           ),
  //         }}
  //         onMouseEnter={handleRebuild()}
  //         onMouseLeave={handleRebuild()}
  //       />
  //     </a>
  //   </div>
  // );
};

export default ImageContainer;
