import React, { useEffect } from "react";
import { Container, Typography } from "@mui/material";
import { observer } from "mobx-react";
import { useStores } from "../../../stores/useStores";
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Label,
  Cell,
} from "recharts";
import { CircleLoad } from "../../../components/suspense/CircleLoad";

const AverageFlightCostBarChart = observer(() => {
  const { flightCostBarChartStore } = useStores();

  useEffect(() => {
    flightCostBarChartStore.computeAverageFlightCostPerCountry();
  }, []);

  return (
    <Container>
      {flightCostBarChartStore.loading ? (
        <Container>
          <CircleLoad height="70vh" />
        </Container>
      ) : (
        <Container>
          <Typography
            variant="h5"
            textAlign="center"
            fontWeight="lighter"
            mb={4}
            mt={4}
          >
            Average Flight Cost by City
          </Typography>
          <ResponsiveContainer width="100%" height={600}>
            <BarChart
              data={flightCostBarChartStore.flightCostBarChartData.barChartData}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="countryName">
                <Label
                  value="Countries"
                  position="insideBottom"
                  style={{ textAnchor: "middle" }}
                  dy={10}
                />
              </XAxis>
              <YAxis>
                <Label
                  angle={-90}
                  value="Average flight cost ($)"
                  position="insideLeft"
                  style={{ textAnchor: "middle" }}
                />
              </YAxis>
              <Tooltip />
              <Bar dataKey="cost" fill="#00008b">
                {flightCostBarChartStore.flightCostBarChartData.barChartColors.map(
                  (entry, index) => (
                    <Cell key={`cell-${index}`} fill={entry} />
                  )
                )}
              </Bar>
            </BarChart>
          </ResponsiveContainer>
        </Container>
      )}
    </Container>
  );
});

export { AverageFlightCostBarChart };
