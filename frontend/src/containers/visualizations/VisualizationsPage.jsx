import React, { useEffect } from "react";
import { Container, Typography, Grid, Divider } from "@mui/material";
import { observer } from "mobx-react";
import { AverageFlightCostBarChart } from "./components/AverageFlightCostBarChart";
import { CovidHeatmap } from "./covidHeatmap";
import { QuarantineChart } from "./quarantineChart";

const VisualizationsPage = observer(() => {
  useEffect(() => {}, []);

  return (
    <Container>
      <Typography
        variant="h4"
        textAlign="left"
        fontWeight="lighter"
        mb={2}
        mt={4}
      >
        Visualizations
      </Typography>

      <Grid container>
        <Grid item xs={12}>
          <CovidHeatmap />
          <Divider />
        </Grid>
        <Grid item xs={12} pb={2}>
          <AverageFlightCostBarChart />
        </Grid>
        <Grid item xs={12}>
          <QuarantineChart />
        </Grid>
      </Grid>
    </Container>
  );
});

export { VisualizationsPage };
