import { cacheProvider } from "../../about/RequestCache";
import { action, makeAutoObservable } from "mobx";

const artworkUrl = "https://api.gallerydb.me/artworks";

const countByKey = (iterable, key) => {
  const d = {};
  iterable.forEach((i) => {
    if (i[key] in d) {
      d[i[key]] += 1;
    } else {
      d[i[key]] = 1;
    }
  });
  return d;
};

const getRechartsFormat = (iterable) => {
  const d = [];
  Object.entries(iterable).forEach(([key, value]) => {
    if (key != "null") {
      d.push({
        name: key,
        count: value,
      });
    }
  });
  return d;
};

class ArtworkStore {
  data = {};
  loading = false;

  constructor() {
    makeAutoObservable(this);
  }

  fetchData = () => {
    this.loading = true;
    cacheProvider.get(artworkUrl).then(
      action((payload) => {
        this.data = this.reduceData(payload);
        this.loading = false;
      })
    );
  };

  reduceData = (payload) => {
    return getRechartsFormat(countByKey(payload.artworks, "date_created"));
  };
}

export { ArtworkStore };
