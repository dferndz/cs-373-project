import React from "react";
import { Container, Grid, Typography, Divider } from "@mui/material";
import { observer } from "mobx-react";
import { ArtworkChart } from "./artworkChart/ArtworkChart";
import { ArtistRadarChart } from "./artistChart/ArtistRadarChart";
import { RatingsRanking } from "./pictureRankings";

const ProviderVisualizationsPage = observer(() => {
  return (
    <Container>
      <Typography
        variant="h4"
        textAlign="left"
        fontWeight="lighter"
        mb={2}
        mt={4}
      >
        Provider Visualizations
      </Typography>
      <Divider />

      <Grid container>
        <Grid item xs={12}>
          <ArtworkChart />
          <Divider />
        </Grid>
        <Grid item xs={12}>
          <ArtistRadarChart />
        </Grid>
        <Grid item xs={12} mb={3}>
          <RatingsRanking />
        </Grid>
      </Grid>
    </Container>
  );
});

export { ProviderVisualizationsPage };
