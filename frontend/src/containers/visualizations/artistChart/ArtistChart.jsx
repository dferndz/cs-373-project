import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import { Grid, Typography } from "@mui/material";
import { ArtistStore } from "./ArtistStore";
import { observer } from "mobx-react";
import { Fallback } from "../../../components/suspense";

/**
 *  Code adapted from the docs for recharts, namely
 *  https://recharts.org/en-US/examples/LineBarAreaComposedChart
 */

const ArtistChart = observer(() => {
  const [dataStore] = useState(() => new ArtistStore());

  useEffect(() => {
    dataStore.fetchData();
  }, []);

  return (
    <React.Fragment>
      {dataStore.loading ? (
        <Fallback />
      ) : (
        <Grid container>
          <Grid item xs={12}>
            <Typography
              variant="h5"
              textAlign="center"
              fontWeight="lighter"
              mb={4}
              mt={4}
            >
              Artist Nationalities
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <ResponsiveContainer width="100%" height={400}>
              <BarChart
                data={dataStore.data}
                margin={{
                  top: 5,
                  right: 30,
                  left: 20,
                  bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="count" fill="#1976d2" minPointSize={1} />
              </BarChart>
            </ResponsiveContainer>
          </Grid>
        </Grid>
      )}
      <ReactTooltip />
    </React.Fragment>
  );
});

export { ArtistChart };
