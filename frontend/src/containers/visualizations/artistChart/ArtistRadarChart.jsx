import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  ResponsiveContainer,
} from "recharts";
import { Grid, Typography } from "@mui/material";
import { ArtistStore } from "./ArtistStore";
import { observer } from "mobx-react";
import { Fallback } from "../../../components/suspense";

/**
 *  Code adapted from the docs for recharts, namely
 *  https://recharts.org/en-US/examples/LineBarAreaComposedChart
 */

const ArtistRadarChart = observer(() => {
  const [dataStore] = useState(() => new ArtistStore());

  useEffect(() => {
    dataStore.fetchData();
  }, []);

  return (
    <React.Fragment>
      {dataStore.loading ? (
        <Fallback />
      ) : (
        <Grid container>
          <Grid item xs={12}>
            <Typography
              variant="h5"
              textAlign="center"
              fontWeight="lighter"
              mb={4}
              mt={4}
            >
              Artist Nationalities
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <ResponsiveContainer width="100%" height={400}>
              <RadarChart
                cx="50%"
                cy="50%"
                outerRadius="80%"
                data={dataStore.data}
              >
                <PolarGrid />
                <PolarAngleAxis dataKey="name" />
                <PolarRadiusAxis />
                <Radar
                  dataKey="count"
                  stroke="#1976d2"
                  fill="#1976d2"
                  fillOpacity={0.6}
                />
              </RadarChart>
            </ResponsiveContainer>
          </Grid>
        </Grid>
      )}
      <ReactTooltip />
    </React.Fragment>
  );
});

export { ArtistRadarChart };
