import React, { useState } from "react";
import { PieChart, Pie, Sector, Cell } from "recharts";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042", "#EE4B2B"];

// https://recharts.org/en-US/examples/CustomActiveShapePieChart
const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value,
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path
        d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
        stroke={fill}
        fill="none"
      />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 6}
        y={ey}
        textAnchor={textAnchor}
        fill="#333"
      >{`Number of Cities: ${value}`}</text>
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 6}
        y={ey}
        dy={18}
        textAnchor={textAnchor}
        fill="#999"
      >
        {`(Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 6}
        y={ey}
        dy={36}
        textAnchor={textAnchor}
        fontStyle="italic"
        fill="#333"
      >
        {payload.cities.slice(0, 3).join(", ") +
          (payload.cities.length > 3 ? "..." : "")}
      </text>
    </g>
  );
};

const QuarantinePie = ({ store }) => {
  const [piedx, setPiedx] = useState(0);

  return (
    <PieChart width={670} height={300}>
      <Pie
        activeIndex={piedx}
        activeShape={renderActiveShape}
        data={store.data}
        cx={335}
        cy={150}
        innerRadius={60}
        outerRadius={80}
        fill="#1976d2"
        paddingAngle={5}
        onMouseEnter={(_, i) => {
          setPiedx(i);
        }}
        dataKey="value"
      >
        {COLORS.map((_, idx) => (
          <Cell key={`pie-cell-${idx}`} fill={COLORS[idx % COLORS.length]} />
        ))}
      </Pie>
    </PieChart>
  );
};

export { QuarantinePie };
