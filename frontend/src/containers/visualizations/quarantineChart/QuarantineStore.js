import { cacheProvider } from "../../about/RequestCache";
import { apiService } from "../../../stores/apiService";
import { action, makeAutoObservable } from "mobx";

const countriesUrl = apiService.getUrl("/cities?page_size=200");

class QuarantineStore {
  data = {};
  loading = false;

  constructor() {
    makeAutoObservable(this);
  }

  fetchData = () => {
    this.loading = true;
    cacheProvider.get(countriesUrl).then(
      action((payload) => {
        this.data = this.reduceData(payload);
        this.loading = false;
      })
    );
  };

  reduceData = (payload) => {
    let bins = [
      {
        name: "0 days",
      },
      {
        name: "1–7 days",
      },
      {
        name: "8–14 days",
      },
      {
        name: "15–28 days",
      },
      {
        name: "29+ days",
      },
    ];

    payload.data.forEach(({ quarantine_dur, name }) => {
      let idx = Math.min(Math.ceil(quarantine_dur / 7), bins.length - 1);

      if (!("value" in bins[idx])) bins[idx]["value"] = 0;
      if (!("cities" in bins[idx])) bins[idx]["cities"] = [];

      bins[idx]["value"] += 1;
      bins[idx]["cities"].push(name);
    });

    return bins;
  };
}

export { QuarantineStore };
