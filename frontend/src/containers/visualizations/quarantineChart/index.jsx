import React, { useState, useEffect } from "react";
import { Fallback } from "../../../components/suspense";
import { Divider, Grid, Typography } from "@mui/material";
import { QuarantinePie } from "./chart";
import { QuarantineStore } from "./QuarantineStore";
import { observer } from "mobx-react";

const QuarantineChart = observer(() => {
  const [dataStore] = useState(() => new QuarantineStore());

  useEffect(() => {
    dataStore.fetchData();
    // console.log (dataStore.data);
  }, []);

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12}>
        <Divider />
        <Typography
          variant="h5"
          textAlign="center"
          fontWeight="lighter"
          mb={4}
          mt={4}
        >
          Quarantine Durations by City
        </Typography>
      </Grid>
      {dataStore.loading ? <Fallback /> : <QuarantinePie store={dataStore} />}
    </Grid>
  );
});

export { QuarantineChart };
