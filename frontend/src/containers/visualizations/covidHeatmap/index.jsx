import React from "react";
import { WorldMap } from "./map";
import { Divider, Grid, Typography } from "@mui/material";

const CovidHeatmap = () => {
  return (
    <Grid container>
      <Grid item xs={12}>
        <Divider />
        <Typography
          variant="h5"
          textAlign="center"
          fontWeight="lighter"
          mb={4}
          mt={4}
        >
          COVID Cases by Country
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <WorldMap />
      </Grid>
    </Grid>
  );
};

export { CovidHeatmap };
