import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import { scaleLinear } from "d3-scale";
import {
  ComposableMap,
  Sphere,
  Graticule,
  Geographies,
  Geography,
} from "react-simple-maps";
import { CovidCasesStore } from "./CovidCasesStore";
import { useNavigate } from "react-router";
import { observer } from "mobx-react";
import { Fallback } from "../../../components/suspense";

const colorScale = scaleLinear()
  .domain([10000, 20000000])
  .range(["#ffedea", "#ff5233"]);

const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const WorldMap = observer(() => {
  const formatter = Intl.NumberFormat("en", {
    maximumSignificantDigits: 3,
    notation: "compact",
  });
  const [value, setValue] = useState(0);
  const [dataStore] = useState(() => new CovidCasesStore());
  const navigate = useNavigate();

  const handleRebuild = () => () => {
    ReactTooltip.rebuild();
  };

  const handleClick = (countryId) => () => {
    if (countryId) {
      navigate(`/countries/${countryId}`);
    }
  };

  useEffect(() => {
    setValue(value + 1);
  }, [dataStore.data]);

  useEffect(() => {
    dataStore.fetchData();
  }, []);

  return (
    <React.Fragment>
      {dataStore.loading ? (
        <Fallback />
      ) : (
        <ComposableMap
          height={400}
          projectionConfig={{
            rotate: [-10, 0, 0],
            scale: 147,
          }}
        >
          <Sphere stroke="#E4E5E6" strokeWidth={0.5} />
          <Graticule stroke="#E4E5E6" strokeWidth={0.5} />
          <Geographies geography={geoUrl}>
            {({ geographies }) =>
              geographies.map((geo) => {
                const iso = geo.properties.ISO_A2;
                const d =
                  iso in dataStore.data
                    ? dataStore.data[iso]
                    : {
                        cases: 0,
                        deaths: 0,
                        missing: true,
                      };
                return (
                  <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    fill={d.cases > 0 ? colorScale(d.cases) : "white"}
                    stroke="gray"
                    strokeWidth={0.1}
                    data-tip={
                      "missing" in d
                        ? `No data for ${geo.properties.NAME}`
                        : `${geo.properties.NAME} | Cases: ${formatter.format(
                            d.cases
                          )} | Deaths: ${formatter.format(d.deaths)}`
                    }
                    onMouseEnter={handleRebuild()}
                    onMouseLeave={handleRebuild()}
                    onClick={handleClick(d.id)}
                    style={{
                      hover: {
                        fill: "#F53",
                      },
                      pressed: {
                        fill: "#F53",
                      },
                    }}
                  />
                );
              })
            }
          </Geographies>
        </ComposableMap>
      )}
      <ReactTooltip />
    </React.Fragment>
  );
});

export { WorldMap };
