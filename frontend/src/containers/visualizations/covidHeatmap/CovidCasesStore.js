import { cacheProvider } from "../../about/RequestCache";
import { apiService } from "../../../stores/apiService";
import { action, makeAutoObservable } from "mobx";
import { aggregateByKey } from "../../../utils/functions/dataManagement";

const countriesUrl = apiService.getUrl("/cities?page_size=200");

const makeContainer = (i) => ({
  cases: i.cases,
  deaths: i.deaths,
  id: i.country,
});

const aggregateFunc = (a, b) => ({
  cases: a["cases"] + b["cases"],
  deaths: a["deaths"] + b["deaths"],
  id: a.id,
});

class CovidCasesStore {
  data = {};
  loading = false;

  constructor() {
    makeAutoObservable(this);
  }

  fetchData = () => {
    this.loading = true;
    cacheProvider.get(countriesUrl).then(
      action((payload) => {
        this.data = this.reduceData(payload);
        this.loading = false;
      })
    );
  };

  reduceData = (payload) => {
    return aggregateByKey(
      payload.data,
      "country_code",
      makeContainer,
      aggregateFunc
    );
  };
}

export { CovidCasesStore };
