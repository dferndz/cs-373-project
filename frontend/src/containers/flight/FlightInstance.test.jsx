import React from "react";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";
import { StoreProvider } from "../../stores/context";
import { FlightInstance } from "./FlightInstance";
import { flights } from "../../assets/data";

jest.mock("react-router", () => ({
  ...jest.requireActual("react-router"),
  useParams: () => ({ id: "1" }),
}));

jest.mock("../../stores/FlightStore", () => ({
  ...jest.requireActual("../../stores/FlightStore"),
  flightStore: {
    flights: jest.requireActual("../../assets/data").flights,
    pagination: {
      currentPage: 1,
      totalPages: 1,
      totalInstances: 1,
      pageSize: 1,
    },
    loading: false,
    searchQuery: "",
    instance: jest.requireActual("../../assets/data").flights[0],
    loadingInstance: false,
    getFlight: jest.fn(),
  },
}));

describe("flight instance", () => {
  test("it renders a page", () => {
    const { element } = renderWithHistory(
      <StoreProvider>
        <FlightInstance />
      </StoreProvider>,
      ["/flights/1"]
    );

    expect(
      element.getByText(flights[0].departure_city_name)
    ).toBeInTheDocument();
    expect(element.getByText(flights[0].arrival_city_name)).toBeInTheDocument();
    expect(element.getByText(flights[0].service_class)).toBeInTheDocument();
  });
});
