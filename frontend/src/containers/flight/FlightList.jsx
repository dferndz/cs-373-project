import React, { useEffect } from "react";
import { Box, Container, Grid, Typography } from "@mui/material";
import { useStores } from "../../stores/useStores";
import { observer } from "mobx-react";
import { Search } from "../country/components/Search";
import { Paginator } from "../../components/pagination";
import { FlightCard } from "./components/FlightCard.jsx";
import { CircleLoad } from "../../components/suspense/CircleLoad";
import { FilterHeader } from "../../components/filters/FilterHeader";
import { FlightFilterData } from "../../assets/data/filterData.js";
import qs from "query-string";
import { useLocation } from "react-router";

const FlightList = observer(() => {
  const { flightStore } = useStores();
  const { search } = useLocation();

  useEffect(() => {
    flightStore.setFilters(qs.parse(search));
  }, [search]);
  return (
    <Container>
      <Box mt={4} ml={3} mb={4}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Search
              title="Plan a Flight."
              placeholder="Search for a flight number or city"
            />
          </Grid>
          <Grid item xs={12} mt={-3}>
            <FilterHeader filterData={FlightFilterData} />
          </Grid>
          {flightStore.loading ? (
            <Container>
              <CircleLoad height="70vh" />
            </Container>
          ) : (
            <React.Fragment>
              <Grid item container spacing={2}>
                <Grid item xs={12}>
                  <Paginator pagination={flightStore.pagination} />
                </Grid>

                {flightStore.flights.length > 0 ? (
                  flightStore.flights.map((flight, index) => (
                    <Grid key={index} container item xs={12}>
                      <FlightCard flight={flight} />
                    </Grid>
                  ))
                ) : (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    style={{ minHeight: "35vh" }}
                  >
                    <Typography>No records found</Typography>
                  </Grid>
                )}
                {}

                <Grid item xs={12}>
                  <Paginator pagination={flightStore.pagination} />
                </Grid>
              </Grid>
            </React.Fragment>
          )}
        </Grid>
      </Box>
    </Container>
  );
});

export { FlightList };
