import { React, useEffect } from "react";
import { Box, Container, Grid, Typography, Divider } from "@mui/material";
import { useParams } from "react-router";
import { useStores } from "../../stores/useStores";
import { observer } from "mobx-react";
import { SegmentCard } from "./components/SegmentCard.jsx";
import { SegmentMap } from "./components/SegmentMap";
import { SimpleClickableCard } from "../../components/SimpleClickableCard";
import { CircleLoad } from "../../components/suspense/CircleLoad";

const FlightInstance = observer(() => {
  const { id } = useParams();
  const { flightStore } = useStores();
  const flight = flightStore.instance;
  const formatter = Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    currencyDisplay: "narrowSymbol",
  });

  useEffect(() => {
    flightStore.getFlight(id);
  }, [id, flightStore]);

  if (flightStore.loadingInstance)
    return (
      <Container>
        <CircleLoad height="20vh" />
      </Container>
    );

  if (!flight)
    return (
      <Container>
        <Box ml={3}>
          <h3>404 Not Found</h3>
        </Box>
      </Container>
    );

  return (
    <Container sx={{ marginBottom: "3vh", minHeight: "100vh" }}>
      <Grid container spacing={4}>
        <Grid
          container
          item
          xs={12}
          sx={{ marginTop: "3vh" }}
          alignItems="center"
        >
          <Grid item xs>
            <Typography variant="h3" fontWeight="bold">
              {flight.departure_city_name} - {flight.arrival_city_name}
            </Typography>
            <Typography variant="subtitle1">
              {flight.segments[0].departure_iata} -{" "}
              {flight.segments[flight.segments.length - 1].arrival_iata}
            </Typography>
          </Grid>

          <Grid item xs>
            <Typography variant="h4" sx={{ textAlign: "right" }}>
              {formatter.format(flight.cost_total)}
            </Typography>
            <Typography variant="subtitle1" sx={{ textAlign: "right" }}>
              {flight.service_class}
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Divider />
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <SegmentMap height={500} width="100%" zoom={0} flight={flight} />
        </Grid>

        <Grid container item xs={12} spacing={2} direction="row">
          <Grid item xs={12}>
            <Typography variant="h6" fontWeight="lighter">
              Number of Stops: {flight.segments.length}
            </Typography>
            <Divider />
          </Grid>

          {flight.segments.map((segment, index) => (
            <Grid item key={index} xs={12}>
              <SegmentCard segment={segment} />
            </Grid>
          ))}
        </Grid>

        <Grid container item xs={12} spacing={2} direction="row">
          <Grid item xs={12} md={3}>
            <Typography mt={4} variant="h6" fontWeight="lighter">
              Departure Country
            </Typography>
            <Divider />
            <SimpleClickableCard
              id={flight.departure_country}
              model="countries"
              name={flight.departure_country_name}
              desc={flight.departure_country_continent}
              img={flight.departure_country_flag_url}
            />
          </Grid>

          <Grid item xs={12} md={3}>
            <Typography mt={4} variant="h6" fontWeight="lighter">
              Arrival Country
            </Typography>
            <Divider />
            <SimpleClickableCard
              id={flight.arrival_country}
              model="countries"
              name={flight.arrival_country_name}
              desc={flight.arrival_country_continent}
              img={flight.arrival_country_flag_url}
            />
          </Grid>

          <Grid item xs={12} md={3}>
            <Typography mt={4} variant="h6" fontWeight="lighter">
              Departure City
            </Typography>
            <Divider />
            <SimpleClickableCard
              id={flight.departure_city}
              model="cities"
              name={flight.departure_city_name}
              desc={flight.departure_country_name}
              img={flight.departure_city_pic_url}
            />
          </Grid>

          <Grid item xs={12} md={3}>
            <Typography mt={4} variant="h6" fontWeight="lighter">
              Arrival City
            </Typography>
            <Divider />
            <SimpleClickableCard
              id={flight.arrival_city}
              model="cities"
              name={flight.arrival_city_name}
              desc={flight.arrival_country_name}
              img={flight.arrival_city_pic_url}
            />
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
});

export { FlightInstance };
