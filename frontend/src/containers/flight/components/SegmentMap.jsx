import React from "react";
import GoogleMapReact from "google-map-react";

const GOOGLE_MAPS_KEY = process.env.REACT_APP_GOOGLE_MAPS_API_KEY || "";

// Adapted From https://github.com/Dooffy/google-map-react-polyline-example
var colors = ["#f44336", "#2196f3", "#4caf50", "#ff9800"];
function PlotSegment(map, maps, flight) {
  flight.segments.forEach((segment, index) => {
    let lineSegment = new maps.Polyline({
      geodesic: true,
      path: [
        { lat: segment.departure_lat, lng: segment.departure_lng },
        { lat: segment.arrival_lat, lng: segment.arrival_lng },
      ],
      strokeWeight: 5,
      strokeOpacity: 0.5,
      strokeColor: colors[Math.floor(index % colors.length)],
    });
    lineSegment.setMap(map);
    console.log(segment);
  });
}

const SegmentMap = ({ height, width, zoom, flight }) => {
  return (
    <div style={{ height: height, width: width }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
        defaultCenter={[
          flight.segments[0].departure_lat,
          flight.segments[0].departure_lng,
        ]}
        defaultZoom={zoom}
        yesIWantToUseGoogleMapApiInternals={true}
        onGoogleApiLoaded={({ map, maps }) => PlotSegment(map, maps, flight)}
      ></GoogleMapReact>
    </div>
  );
};

export { SegmentMap };
