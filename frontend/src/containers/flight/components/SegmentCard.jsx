import React from "react";
import { Grid, Avatar, Paper, Typography } from "@mui/material";

const SegmentCard = ({ segment }) => {
  return (
    <Paper
      elevation={2}
      sx={{
        width: "100%",
        minHeight: "10vh",
        display: "flex",
        alignItems: "center",
        margin: "auto",
      }}
    >
      <Grid
        container
        spacing={1}
        direction="row"
        justifyContent="space-around"
        alignItems="center"
        data-testid="segment-list-card"
      >
        <Grid item xs={0} md={0}>
          <Avatar src={segment.oper_carrier_logo} />
        </Grid>
        <Grid item xs={10} md={5}>
          <Typography variant="body1" fontWeight="bold">
            {segment.departure_name} - {segment.arrival_name}
          </Typography>
          <Typography variant="subtitle2">
            {segment.departure_iata} - {segment.arrival_iata}
          </Typography>
        </Grid>
        <Grid item xs={6} md={2}>
          <Typography variant="body2">
            {new Date(segment.departure_time).toLocaleString()} CDT
          </Typography>
          <Typography variant="subtitle2">
            Duration: {String(segment.duration).substring(2)}
          </Typography>
        </Grid>
        <Grid item xs={6} md={2}>
          <Typography variant="body1">
            {segment.oper_carrier_code}
            {segment.flight_num}
          </Typography>
        </Grid>
      </Grid>
    </Paper>
  );
};

export { SegmentCard };
