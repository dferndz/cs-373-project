import React from "react";
import { TextField, Typography, Box, Divider } from "@mui/material";

const Search = ({ title, onTextChange, value }) => {
  return (
    <React.Fragment>
      <Typography
        variant="h4"
        textAlign="center"
        fontWeight="lighter"
        mb={4}
        mt={2}
      >
        {title}
      </Typography>
      <Box mb={4}>
        <TextField
          data-testid="flight-search-text-entry"
          value={value}
          name="search"
          autoComplete="off"
          onChange={onTextChange}
          fullWidth
          placeholder="Search for a flight number, airport, or destination."
        />
      </Box>
      <Divider />
    </React.Fragment>
  );
};

export { Search };
