import React, { useCallback } from "react";
import { Grid, Avatar, Paper, Typography, Button } from "@mui/material";
import { useNavigate } from "react-router";
import { default as HW } from "../../../components/highlighter/HighlightWrapper";

const FlightCard = ({ flight }) => {
  const navigate = useNavigate();
  const formatter = Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    currencyDisplay: "narrowSymbol",
  });

  const handleClick = useCallback(() => {
    navigate(`/flights/${flight.id}`);
  }, [navigate, flight.id]);

  return (
    <Button
      component="span"
      sx={{ width: "100%", textTransform: "none" }}
      onClick={handleClick}
    >
      <Paper
        elevation={1}
        sx={{
          width: "100%",
          height: "auto",
          minHeight: "10vh",
          alignItems: "center",
        }}
      >
        <Grid
          container
          spacing={1}
          direction="row"
          justifyContent="center"
          alignItems="center"
          data-testid="flight-list-card"
          mt={1}
        >
          <Grid item xs={3} md={1}>
            <Avatar alt="airline" src={flight.segments[0].oper_carrier_logo} />
          </Grid>
          <Grid item xs={7} md={3}>
            <Typography variant="body1" fontWeight="bold">
              <HW
                text={`${flight.departure_city_name} - ${flight.arrival_city_name}`}
              />
            </Typography>
            <Typography variant="subtitle2">
              <HW
                text={`${flight.segments[0].departure_iata} - ${
                  flight.segments[flight.segments.length - 1].arrival_iata
                }`}
              />
            </Typography>
          </Grid>
          <Grid item xs={6} md={2}>
            <Typography variant="body1">
              <HW text={`${flight.service_class}`} />
            </Typography>
            <Typography variant="subtitle2">
              <HW text={`Segments: ${flight.segments.length}`} />
            </Typography>
          </Grid>
          <Grid item xs={4} md={1}>
            <Typography variant="body1">
              <HW
                text={`${flight.segments[0].carrier_code}${flight.segments[0].flight_num}`}
              />
            </Typography>
          </Grid>
          <Grid item xs={6} md={2}>
            <Typography variant="body2">
              <HW
                text={`${new Date(flight.departure_time).toLocaleString()} CDT`}
              />
            </Typography>
            <Typography variant="subtitle2">
              <HW text={`Duration: ${flight.duration.substring(2)}`} />
            </Typography>
          </Grid>
          <Grid item xs={4} md={2}>
            <Typography variant="body1">
              <HW text={formatter.format(flight.cost_total)} />
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </Button>
  );
};

export { FlightCard };
