import React from "react";
import { theme } from "../../theme";
import { Box, Typography, Button, ThemeProvider } from "@mui/material";

// image from https://www.occupationwild.com/austin-adventures
//  was marked creative commons.
import Image from "../../assets/images/HeroImage.jpg";
import { useNavigate } from "react-router";

const styles = {
  heroContainer: {
    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(${Image})`,
    height: "80vh",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    position: "relative",
  },

  heroText: {
    textAlign: "center",
    position: "absolute",
    top: "50%",
    left: "50%",
    color: "#fff",
    transform: "translate(-50%, -50%)",
  },

  heroButton: {
    display: "block",
    margin: "5vh auto",
  },
};

const Hero = () => {
  const navigate = useNavigate();
  return (
    <ThemeProvider theme={theme}>
      <Box style={styles.heroContainer}>
        <Box style={styles.heroText}>
          <Typography variant="h1">Travel Safe.</Typography>
          <Typography variant="p">Tourist, COVID, and Flight Data.</Typography>
          <Button
            variant="contained"
            style={styles.heroButton}
            onClick={() => {
              navigate(`/about/`);
            }}
          >
            About Us
          </Button>
        </Box>
      </Box>
    </ThemeProvider>
  );
};

export { Hero };
