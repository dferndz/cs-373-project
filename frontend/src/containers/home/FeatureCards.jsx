import React from "react";
import { theme } from "../../theme";
import {
  Container,
  Grid,
  Typography,
  ThemeProvider,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Box,
} from "@mui/material";
import { useNavigate } from "react-router";

const styles = {
  FeaturesContainer: {
    padding: "5vh 10px 8vh 10px",
  },

  FeaturesText: {
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    margin: "0 0 5vh 0",
  },
};

const FeatureCards = () => {
  const navigate = useNavigate();
  return (
    <ThemeProvider theme={theme}>
      <Container style={styles.FeaturesContainer}>
        <Typography variant="h5" component="div" style={styles.FeaturesText}>
          Features
        </Typography>

        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          <Grid item xs={2} sm={4} md={4}>
            <Card sx={{ maxWidth: 345 }}>
              <CardActionArea
                id="splash-country-link"
                onClick={() => {
                  navigate(`/countries/`);
                }}
              >
                <CardMedia
                  component="img"
                  height="200"
                  image="https://miapartner.com/wp-content/uploads/2019/01/national-flags-of-the-european-countries-against-PV4PP3H-e1547606304979.jpg"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    Countries
                  </Typography>
                  <Box sx={{ minHeight: "10vh" }}>
                    <Typography variant="body2" color="text.secondary">
                      Search through our list of countries to get interesting
                      information as well as local temperature.
                    </Typography>
                  </Box>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>

          <Grid item xs={2} sm={4} md={4}>
            <Card sx={{ maxWidth: 345 }}>
              <CardActionArea
                onClick={() => {
                  navigate(`/cities/`);
                }}
              >
                <CardMedia
                  component="img"
                  height="200"
                  image="https://cdn.pixabay.com/photo/2016/10/14/17/32/dallas-1740681_1280.jpg"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    Cities
                  </Typography>
                  <Box sx={{ minHeight: "10vh" }}>
                    <Typography variant="body2" color="text.secondary">
                      Planning a trip to an unfamiliar city? View our city pages
                      to view important COVID information.
                    </Typography>
                  </Box>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>

          <Grid item xs={2} sm={4} md={4}>
            <Card sx={{ maxWidth: 345 }}>
              <CardActionArea
                onClick={() => {
                  navigate(`/flights/`);
                }}
              >
                <CardMedia
                  component="img"
                  height="200"
                  image="https://cdn.pixabay.com/photo/2020/10/14/14/39/airplane-5654535_1280.jpg"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    Flights
                  </Typography>
                  <Box sx={{ minHeight: "10vh" }}>
                    <Typography variant="body2" color="text.secondary">
                      Search through flights to plan your next trip abroad.
                    </Typography>
                  </Box>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </ThemeProvider>
  );
};

export { FeatureCards };
