import { React } from "react";
import { theme } from "../../theme";
import { Box } from "@mui/material";
import { ThemeProvider } from "@emotion/react";
import { Hero } from "./Hero";
import { FeatureCards } from "./FeatureCards";

const HomePage = () => {
  return (
    <ThemeProvider theme={theme}>
      <Box>
        <Hero />
        <FeatureCards />
      </Box>
    </ThemeProvider>
  );
};

export { HomePage };
