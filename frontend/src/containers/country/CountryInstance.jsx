import React, { useEffect, useMemo, useState } from "react";
import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import { useLocation, useParams } from "react-router";
import { CountryInfoHeader } from "./components/CountryInfoHeader";
import { useStores } from "../../stores/useStores";
import { observer } from "mobx-react";
import { Map } from "../../components/maps/Map";
import { Sidebar } from "../../components/sidebar";
import { CountryInfo } from "./components/CountryInfo";
import { Thermometer } from "../../components/thermometer";
import { FlightCard } from "../flight/components/FlightCard";
import { CountryCityList } from "./components/CountryCityList";
import { CircleLoad } from "../../components/suspense/CircleLoad";
import { FlightStore } from "../../stores/FlightStore";
import qs from "query-string";
import { Paginator } from "../../components/pagination";
import { CurrencyExchangeTable } from "../../components/currency";

const CountryInstance = observer(() => {
  const { id } = useParams();
  const { countryStore } = useStores();
  const [flightStore] = useState(
    () => new FlightStore(`countries/${id}/flights`)
  );
  const { search } = useLocation();
  const params = useMemo(() => qs.parse(search), [search]);
  const country = countryStore.instance;

  useEffect(() => {
    flightStore.setFilters(params);
  }, [params]);

  useEffect(() => {
    countryStore.getCountry(id);
  }, [id]);

  if (countryStore.loadingInstance)
    return (
      <Container>
        <CircleLoad height="20vh" />
      </Container>
    );

  if (!country)
    return (
      <Container>
        <Box ml={3}>
          <h3>404 Not Found</h3>
        </Box>
      </Container>
    );

  return (
    <Container>
      <Box mb={4}>
        <CountryInfoHeader country={country} />

        <Grid container spacing={4} mt>
          <Grid item sm={4}>
            <Sidebar ml={3}>
              <CountryInfo country={country} />
            </Sidebar>
            <Sidebar ml={3} mt={2}>
              <CurrencyExchangeTable rates={country.exchange_rates} />
            </Sidebar>
            <Sidebar ml={3} mt={2}>
              <Thermometer
                temp={{
                  average: country.temp,
                  high: country.temp_max,
                  low: country.temp_min,
                  feelsLike: country.temp_feels_like,
                }}
                air={country.air_quality_index}
                ml={4}
                mt={4}
                mb={4}
              />
            </Sidebar>
            <CountryCityList ml={3} xs={11} countryId={country.id} />
          </Grid>
          <Grid item sm={8}>
            <Typography
              fontSize={18}
              dangerouslySetInnerHTML={{
                __html: country.description,
              }}
            />
            <Map
              location={{
                lat: country.geoData.lat,
                lng: country.geoData.lon,
              }}
              height={300}
              width="100%"
              zoom={3}
            />

            {flightStore.flights.length > 0 ? (
              <React.Fragment>
                <Typography mt={4} variant="h5" fontWeight="lighter">
                  Flights
                </Typography>
                <Divider />
                <Paginator
                  mt={2}
                  showPageSize={false}
                  pagination={flightStore.pagination}
                />
                {flightStore.flights.map((flight, index) => (
                  <FlightCard flight={flight} key={index} />
                ))}
              </React.Fragment>
            ) : (
              <Typography mt={4} variant="h5" fontWeight="lighter">
                No flights found
              </Typography>
            )}
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
});

export { CountryInstance };
