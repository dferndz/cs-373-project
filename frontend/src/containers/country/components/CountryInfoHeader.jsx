import React from "react";
import {
  Box,
  Button,
  Card,
  CardHeader,
  CardMedia,
  Container,
} from "@mui/material";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import { useGoBack } from "../../../hooks/useGoBack";

const CountryInfoHeader = ({ country }) => {
  const handleGoBack = useGoBack();

  return (
    <Container>
      <Box mt={4}>
        <Box mb={2}>
          <Button onClick={handleGoBack} variant="outlined">
            <ArrowBackIosNewIcon /> Go back
          </Button>
        </Box>
        <Card>
          <CardMedia
            component="img"
            height="300"
            image={country.pic_url}
            alt="Paella dish"
          />
          <CardHeader
            title={country.name}
            subheader={`${country.subregion}, ${country.continent}`}
          />
        </Card>
      </Box>
    </Container>
  );
};

export { CountryInfoHeader };
