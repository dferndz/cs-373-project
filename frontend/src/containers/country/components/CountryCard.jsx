import React, { useCallback } from "react";
import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Typography,
  CardActionArea,
} from "@mui/material";
import { useNavigate } from "react-router";
import { default as HW } from "../../../components/highlighter/HighlightWrapper";

const CountryCard = ({ country }) => {
  const navigate = useNavigate();
  const formatter = Intl.NumberFormat("en", {
    maximumSignificantDigits: 3,
    notation: "compact",
  });

  const handleClick = useCallback(() => {
    navigate(`/countries/${country.id}`);
  }, [navigate, country.id]);

  return (
    <Card data-testid="country-list-card">
      <CardActionArea onClick={handleClick} sx={{ height: "100%" }}>
        <CardHeader
          title={<HW text={country.name} />}
          titleTypographyProps={{ variant: "body1" }}
          subheader={<HW text={country.continent} />}
          subheaderTypographyProps={{ variant: "caption" }}
          avatar={<Avatar src={country.flag_url} />}
          sx={{ minHeight: "70px" }}
        />
        <CardMedia
          component="img"
          height="194"
          image={country.pic_url}
          alt="Paella dish"
        />
        <CardContent>
          <Typography variant="caption">
            <HW text={`Air quality: ${country.air_quality_index}`} />
            <br />
            <HW text={`Average temperature: ${country.temp_average} C`} />
            <br />
            <HW text={`Currency: ${country.currency}`} />
            <br />
            <HW text={`Tourists: ${formatter.format(country.tourists)}`} />
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export { CountryCard };
