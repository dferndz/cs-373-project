import React, { useEffect, useMemo } from "react";
import { Grid, Typography, Divider } from "@mui/material";
import { observer } from "mobx-react";
import { SimpleClickableCard } from "../../../components/SimpleClickableCard";
import { useStores } from "../../../stores/useStores";

const CountryCityList = observer(
  ({ header, notFoundHeader, exclude, countryId, ...props }) => {
    const { cityStore } = useStores();
    const { citiesForCountry } = cityStore;
    const excludeCities = useMemo(() => new Set(exclude), [exclude]);
    const cities = useMemo(
      () => citiesForCountry.filter((city) => !excludeCities.has(city.id)),
      [citiesForCountry, excludeCities]
    );

    useEffect(() => {
      if (countryId) {
        cityStore.getCitiesForCountry(countryId);
      }
    }, [cityStore, countryId]);

    return (
      <Grid container item>
        {cities.length > 0 ? (
          <React.Fragment>
            <Grid item {...props}>
              <Typography mt={4} variant="h6" fontWeight="lighter">
                {header || "Cities"}
              </Typography>
              <Divider />
            </Grid>
            {cities.map((city, index) => (
              <Grid item key={index} {...props}>
                <SimpleClickableCard
                  id={city.id}
                  model="cities"
                  name={city.name}
                  desc={"COVID Risk: " + city.risk_level}
                  img={city.pic_url}
                />
              </Grid>
            ))}
          </React.Fragment>
        ) : (
          <Grid item {...props}>
            <Typography mt={4} variant="h6" fontWeight="lighter">
              {notFoundHeader || "No cities found"}
            </Typography>
          </Grid>
        )}
      </Grid>
    );
  }
);

export { CountryCityList };
