import React from "react";
import { Box, Grid, List, ListItem, Typography } from "@mui/material";

const Item = ({ name, value }) => {
  return (
    <ListItem>
      <Grid container>
        <Grid item xs={6}>
          <Typography fontSize={14} variant="h6">
            {name}:
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography fontSize={14} fontWeight="lighter" variant="h6">
            {value}
          </Typography>
        </Grid>
      </Grid>
    </ListItem>
  );
};

const CountryInfo = ({ country, ...props }) => {
  const formatter = Intl.NumberFormat("en", {
    maximumSignificantDigits: 3,
    notation: "compact",
  });

  const floatFormatter = Intl.NumberFormat("en", {
    maximumSignificantDigits: 2,
  });

  return (
    <Box {...props}>
      <Grid container>
        <Grid item md={12}>
          <List>
            <Item name="Capital" value={country.capital} />
            <Item
              name="Flag"
              value={<img alt="flag" width="30%" src={country.flag_url} />}
            />
            <Item
              name="Population"
              value={formatter.format(country.population)}
            />
            <Item
              name="Vaccinated"
              value={`${floatFormatter.format(
                (country.num_vaccinated / country.population) * 100
              )}%`}
            />
            <Item name="Tourists" value={formatter.format(country.tourists)} />
            <Item name="Language" value={country.languages[0]} />
            <Item name="Currency" value={country.currency} />
          </List>
        </Grid>
      </Grid>
    </Box>
  );
};

export { CountryInfo };
