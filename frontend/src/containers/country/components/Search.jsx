import React, { useCallback, useEffect, useMemo, useState } from "react";
import { TextField, Typography, Box, Divider, Button } from "@mui/material";
import { useLocation, useNavigate } from "react-router";
import qs from "query-string";

const Search = ({ title, placeholder }) => {
  const navigate = useNavigate();
  const { pathname, search } = useLocation();
  const params = useMemo(() => qs.parse(search), [search]);
  const [searchQuery, setSearchQuery] = useState();

  const onTextChange = useCallback(
    (event) => {
      setSearchQuery(event.target.value);
    },
    [setSearchQuery]
  );

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      navigate({
        pathname: pathname,
        search: qs.stringify({
          ...params,
          page: 1,
          search: searchQuery,
        }),
      });
    },
    [navigate, pathname, search, searchQuery]
  );

  useEffect(() => {
    if ("search" in params) {
      if (params["search"] !== searchQuery) {
        setSearchQuery(params["search"] || "");
      }
    } else {
      setSearchQuery("");
    }
  }, [params]);

  return (
    <React.Fragment>
      <Typography
        variant="h4"
        textAlign="center"
        fontWeight="lighter"
        mb={4}
        mt={2}
      >
        {title}
      </Typography>
      <Box mb={4}>
        <form onSubmit={handleSubmit}>
          <TextField
            data-testid="country-search-text-entry"
            value={searchQuery || ""}
            name="search"
            autoComplete="off"
            onChange={onTextChange}
            fullWidth
            placeholder={placeholder}
            InputProps={{ endAdornment: <Button type="submit">Search</Button> }}
          />
        </form>
      </Box>
      <Divider />
    </React.Fragment>
  );
};

export { Search };
