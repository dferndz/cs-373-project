import React from "react";
import { CountryCard } from "./CountryCard";
import { renderWithHistory } from "../../../utils/tests/renderWithHistory";
import { countries } from "../../../assets/data";

describe("country card", () => {
  test("renders a card", () => {
    const { element } = renderWithHistory(
      <CountryCard country={countries[0]} />
    );

    expect(element.getByTestId("country-list-card")).toBeInTheDocument();
    expect(element.getByText(countries[0].continent)).toBeInTheDocument();
    // expect(element.getByText(/learn more/i)).toBeInTheDocument();
  });
});
