import React, { useEffect } from "react";
import { Box, Container, Grid, Typography } from "@mui/material";
import { useStores } from "../../stores/useStores";
import { observer } from "mobx-react";
import { CountryCard } from "./components/CountryCard";
import { Search } from "./components/Search";
import { Paginator } from "../../components/pagination";
import { CircleLoad } from "../../components/suspense/CircleLoad";
import { FilterHeader } from "../../components/filters/FilterHeader";
import { CountryFilterData } from "../../assets/data/filterData.js";
import { useLocation } from "react-router";
import qs from "query-string";

const CountryList = observer(() => {
  const { countryStore } = useStores();
  const { search } = useLocation();

  useEffect(() => {
    countryStore.setFilters(qs.parse(search));
  }, [search]);

  return (
    <Container>
      <Box mt={4} ml={3} mb={4}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Search
              title="What country are you visiting next?"
              placeholder="Search for a country"
            />
          </Grid>
          <Grid item xs={12} mt={-3}>
            <FilterHeader filterData={CountryFilterData} />
          </Grid>
          {countryStore.loading ? (
            <Container>
              <CircleLoad height="70vh" />
            </Container>
          ) : (
            <React.Fragment>
              <Grid item xs={12}>
                <Paginator pagination={countryStore.pagination} />
              </Grid>
              {countryStore.countries.length > 0 ? (
                countryStore.countries.map((country, index) => (
                  <Grid key={index} item xs={12} sm={6} md={4} display="flex">
                    <CountryCard country={country} />
                  </Grid>
                ))
              ) : (
                <Grid item xs={12} sm={6} md={4} style={{ minHeight: "35vh" }}>
                  <Typography>No records found</Typography>
                </Grid>
              )}

              <Grid item xs={12}>
                <Paginator pagination={countryStore.pagination} />
              </Grid>
            </React.Fragment>
          )}
        </Grid>
      </Box>
    </Container>
  );
});

export { CountryList };
