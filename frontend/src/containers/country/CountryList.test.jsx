import React from "react";
import { waitFor, screen } from "@testing-library/react";
import { CountryList } from "./ContryList";
import { StoreProvider } from "../../stores/context";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";
import { countryStore } from "../../stores/CountryStore";

jest.mock("../../stores/CountryStore", () => ({
  ...jest.requireActual("../../stores/CountryStore"),
  countryStore: {
    countries: jest.requireActual("../../assets/data").countries,
    pagination: {
      currentPage: 1,
      totalPages: 1,
      totalInstances: 1,
      pageSize: 1,
    },
    loading: false,
    searchQuery: "",
    instance: null,
    loadingInstance: false,
    filters: {},
    setFilters: jest.fn(),
  },
}));

describe("renders a list of countries", () => {
  test("renders a card for all countries", async () => {
    renderWithHistory(
      <StoreProvider>
        <CountryList />
      </StoreProvider>,
      ["/countries"]
    );

    await waitFor(() =>
      expect(countryStore.countries.length).toBeGreaterThan(0)
    );
    countryStore.countries.forEach((country) => {
      expect(screen.getByText(country.name)).toBeInTheDocument();
    });
  });

  test("renders a search element", async () => {
    const { element } = renderWithHistory(
      <StoreProvider>
        <CountryList />
      </StoreProvider>,
      ["/countries"]
    );

    expect(
      element.getByTestId("country-search-text-entry")
    ).toBeInTheDocument();
  });
});
