import React from "react";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";
import { StoreProvider } from "../../stores/context";
import { CountryInstance } from "./CountryInstance";
import { countries } from "../../assets/data";

jest.mock("react-router", () => ({
  ...jest.requireActual("react-router"),
  useParams: () => ({ id: "1" }),
}));

jest.mock("../../stores/CountryStore", () => ({
  ...jest.requireActual("../../stores/CountryStore"),
  countryStore: {
    countries: [],
    pagination: {
      currentPage: 1,
      totalPages: 1,
      totalInstances: 1,
      pageSize: 1,
    },
    loading: false,
    searchQuery: "",
    instance: jest.requireActual("../../assets/data").countries[0],
    loadingInstance: false,
    getCountry: jest.fn(),
  },
}));

describe("country instance", () => {
  test("it renders a page", () => {
    const { element } = renderWithHistory(
      <StoreProvider>
        <CountryInstance />
      </StoreProvider>,
      ["/countries/1"]
    );

    expect(element.getByText(countries[0].name)).toBeInTheDocument();
    expect(element.getByText(countries[0].capital)).toBeInTheDocument();
    expect(element.getByText(countries[0].languages[0])).toBeInTheDocument();
    expect(element.getByText(countries[0].currency)).toBeInTheDocument();
  });
});
