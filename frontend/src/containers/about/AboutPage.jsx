import React from "react";
import { useState, useEffect } from "react";
import { Box, Container, Typography } from "@mui/material";
import { BioCard } from "../../components/about/BioCard";
import { GenericCard } from "../../components/about/GenericCard";

import { bios } from "../../assets/data/bios.js";
import { tools, dataSources, sourceCode } from "../../assets/data/tools.js";
import { CommitIcon, IssuesIcon, TestIcon } from "../../assets/images/icons";

import { CircleLoad } from "../../components/suspense/CircleLoad";
import { cacheProvider } from "./RequestCache";

const CARD_WIDTH = 300;
const CARD_HEIGHT = 250;

const AboutPage = () => {
  const [loading, setLoading] = useState(true);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);

  const getCommits = async () => {
    // adapted from texasvotes.me repository
    // erase initial values
    let total = 0;
    bios.forEach((u) => {
      u.commits = 0;
    });

    const data = await cacheProvider.get(
      `https://gitlab.com/api/v4/projects/33895875/repository/contributors?per_page=100&page=${0}`
    );

    bios.forEach((element) => {
      const { gitlabNames } = element;

      let numCommits = data
        .filter((v) => gitlabNames.includes(v.name))
        .reduce((total, current) => total + current.commits, 0);

      element.commits = numCommits;
      total += numCommits;
    });

    return total;
  };

  const getIssues = async () => {
    // adapted from texasvotes.me repository
    // reset all commits to zero
    let total = 0;
    bios.forEach((u) => {
      u.issues = 0;
    });

    // need to do pagination if we have more than 100 issues
    let page = 1;
    let responseLength = 0;
    do {
      const data = await cacheProvider.get(
        `https://gitlab.com/api/v4/projects/33895875/issues?per_page=100&page=${page}`
      );
      responseLength = data.length;

      let pageTotal = 0;
      bios.forEach((element) => {
        const { gitlabNames } = element;

        let userIssues = data.filter((v) =>
          gitlabNames.includes(v.author.name)
        ).length;

        element.issues += userIssues;
        pageTotal += userIssues;
      });

      total += pageTotal;
      page++;
    } while (responseLength === 20); // iterate until page length is < 100

    return total;
  };

  const getTests = () => {
    return bios.reduce((prev, currentUser) => prev + currentUser.tests, 0);
  };

  const getAll = async () => {
    const numCommits = await getCommits();
    const numIssues = await getIssues();
    const numTests = await getTests();

    setTotalCommits(numCommits);
    setTotalIssues(numIssues);
    setTotalTests(numTests);
  };

  useEffect(() => {
    getAll().then(() => {
      setLoading(false);
    });
  }, []);

  return (
    <Container>
      <Typography align="center" variant="h3" padding={3} fontWeight="bold">
        About Us
      </Typography>
      <Typography variant="body1" sx={{ pt: 1 }}>
        The SafeTravels team has developed a product that allows you to quickly
        look at information from various countries and popular tourist cities,
        including current travel restrictions due to COVID-19. You can also see
        flights to any destination that suits your interest. We aim to ensure
        your next trip is as safe and stress-free as possible by giving this
        information all in one site.
      </Typography>

      <Typography align="center" variant="h3" padding={3} fontWeight="bold">
        Our Team
      </Typography>
      <Box
        sx={{
          minHeight: 500,
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        {!loading ? (
          bios.map((element, idx) => {
            return <BioCard {...element} key={`${idx}-bios`} />;
          })
        ) : (
          <CircleLoad height="5vh" />
        )}
      </Box>
      <Typography align="center" variant="h3" padding={3} fontWeight="bold">
        GitLab Statistics
      </Typography>
      <Box
        sx={{
          minHeight: 200,
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        {!loading ? (
          <Box display="hidden">
            <GenericCard
              title="Commits"
              height={150}
              width={200}
              description={`${totalCommits}`}
              icon={CommitIcon}
            />
            <GenericCard
              title="Issues"
              height={150}
              width={200}
              description={`${totalIssues}`}
              icon={IssuesIcon}
            />
            <GenericCard
              title="Tests"
              height={150}
              width={200}
              description={`${totalTests}`}
              icon={TestIcon}
            />
          </Box>
        ) : (
          <CircleLoad height="5vh" />
        )}
      </Box>
      <Typography align="center" variant="h3" padding={3} fontWeight="bold">
        Data Sources
      </Typography>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        {dataSources.map((element, idx) => {
          return (
            <GenericCard
              {...element}
              width={CARD_WIDTH}
              height={CARD_HEIGHT}
              key={`${idx}-tools`}
            />
          );
        })}
      </Box>
      <Typography align="center" variant="h3" padding={3} fontWeight="bold">
        Tools Utilized
      </Typography>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        {tools.map((element, idx) => {
          return (
            <GenericCard
              {...element}
              width={CARD_WIDTH}
              height={CARD_HEIGHT}
              key={`${idx}-tools`}
            />
          );
        })}
      </Box>
      <Typography align="center" variant="h3" padding={3} fontWeight="bold">
        Source Code and API Documentation
      </Typography>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        {sourceCode.map((element, idx) => {
          return (
            <GenericCard
              {...element}
              width={CARD_WIDTH}
              height={CARD_HEIGHT}
              key={`${idx}-tools`}
            />
          );
        })}
      </Box>
    </Container>
  );
};

export { AboutPage };
