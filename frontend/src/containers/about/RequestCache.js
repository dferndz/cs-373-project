import axios from "axios";

class RequestCache {
  constructor(ms) {
    this.threshold = ms;
  }

  getItem = (key) => {
    const item = localStorage.getItem(key);
    if (!item) return null;

    try {
      return JSON.parse(item);
    } catch (e) {
      return null;
    }
  };

  setItem = (key, dict) => {
    localStorage.setItem(key, JSON.stringify(dict));
  };

  updatePayload = (url, payload) => {
    this.setItem(url, {
      payload: payload,
      timestamp: Date.now(),
    });
  };

  getPayload = (url) => {
    const item = this.getItem(url);
    if (!item) return null;

    const now = Date.now();
    const { payload, timestamp } = item;

    if (timestamp + this.threshold > now) {
      return payload;
    }

    return null;
  };

  get = async (url) => {
    const payload = this.getPayload(url);

    if (payload !== null) return payload;

    const response = await axios.get(url);
    this.updatePayload(url, response.data);
    return response.data;
  };
}

// cache payloads for 5 minutes
const cacheProvider = new RequestCache(1000 * 60 * 5);
export { cacheProvider, RequestCache };
