import React, { useEffect } from "react";
import { Container, Grid, Box, Typography } from "@mui/material";
import { observer } from "mobx-react";
import { Search } from "../country/components/Search";
import { useStores } from "../../stores/useStores";
import { useLocation } from "react-router";
import qs from "query-string";
import { CircleLoad } from "../../components/suspense/CircleLoad";
import { CityCard } from "../city/components/CityCard";
import { CountryCard } from "../country/components/CountryCard";
import { FlightCard } from "../flight/components/FlightCard";
import { ModelSearchCard } from "./components/ModelSearchCard";

const SearchPage = observer(() => {
  const { searchStore } = useStores();
  const { search } = useLocation();

  useEffect(() => {
    searchStore.setFilters(qs.parse(search));
  }, [search]);

  return (
    <Container>
      <Box mt={4} ml={3} mb={4}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Search
              title="What are you looking for in SafeTravels?"
              placeholder="Search SafeTravels for countries, cities, and flights"
            />
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="h4"
              textAlign="left"
              fontWeight="lighter"
              mb={4}
              mt={2}
            >
              Countries
            </Typography>
          </Grid>

          {searchStore.loading ? (
            <Container>
              <CircleLoad height="70vh" />
            </Container>
          ) : (
            <React.Fragment>
              {searchStore.searchResults["countries"] != undefined &&
              searchStore.searchResults["countries"].length > 0 ? (
                searchStore.searchResults["countries"]
                  .filter((item, idx) => idx < 5)
                  .map((country, index) => (
                    <Grid key={index} item xs={12} sm={6} md={4}>
                      <CountryCard country={country} />
                    </Grid>
                  ))
              ) : (
                <Grid item xs={12} sm={6} md={4} style={{ minHeight: "35vh" }}>
                  <Typography>No records found</Typography>
                </Grid>
              )}
              {searchStore.searchResults["countries"] != undefined &&
              searchStore.searchResults["countries"].length > 0 &&
              searchStore.searchResults["countries"].length > 5 ? (
                <Grid key={5} item xs={12} sm={6} md={4}>
                  <ModelSearchCard
                    pathname="countries"
                    params={qs.parse(search)}
                  ></ModelSearchCard>
                </Grid>
              ) : (
                <></>
              )}
            </React.Fragment>
          )}
          <Grid item xs={12}>
            <Typography
              variant="h4"
              textAlign="left"
              fontWeight="lighter"
              mb={4}
              mt={2}
            >
              Cities
            </Typography>
          </Grid>

          {searchStore.loading ? (
            <Container>
              <CircleLoad height="70vh" />
            </Container>
          ) : (
            <React.Fragment>
              {searchStore.searchResults["cities"] != undefined &&
              searchStore.searchResults["cities"].length > 0 ? (
                searchStore.searchResults["cities"]
                  .filter((item, idx) => idx < 5)
                  .map((city, index) => (
                    <Grid key={index} item xs={12} sm={6} md={4}>
                      <CityCard city={city} />
                    </Grid>
                  ))
              ) : (
                <Grid item xs={12} sm={6} md={4} style={{ minHeight: "35vh" }}>
                  <Typography>No records found</Typography>
                </Grid>
              )}
              {searchStore.searchResults["cities"] != undefined &&
              searchStore.searchResults["cities"].length > 0 &&
              searchStore.searchResults["cities"].length > 5 ? (
                <Grid key={5} item xs={12} sm={6} md={4}>
                  <ModelSearchCard
                    pathname="cities"
                    params={qs.parse(search)}
                  ></ModelSearchCard>
                </Grid>
              ) : (
                <></>
              )}
            </React.Fragment>
          )}

          <Grid item xs={12}>
            <Typography
              variant="h4"
              textAlign="left"
              fontWeight="lighter"
              mb={4}
              mt={2}
            >
              Flights
            </Typography>
          </Grid>

          {searchStore.loading ? (
            <Container>
              <CircleLoad height="70vh" />
            </Container>
          ) : (
            <React.Fragment>
              {searchStore.searchResults["flights"] != undefined &&
              searchStore.searchResults["flights"].length > 0 ? (
                searchStore.searchResults["flights"]
                  .filter((item, idx) => idx < 5)
                  .map((flight, index) => (
                    <Grid key={index} container item xs={12}>
                      <FlightCard flight={flight} />
                    </Grid>
                  ))
              ) : (
                <Grid item xs={12} sm={6} md={4} style={{ minHeight: "35vh" }}>
                  <Typography>No records found</Typography>
                </Grid>
              )}
              {searchStore.searchResults["flights"] != undefined &&
              searchStore.searchResults["flights"].length > 0 &&
              searchStore.searchResults["flights"].length > 5 ? (
                <Grid key={5} item xs={12} sm={6} md={4}>
                  <ModelSearchCard
                    pathname="flights"
                    params={qs.parse(search)}
                  ></ModelSearchCard>
                </Grid>
              ) : (
                <></>
              )}
            </React.Fragment>
          )}
        </Grid>
      </Box>
    </Container>
  );
});

export { SearchPage };
