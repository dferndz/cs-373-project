import React, { useCallback } from "react";
import {
  // Avatar,
  Card,
  // CardContent,
  CardHeader,
  // CardMedia,
  // Typography,
  CardActionArea,
} from "@mui/material";
import { useNavigate } from "react-router";
import qs from "query-string";

const ModelSearchCard = ({ pathname, params }) => {
  const navigate = useNavigate();

  const handleClick = useCallback(() => {
    const url = `/${pathname}/?${qs.stringify({ ...params })}`;
    navigate(url);
  }, [navigate]);

  return (
    <Card style={{ height: "145" }} data-testid="-list-card">
      <CardActionArea onClick={handleClick}>
        <CardHeader title={"View more " + pathname + " results"} />
      </CardActionArea>
    </Card>
  );
};

export { ModelSearchCard };
