import React, { useEffect, useMemo, useState } from "react";
import { Box, Container, Grid, Typography } from "@mui/material";
import { useLocation, useParams } from "react-router";
import { CityInfoHeader } from "./components/CityInfoHeader";
import { useStores } from "../../stores/useStores";
import { observer } from "mobx-react";
import { Map } from "../../components/maps/Map";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { FlightCard } from "../flight/components/FlightCard";
import { Divider } from "@mui/material";
import { CircleLoad } from "../../components/suspense/CircleLoad";
import { SimpleClickableCard } from "../../components/SimpleClickableCard";
import { CountryCityList } from "../country/components/CountryCityList";
import { FlightStore } from "../../stores/FlightStore";
import qs from "query-string";
import { Paginator } from "../../components/pagination";

const CityInstance = observer(() => {
  const { id } = useParams();
  const { cityStore } = useStores();
  const city = cityStore.instance;
  const [flightStore] = useState(() => new FlightStore(`cities/${id}/flights`));
  const { search } = useLocation();
  const params = useMemo(() => qs.parse(search), [search]);

  useEffect(() => {
    flightStore.setFilters(params);
  }, [params]);

  useEffect(() => {
    cityStore.getCity(id);
  }, [id, cityStore, flightStore]);

  if (cityStore.loadingInstance)
    return (
      <Container>
        <CircleLoad height="20vh" />
      </Container>
    );

  if (!city)
    return (
      <Container>
        <Box ml={3}>
          <h3>404 Not Found</h3>
        </Box>
      </Container>
    );

  return (
    <Container>
      <Box mb={4}>
        <CityInfoHeader city={city} />

        <Grid container spacing={4} mt>
          <Grid item sm={4}>
            <Typography mt={1} variant="h6" fontWeight="lighter">
              Country
            </Typography>
            <Divider />
            <SimpleClickableCard
              id={city.country}
              name={city.country_name}
              desc={city.country_continent}
              img={city.flag_url}
              model="countries"
            />

            <Typography mt={4} variant="h6" fontWeight="lighter">
              City Statistics
            </Typography>
            <Divider sx={{ mb: 1 }} />

            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 30 }} aria-label="simple table">
                <TableBody>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      City Population
                    </TableCell>
                    <TableCell align="right">{city.population}</TableCell>
                  </TableRow>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      City Temperature
                    </TableCell>
                    <TableCell align="right">{city.temp}</TableCell>
                  </TableRow>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      Risk Level
                    </TableCell>
                    <TableCell align="right">{city.risk_level}</TableCell>
                  </TableRow>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      Total Covid Cases
                    </TableCell>
                    <TableCell align="right">{city.cases}</TableCell>
                  </TableRow>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      Total Covid Deaths
                    </TableCell>
                    <TableCell align="right">{city.deaths}</TableCell>
                  </TableRow>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      Mask Requirement
                    </TableCell>
                    <TableCell align="right">{city.mask_policy}</TableCell>
                  </TableRow>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      Vaccine Requirement
                    </TableCell>
                    <TableCell align="right">{city.vaccine_policy}</TableCell>
                  </TableRow>
                  <TableRow
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      Quarantine Duration
                    </TableCell>
                    <TableCell align="right">{city.quarantine_dur}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>

            <CountryCityList
              header="Related cities"
              notFoundHeader="No related cities found"
              exclude={[city.id]}
              ml={3}
              xs={11}
              countryId={city.country}
            />
          </Grid>

          <Grid item sm={8}>
            <Typography variant="h4">Covid Information</Typography>
            <Typography
              fontSize={18}
              dangerouslySetInnerHTML={{
                __html: city.restriction_summary,
              }}
            />
            <Map
              location={{
                lat: city.lat,
                lng: city.lon,
              }}
              height={300}
              width="100%"
              zoom={5}
            />

            <Grid item xs={12}>
              {flightStore.flights.length > 0 ? (
                <React.Fragment>
                  <Typography mt={4} variant="h5" fontWeight="lighter">
                    Flights
                  </Typography>
                  <Divider />
                  <Paginator
                    mt={2}
                    showPageSize={false}
                    pagination={flightStore.pagination}
                  />
                  {flightStore.flights.map((flight, index) => (
                    <FlightCard flight={flight} key={index} />
                  ))}
                </React.Fragment>
              ) : (
                <Typography mt={4} variant="h5" fontWeight="lighter">
                  No flights found
                </Typography>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
});

export { CityInstance };
