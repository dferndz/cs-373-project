import React from "react";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";
import { StoreProvider } from "../../stores/context";
import { CityInstance } from "./CityInstance";
import { cities } from "../../assets/data";

jest.mock("react-router", () => ({
  ...jest.requireActual("react-router"),
  useParams: () => ({ id: "1" }),
}));

jest.mock("../../stores/CityStore", () => ({
  ...jest.requireActual("../../stores/CityStore"),
  cityStore: {
    cities: [],
    pagination: {
      currentPage: 1,
      totalPages: 1,
      totalInstances: 1,
      pageSize: 1,
    },
    loading: false,
    searchQuery: "",
    citiesForCountry: [jest.requireActual("../../assets/data").cities[0]],
    instance: jest.requireActual("../../assets/data").cities[0],
    city: jest.requireActual("../../assets/data").cities[0],
    loadingInstance: false,
    getCity: jest.fn(),
    getCitiesForCountry: jest.fn(),
  },
}));

describe("city instance", () => {
  test("it renders a page", () => {
    const { element } = renderWithHistory(
      <StoreProvider>
        <CityInstance />
      </StoreProvider>,
      ["/cities/1"]
    );

    // expect(element.getByText(cities[0].country)).toBeInTheDocument();
    expect(element.getByText(cities[0].quarantine_dur)).toBeInTheDocument();
    expect(element.getByText(cities[0].country_continent)).toBeInTheDocument();
    expect(element.getByText(cities[0].cases)).toBeInTheDocument();
    expect(element.getByText(cities[0].deaths)).toBeInTheDocument();
    // expect(element.getByText(cities[0].flag_url)).toBeInTheDocument();
  });
});
