import React from "react";
import { waitFor, screen } from "@testing-library/react";
import { CityList } from "./CityList";
import { StoreProvider } from "../../stores/context";
import { renderWithHistory } from "../../utils/tests/renderWithHistory";
import { cityStore } from "../../stores/CityStore";

jest.mock("../../stores/CityStore", () => ({
  ...jest.requireActual("../../stores/CityStore"),
  cityStore: {
    cities: jest.requireActual("../../assets/data").cities,
    pagination: {
      currentPage: 1,
      totalPages: 1,
      totalInstances: 1,
      pageSize: 1,
    },
    loading: false,
    searchQuery: "",
    instance: null,
    loadingInstance: false,
    filters: {},
    setFilters: jest.fn(),
  },
}));

describe("renders a list of cities", () => {
  test("renders a card for all cities", async () => {
    renderWithHistory(
      <StoreProvider>
        <CityList />
      </StoreProvider>,
      ["/cities"]
    );

    await waitFor(() => expect(cityStore.cities.length).toBeGreaterThan(0));
    cityStore.cities.forEach((city) => {
      expect(screen.getByText(city.name)).toBeInTheDocument();
    });
  });
});
