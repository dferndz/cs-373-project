import React, { useCallback } from "react";
import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Typography,
  CardActionArea,
} from "@mui/material";
import { useNavigate } from "react-router";
import { default as HW } from "../../../components/highlighter/HighlightWrapper";

const CityCard = ({ city }) => {
  const navigate = useNavigate();
  //   const formatter = Intl.NumberFormat("en", {
  //     maximumSignificantDigits: 3,
  //     notation: "compact",
  //   });

  const handleClick = useCallback(() => {
    navigate(`/cities/${city.id}`);
  }, [navigate, city.id]);

  return (
    <Card data-testid="city-list-card">
      <CardActionArea onClick={handleClick} sx={{ height: "100%" }}>
        <CardHeader
          title={<HW text={city.name} />}
          titleTypographyProps={{ variant: "body1" }}
          subheader={<HW text={city.country_name} />}
          subheaderTypographyProps={{ variant: "caption" }}
          avatar={<Avatar src={city.flag_url} />}
          sx={{ minHeight: "70px" }}
        />
        <CardMedia
          component="img"
          height="194"
          image={city.pic_url}
          alt="Paella dish"
        />
        <CardContent>
          <Typography variant="caption">
            <HW text={`Risk Level: ${city.risk_level}`} />
            <br />
            <HW text={`Total Covid Deaths: ${city.deaths}`} />
            <br />
            <HW text={`Total Covid Cases: ${city.cases}`} />
            <br />
            <HW text={`Quarantine Duration: ${city.quarantine_dur}`} />
            <br />
            <HW text={`Mask Policy: ${city.mask_policy}`} />
            <br />
            <HW text={`Vaccine Policy: ${city.vaccine_policy}`} />
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export { CityCard };
