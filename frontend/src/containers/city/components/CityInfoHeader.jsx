import React from "react";
import { Box, Button, Card, CardHeader, CardMedia } from "@mui/material";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import { useGoBack } from "../../../hooks/useGoBack";

const CityInfoHeader = ({ city }) => {
  const handleGoBack = useGoBack();

  return (
    <Box mt={4}>
      <Box mb={2}>
        <Button onClick={handleGoBack} variant="outlined">
          <ArrowBackIosNewIcon /> Go back
        </Button>
      </Box>
      <Card>
        <CardMedia
          component="img"
          height="300"
          image={city.pic_url}
          alt="Paella dish"
        />
        <CardHeader title={city.name} subheader={city.country_name} />
      </Card>
    </Box>
  );
};

export { CityInfoHeader };
