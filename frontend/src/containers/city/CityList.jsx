import React, { useEffect } from "react";
import { Box, Container, Grid, Typography } from "@mui/material";
import { useStores } from "../../stores/useStores";
import { observer } from "mobx-react";
import { CityCard } from "./components/CityCard";
import { Search } from "../country/components/Search";
import { Paginator } from "../../components/pagination";
import { CircleLoad } from "../../components/suspense/CircleLoad";
import { FilterHeader } from "../../components/filters/FilterHeader";
import { CityFilterData } from "../../assets/data/filterData.js";
import { useLocation } from "react-router";
import qs from "query-string";

const CityList = observer(() => {
  const { cityStore } = useStores();
  const { search } = useLocation();

  useEffect(() => {
    cityStore.setFilters(qs.parse(search));
  }, [search]);

  return (
    <Container>
      <Box mt={4} ml={3} mb={4}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Search
              title="What city are you looking to go?"
              placeholder="Search cities"
            />
          </Grid>
          <Grid item xs={12} mt={-3}>
            <FilterHeader filterData={CityFilterData} />
          </Grid>
          {cityStore.loading ? (
            <Container>
              <CircleLoad height="70vh" />
            </Container>
          ) : (
            <React.Fragment>
              <Grid item xs={12}>
                <Paginator pagination={cityStore.pagination} />
              </Grid>
              {cityStore.cities.length > 0 ? (
                cityStore.cities.map((city, index) => (
                  <Grid key={index} item xs={12} sm={6} md={4} display="flex">
                    <CityCard city={city} />
                  </Grid>
                ))
              ) : (
                <Grid item xs={12} sm={6} md={4} style={{ minHeight: "35vh" }}>
                  <Typography>No records found</Typography>
                </Grid>
              )}
              {}
              <Grid item xs={12}>
                <Paginator pagination={cityStore.pagination} />
              </Grid>
            </React.Fragment>
          )}
        </Grid>
      </Box>
    </Container>
  );
});

export { CityList };
