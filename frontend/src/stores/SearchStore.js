import { makeAutoObservable, action, reaction } from "mobx";
import { apiService } from "./apiService";
import qs from "query-string";
import { DEFAULT_PAGE_SIZE } from "../constants";

class SearchStore {
  searchResults = [];
  pagination = {
    currentPage: 0,
    totalPages: 0,
    totalInstances: 0,
    pageSize: DEFAULT_PAGE_SIZE,
  };
  loading = true;
  searchQuery = "";

  instance = null;
  loadingInstance = false;
  filters = {};

  constructor() {
    makeAutoObservable(this);
    reaction(
      () => this.filters,
      () => this.fetchSearchResults()
    );
  }

  dispatch = (data) => {
    this.searchResults = data;
  };

  setFilters = (params) => {
    this.filters = params;
  };

  fetchSearchResults = () => {
    this.loading = true;
    const model = this.filters
      ? `search?${qs.stringify(this.filters)}&limit=6`
      : "search?limit=6";

    apiService.getData(model).then(
      action((data) => {
        if (data) {
          this.dispatch(data);
        } else {
          // TODO: handle a possible error
        }
        this.loading = false;
      })
    );
  };

  search = (query) => {
    this.searchQuery = query.toLowerCase();
  };
}

const searchStore = new SearchStore();

export { SearchStore, searchStore };
