import React from "react";
import { createContext } from "react";
import { countryStore } from "./CountryStore";
import { flightStore } from "./FlightStore";
import { cityStore } from "./CityStore";
import { searchStore } from "./SearchStore";
import { flightCostBarChartStore } from "./FlightCostBarChartStore";

const defaultStores = {
  countryStore: countryStore,
  flightStore: flightStore,
  cityStore: cityStore,
  searchStore: searchStore,
  flightCostBarChartStore: flightCostBarChartStore,
};

const storesContext = createContext(defaultStores);

const StoreProvider = ({ children }) => {
  return (
    <storesContext.Provider value={defaultStores}>
      {children}
    </storesContext.Provider>
  );
};

export { storesContext, defaultStores, StoreProvider };
