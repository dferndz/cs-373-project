import { makeAutoObservable, action, reaction } from "mobx";
import { apiService } from "./apiService";
import qs from "query-string";
import { DEFAULT_PAGE_SIZE } from "../constants";

class CountryStore {
  countries = [];
  pagination = {
    currentPage: 0,
    totalPages: 0,
    totalInstances: 0,
    pageSize: DEFAULT_PAGE_SIZE,
    thisPageSize: 0,
  };
  loading = false;
  searchQuery = "";

  instance = null;
  loadingInstance = false;
  filters = {};

  loadingCountriesForCity = false;

  constructor() {
    makeAutoObservable(this);
    reaction(
      () => this.filters,
      () => this.fetchCountries()
    );
  }

  dispatch = (data) => {
    this.countries = data.data;
    this.pagination = {
      currentPage: data.page,
      totalPages: data.total_pages,
      totalInstances: data.total_rows,
      pageSize: data.page_size,
      thisPageSize: data.data.length,
    };
  };

  fetchCountries = () => {
    this.loading = true;
    const model = this.filters
      ? `countries?${qs.stringify(this.filters)}`
      : "countries";
    apiService.getData(model).then(
      action((data) => {
        if (data) {
          this.dispatch(data);
        } else {
          // TODO: handle a possible error
        }
        this.loading = false;
      })
    );
  };

  setFilters = (params) => {
    this.filters = params;
  };

  getCountry = (id) => {
    this.loadingInstance = true;
    apiService.getInstance("countries", id).then(
      action((data) => {
        if (data) {
          this.instance = data;
        } else {
          // TODO: handle error
        }
        this.loadingInstance = false;
      })
    );
  };

  getCountriesForCity = (cityId) => {
    this.loadingCountriesForCity = true;
    this.countriesForCity = [];

    apiService.getData(`cities/${cityId}/flights`).then(
      action((data) => {
        if (data) {
          this.countriesForCity = data.data;
        } else {
          // TODO: handle error
        }
        this.loadingCountriesForCity = false;
      })
    );
  };
}

const countryStore = new CountryStore();

export { CountryStore, countryStore };
