import { makeAutoObservable, action, reaction } from "mobx";
import { apiService } from "./apiService";
import qs from "query-string";
import { DEFAULT_PAGE_SIZE } from "../constants";

class FlightStore {
  flights = [];
  pagination = {
    currentPage: 0,
    totalPages: 0,
    totalInstances: 0,
    pageSize: DEFAULT_PAGE_SIZE,
    thisPageSize: 0,
  };
  loading = false;
  searchQuery = "";

  instance = null;
  loadingInstance = false;

  flightsForCountry = [];
  loadingFlightsForCountry = false;

  flightsForCity = [];
  loadingFlightsForCity = false;

  filters = {};
  model = "";

  constructor(model = "flights") {
    makeAutoObservable(this);
    this.model = model;
    reaction(
      () => this.filters,
      () => this.fetchFlights()
    );
  }

  setFilters = (params) => {
    this.filters = params;
  };

  dispatch = (data) => {
    this.flights = data.data;
    this.pagination = {
      currentPage: data.page,
      totalPages: data.total_pages,
      totalInstances: data.total_rows,
      pageSize: data.page_size,
      thisPageSize: data.data.length,
    };
  };

  fetchFlights = () => {
    this.loading = true;

    const model = this.filters
      ? `${this.model}?${qs.stringify(this.filters)}`
      : this.model;

    apiService.getData(model).then(
      action((data) => {
        if (data) {
          this.dispatch(data);
        } else {
          // TODO: handle a possible error
        }
        this.loading = false;
      })
    );
  };

  search = (query) => {
    this.searchQuery = query.toLowerCase();
  };

  getFlight = (id) => {
    this.loadingInstance = true;
    apiService.getInstance("flights", id).then(
      action((data) => {
        if (data) {
          this.instance = data;
        } else {
          // TODO: handle error
        }
        this.loadingInstance = false;
      })
    );
  };

  getFlightsForCountry = (countryId) => {
    this.loadingFlightsForCountry = true;
    this.flightsForCountry = [];

    apiService.getData(`countries/${countryId}/flights`).then(
      action((data) => {
        if (data) {
          this.flightsForCountry = data.data;
        } else {
          // TODO: handle error
        }
        this.loadingFlightsForCountry = false;
      })
    );
  };

  getFlightsForCity = (cityId) => {
    this.loadingFlightsForCity = true;
    this.flightsForCity = [];

    apiService.getData(`cities/${cityId}/flights`).then(
      action((data) => {
        if (data) {
          this.flightsForCity = data.data;
        } else {
          // TODO: handle error
        }
        this.loadingFlightsForCity = false;
      })
    );
  };
}

const flightStore = new FlightStore();

export { FlightStore, flightStore };
