const API_URL = process.env.REACT_APP_API_URL;

class ApiService {
  getUrl = (path) => `${API_URL}/${path}`;

  getData = async (model) => {
    return await fetch(this.getUrl(model))
      .then((res) =>
        res
          .json()
          .then((data) => Promise.resolve(data))
          .catch(() => Promise.resolve(null))
      )
      .catch(() => Promise.resolve(null));
  };

  getInstance = async (model, id) => {
    return await this.getData(`${model}/${id}`);
  };
}

const apiService = new ApiService();

export { apiService, ApiService };
