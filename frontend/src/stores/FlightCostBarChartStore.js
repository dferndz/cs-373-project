import { makeAutoObservable, action, reaction } from "mobx";
import { cacheProvider } from "../containers/about/RequestCache";
import { apiService } from "./apiService";
import { DEFAULT_PAGE_SIZE } from "../constants";

class FlightCostBarChartStore {
  flightCostBarChartData = [];
  pagination = {
    currentPage: 0,
    totalPages: 0,
    totalInstances: 0,
    pageSize: DEFAULT_PAGE_SIZE,
  };
  loading = true;
  searchQuery = "";

  instance = null;
  loadingInstance = false;
  filters = {};

  constructor() {
    makeAutoObservable(this);
    reaction(
      () => {},
      () => this.computeAverageFlightCostPerCountry()
    );
  }

  dispatch = (barChartData, barChartColors) => {
    this.flightCostBarChartData.barChartData = barChartData;
    this.flightCostBarChartData.barChartColors = barChartColors;
  };

  computeAverageFlightCostPerCountry = () => {
    this.loading = true;
    const model = "flights?page_size=1617";

    cacheProvider.get(apiService.getUrl(model)).then(
      action((data) => {
        if (data) {
          // Perform flight cost average computations
          var flightDatabyArrivalCountry = this.groupBy(
            data.data,
            "arrival_country_name"
          );
          var averageFlightCostPerCountry = {};
          for (const [countryName, flightData] of Object.entries(
            flightDatabyArrivalCountry
          )) {
            var totalFlightCost = 0;
            for (let flight of flightData) {
              totalFlightCost += flight.cost_total;
            }
            averageFlightCostPerCountry[countryName] =
              totalFlightCost / flightData.length;
          }
          var barChartData = [];

          for (const [countryName, averageFlightCost] of Object.entries(
            averageFlightCostPerCountry
          )) {
            barChartData.push({
              countryName: countryName,
              cost: averageFlightCost.toFixed(2),
            });
          }
          var barChartColors = [];
          // Generate random bar color
          for (var c = 0; c < barChartData.length; c++) {
            let n = (Math.random() * 0xfffff * 1000000).toString(16);
            let randomColor = "#" + n.slice(0, 6);
            barChartColors.push(randomColor);
          }

          // Dispatch flight cost average data

          this.dispatch(barChartData, barChartColors);
          //   dispatch(averageFlightCostPerCountry);
        }
        this.loading = false;
      })
    );
  };

  groupBy = function (xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };
}

const flightCostBarChartStore = new FlightCostBarChartStore();

export { FlightCostBarChartStore, flightCostBarChartStore };
