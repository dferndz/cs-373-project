import { makeAutoObservable, action, reaction } from "mobx";
import { apiService } from "./apiService";
import qs from "query-string";
import { DEFAULT_PAGE_SIZE } from "../constants";

class CityStore {
  cities = [];
  pagination = {
    currentPage: 0,
    totalPages: 0,
    totalInstances: 0,
    pageSize: DEFAULT_PAGE_SIZE,
    thisPageSize: 0,
  };
  loading = false;
  searchQuery = "";

  instance = null;
  loadingInstance = false;
  filters = {};

  citiesForCountry = [];
  loadingCitiesForCountry = false;

  constructor() {
    makeAutoObservable(this);
    reaction(
      () => this.filters,
      () => this.fetchCities()
    );
  }

  dispatch = (data) => {
    this.cities = data.data;
    this.pagination = {
      currentPage: data.page,
      totalPages: data.total_pages,
      totalInstances: data.total_rows,
      pageSize: data.page_size,
      thisPageSize: data.data.length,
    };
  };

  setFilters = (params) => {
    this.filters = params;
  };

  fetchCities = () => {
    this.loading = true;
    const model = this.filters
      ? `cities?${qs.stringify(this.filters)}`
      : "cities";

    apiService.getData(model).then(
      action((data) => {
        if (data) {
          this.dispatch(data);
        } else {
          // TODO: handle a possible error
        }
        this.loading = false;
      })
    );
  };

  search = (query) => {
    this.searchQuery = query.toLowerCase();
  };

  getCity = (id) => {
    this.loadingInstance = true;
    apiService.getInstance("cities", id).then(
      action((data) => {
        if (data) {
          this.instance = data;
        } else {
          // TODO: handle error
        }
        this.loadingInstance = false;
      })
    );
  };

  getCitiesForCountry = (countryId) => {
    this.loadingCitiesForCountry = true;
    this.citiesForCountry = [];

    apiService.getData(`countries/${countryId}/cities`).then(
      action((data) => {
        if (data) {
          this.citiesForCountry = data.data;
        } else {
          // TODO: handle error
        }
        this.loadingCitiesForCountry = false;
      })
    );
  };
}

const cityStore = new CityStore();

export { CityStore, cityStore };
