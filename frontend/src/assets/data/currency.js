const currencies = {
  EUR: {
    name: "EUR",
    rates: {
      USD: 1.11026,
      CAD: 1.410968,
      CNY: 7.008623,
    },
  },
  USD: {
    name: "USD",
    rates: {
      EUR: 0.900756,
      CAD: 1.271787,
      CNY: 6.31278,
    },
  },
  CNY: {
    name: "CNY",
    rates: {
      EUR: 0.142704,
      CAD: 0.201503,
      USD: 0.15841,
    },
  },
};

export { currencies };
