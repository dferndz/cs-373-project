const subregions = {
  NORTH_AMERICA: "Northern America",
  WEST_EUROPE: "Western Europe",
  EAST_ASIA: "Eastern Asia",
};

export { subregions };
