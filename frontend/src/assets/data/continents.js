const continents = {
  AMERICAS: "Americas",
  EUROPE: "Europe",
  ASIA: "Asia",
};

export { continents };
