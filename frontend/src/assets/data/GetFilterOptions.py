import requests


def getFilterOptions():
    url = "https://staging.api.plansafetrips.me/" + "flights"
    # field = "deaths"
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code != 200:
        print("WARNING: Request to {url} failed")
    response = response.json()["data"]

    options = set()
    for instance in response:
        options.add(instance["service_class"])
    options = list(options)
    options.sort()
    print(options)
    return options


getFilterOptions()
