import franceFlag from "../images/flags/france.png";
import chinaFlag from "../images/flags/china.png";

const cities = [
  {
    cases: 30028709,
    country: 2,
    country_continent: "Asia",
    country_name: "India",
    deaths: 390660,
    flag_url: "https://flagcdn.com/w320/in.png",
    iata: "DEL",
    id: 1,
    lat: 28.61394,
    lon: 77.20902,
    mask_policy: "Yes",
    name: "New Delhi",
    pic_url:
      "https://images.pexels.com/photos/789750/pexels-photo-789750.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
    population: 142004,
    quarantine_dur: 14,
    restriction_summary:
      "<p>Authorities have relied on lockdowns and localised restrictions to tackle the COVID-19 outbreak; they do not have a control over the spread of infections owing to India's large population. Partial lockdown measures remain ongoing in the majority of the states since early April due to a severe new outbreak, amplified by a dearth of resources such as hospital beds and oxygen cylinders. There is also a lack of reliable information on the number of cases and fatalities.</p>",
    risk_level: "Extreme",
    temp: 22,
    vaccine_policy: "No",
  },
  {
    id: "2",
    name: "Paris",
    image:
      "https://www.fodors.com/wp-content/uploads/2018/10/HERO_UltimateParis_Heroshutterstock_112137761.jpg",
    country: "France",
    countryId: "2",
    location: { lat: 48.85661, lng: 2.3515 },
    population: 2165423,
    averageTemperature: "12 C",
    flag: franceFlag,
    riskLevel: "High",
    totalCovidDeaths: 109928,
    totalCovidCases: 5694975,
    maskRequirement: "Partial Requirement",
    vaccineRequirement: "No",
    restrictionStartDate: "June 30th 2021",
    restrictionEndDate: "Indefinite",
    quarantineDuration: "7 days",
    restrictionSummary:
      "<p>Health officials confirmed on 24 January 2020 that three cases of COVID-19 were detected in Paris and Bordeaux, the first cases to be confirmed in the European Union (EU). The outbreak has resulted in one of the highest number of deaths in Europe and prompted three nationwide lockdowns. After initially lifting most health measures, officials declared in July 2021 that the country entered a fourth wave of the pandemic. The country's southern regions have especially seen a sharp uptick in new cases due to the spread of the delta virus variant.</p>",
  },
  {
    id: "3",
    name: "Beijing",
    image:
      "https://stillmed.olympics.com/media/Images/OlympicOrg/News/2020/11/13/2020-11-13-beijing-thumbnail.jpg?interpolation=lanczos-none&resize=2120:1200",
    countryId: "3",
    country: "China",
    location: { lat: 39.90596, lng: 116.39125 },
    population: 19612368,
    averageTemperature: "3 C",
    flag: chinaFlag,
    riskLevel: "High",
    totalCovidDeaths: 4636,
    totalCovidCases: 91653,
    maskRequirement: "Partial Requirement",
    vaccineRequirement: "No",
    restrictionStartDate: "January 1st 2021",
    restrictionEndDate: "Indefinite",
    quarantineDuration: "14 days",
    restrictionSummary:
      "<p>Authorities imposed 'closed-management' lockdown measures Beijing's Fangshan and Haidian districts, after detections of new COVID-19 cases. Residents were urged not to leave Beijing, while inbound flights, trains and buses from COVID-19 medium- and high-risk areas where new cases have been found were suspended, including Nanjing and Yangzhou (Jiangsu province), Zhengzhou (Henan) and Zhangjiajie (Hunan). Only travellers with a green health code, issued to those who were not in any medium- or high-risk areas in the preceding 14 days, with a negative nucleic acid test result from within 48 hours prior to departure were permitted entry. Arrival must test again upon arrival and undergo 14 days of quarantine.</p>",
  },
];

export { cities };
