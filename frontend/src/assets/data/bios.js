import {
  DanielHeadshot,
  JayHeadshot,
  SaranHeadshot,
  DillonHeadshot,
  WilleeHeadshot,
} from "../images/headshots";

const bios = [
  {
    name: "Daniel Fernandez",
    gitlabNames: ["Daniel Fernandez"],
    role: "Full-Stack Developer",
    phaseleader: "Phase 1 Leader",
    headshot: DanielHeadshot,
    tests: 9 + 7,
    commits: 0,
    issues: 0,
    description:
      "I am a fourth year CS student at UT Austin. I grew up in Havana, Cuba, and have been living in Texas for the past five years. I spend my free time playing guitar and piano, playing video games from time to time and watching soccer games.",
  },
  {
    name: "Jay Acosta",
    gitlabNames: ["Jay A Acosta", "jayacosta"],
    role: "Front-End Developer",
    phaseleader: "",
    headshot: JayHeadshot,
    tests: 6 + 24 + 15,
    commits: 0,
    issues: 0,
    description:
      "I am third year student CS student at UT Austin. I grew up in Arlington, TX. Outside of class, I really enjoy cycling, making music, and occassionally watching boxing matches.",
  },
  {
    name: "Dillon Samra",
    gitlabNames: ["Dillon Samra"],
    role: "Backend Developer/DevOps",
    phaseleader: "Phase 2 Leader",
    headshot: DillonHeadshot,
    tests: 9,
    commits: 0,
    issues: 0,
    description:
      "I am third year student CS/Business student at UT Austin. In the future, I hope to pursue a career in software engineering or automation.",
  },
  {
    name: "Saran Chockan",
    gitlabNames: ["Saran Chockan"],
    role: "Full-Stack Developer",
    phaseleader: "Phase 3 Leader",
    headshot: SaranHeadshot,
    tests: 2,
    commits: 0,
    issues: 0,
    description:
      "I am a third year student CS student at UT Austin. Aside from school, I love playing basketball with my friends, reading post-modernist philosophy, and competing in blitz chess.",
  },
  {
    name: "Willee Johnston",
    gitlabNames: ["Willee Johnston", "willeewaco"],
    role: "Front-End Developer",
    headshot: WilleeHeadshot,
    phaseleader: "Phase 4 Leader",
    tests: 0,
    commits: 0,
    issues: 0,
    description:
      "I am a fourth year CS major at UT Austin. I grew up in Waco, Texas. In my free time, I enjoy playing board games.",
  },
];

export { bios };
