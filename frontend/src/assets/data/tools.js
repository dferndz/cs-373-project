import {
  GitLabIcon,
  ReactIcon,
  MUIIcon,
  JestIcon,
  AxiosIcon,
  AWSIcon,
  PostmanIcon,
  DockerIcon,
  DiscordIcon,
  NamecheapIcon,
  MapboxIcon,
  TravelRestrictionsAPIIcon,
  RESTCountriesAPIIcon,
  GoogleMapsIcon,
  PrettierIcon,
  MobxIcon,
  APINinjasIcon,
  VercelIcon,
  SeleniumIcon,
  GoogleCloudIcon,
  ElephantIcon,
  RechartsIcon,
  ReactSimpleMapsIcon,
  APILayerIcon,
  SQLAlchemyIcon,
  FlaskIcon,
} from "../images/icons";

const tools = [
  {
    title: "React",
    description: "A JavaScript framework used for developing the front-end",
    link: "https://reactjs.org/",
    icon: ReactIcon,
  },
  {
    title: "Material UI",
    description:
      "A React UI design library based upon Google's Material Design standards",
    link: "https://mui.com",
    icon: MUIIcon,
  },
  {
    title: "Jest",
    description:
      "A Javascript unit testing framework used to test functionality of React components",
    link: "https://jestjs.io/",
    icon: JestIcon,
  },
  {
    title: "Axios",
    description: "A Javascipt HTTP request library used to fetch API data",
    link: "https://axios-http.com/",
    icon: AxiosIcon,
  },
  {
    title: "AWS",
    description: "The cloud hosting platform for our application",
    link: "https://aws.amazon.com/",
    icon: AWSIcon,
  },
  {
    title: "Postman",
    description: "An API design and testing tool",
    link: "https://www.postman.com/",
    icon: PostmanIcon,
  },
  {
    title: "Docker",
    description:
      "Containerization tool used to create consistent runtime environments",
    link: "https://www.docker.com/",
    icon: DockerIcon,
  },
  {
    title: "Discord",
    description: "A communication platform for our team",
    link: "https://discord.com/",
    icon: DiscordIcon,
  },
  {
    title: "Prettier",
    description: "Used to format code to maintain consistent coding style",
    link: "https://prettier.io/",
    icon: PrettierIcon,
  },
  {
    title: "Mobx",
    description: "Used as a state management library for better scalability ",
    link: "https://mobx.js.org",
    icon: MobxIcon,
  },
  {
    title: "GitLab",
    description: "Used to maintain a single git repository across team members",
    link: "https://gitlab.com",
    icon: GitLabIcon,
  },
  {
    title: "Namecheap",
    description: "Used to secure our domain name",
    link: "https://www.namecheap.com/",
    icon: NamecheapIcon,
  },
  {
    title: "Vercel",
    description:
      "A platform that deploys a preview of all branches. This helps code reviewers visualize changes.",
    link: "https://vercel.com/",
    icon: VercelIcon,
  },
  {
    title: "Selenium",
    description: "Javascript framework for frontend acceptance tests.",
    link: "https://www.selenium.dev/",
    icon: SeleniumIcon,
  },
  {
    title: "Google Cloud",
    description:
      "Cloud framework used to host our backend application and database. We also utilize the Secret Manager to store credentials.",
    link: "https://cloud.google.com/gcp?utm_source=google&utm_medium=cpc&utm_campaign=na-US-all-en-dr-bkws-all-all-trial-e-dr-1011347&utm_content=text-ad-none-any-DEV_c-CRE_491349594127-ADGP_Desk%20%7C%20BKWS%20-%20EXA%20%7C%20Txt%20~%20Google%20Cloud%20Platform%20Core-KWID_43700064423315751-kwd-26415313501&utm_term=KW_google%20cloud%20platform-ST_google%20cloud%20platform&gclid=Cj0KCQjw3IqSBhCoARIsAMBkTb3KsOTWWeaiDLn5IW3RgJ4TcV3j1htywAmKSZfpPLxiQGr9r7u-utkaAnGDEALw_wcB&gclsrc=aw.ds",
    icon: GoogleCloudIcon,
  },
  {
    title: "Postgres",
    description:
      "Postgres is a relational database system utilized to manage the storage of our data.",
    link: "https://www.postgresql.org/",
    icon: ElephantIcon,
  },
  {
    title: "Flask",
    description:
      "Flask is a web framework that allowed us to easily create routes for our API.",
    link: "https://flask.palletsprojects.com/en/2.1.x/",
    icon: FlaskIcon,
  },
  {
    title: "SQLAlchemy",
    description:
      "A Python framework that acts as an SQL toolkit for adding new data into our API.",
    link: "https://www.sqlalchemy.org/",
    icon: SQLAlchemyIcon,
  },
  {
    title: "Newman",
    description:
      "A npm command line runner that tests the format and structure of our production API.",
    link: "https://www.npmjs.com/package/newman",
    icon: PostmanIcon,
  },
  {
    title: "Recharts",
    description:
      "A npm package used to display visualizations using graphical data",
    link: "https://recharts.org/en-US/",
    icon: RechartsIcon,
  },
  {
    title: "react-simple-maps",
    description:
      "A npm package used to display the COVID heatmap in our visualizations",
    link: "https://www.react-simple-maps.io/",
    icon: ReactSimpleMapsIcon,
  },
];

const dataSources = [
  {
    title: "Rest Countries",
    description: "Used to pull flag information for countries",
    link: "https://restcountries.com/",
    icon: RESTCountriesAPIIcon,
  },
  {
    title: "Country API",
    description: "Used to pull information and statistics about countries",
    link: "https://api-ninjas.com/api/country",
    icon: APINinjasIcon,
  },
  {
    title: "Flight Offers API",
    description: "Used to pull flight information for each instance",
    link: "https://developers.amadeus.com/self-service/category/air/api-doc/flight-offers-search/api-reference",
    icon: TravelRestrictionsAPIIcon,
  },
  {
    title: "Travel Restrictions API",
    description:
      "Used to retrieve information about a country's travel restrictions",
    link: "https://developers.amadeus.com/self-service/category/covid-19-and-travel-safety/api-doc/travel-restrictions/api-reference",
    icon: TravelRestrictionsAPIIcon,
  },
  {
    title: "Mapbox API",
    description: "Used to get data about a countries borders and location",
    link: "https://www.mapbox.com/",
    icon: MapboxIcon,
  },
  {
    title: "Google Maps API",
    description: "Used to render a city location for an instance page",
    link: "https://developers.google.com/maps",
    icon: GoogleMapsIcon,
  },
  {
    title: "Currency Exchange Rates API",
    description: "Used to get currency exchange rates per country",
    link: "http://api.exchangeratesapi.io",
    icon: APILayerIcon,
  },
];

const sourceCode = [
  {
    title: "GitLab Repository",
    link: "https://gitlab.com/dferndz/cs-373-project",
    icon: GitLabIcon,
  },
  {
    title: "Postman API",
    link: "https://documenter.getpostman.com/view/19718978/UVkqsvCG",
    icon: PostmanIcon,
  },
];

export { tools, dataSources, sourceCode };
