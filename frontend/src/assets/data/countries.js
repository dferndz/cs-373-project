import { continents } from "./continents";
import { subregions } from "./subregions";

import usFlag from "../images/flags/us.png";
// import franceFlag from "../images/flags/france.png";
// import chinaFlag from "../images/flags/china.png";

const countries = [
  {
    id: "1",
    name: "US",
    vaccinationCount: 215602728,
    pic_url:
      "https://static5.depositphotos.com/1030296/395/i/600/depositphotos_3958211-stock-photo-new-york-cityscape-tourism-concept.jpg",
    flag_url: usFlag,
    capital: "Washington, D.C.",
    population: 329484123,
    languages: ["English"],
    currency: "USD",
    exchange_rates: {
      cad: 0.19614991995684505,
      cny: 0.157076933241692,
      eur: 0.14301081819635328,
      usd: 1,
    },
    geoData: {
      lat: 1,
      lon: 0,
    },
    continent: continents.AMERICAS,
    subregion: subregions.NORTH_AMERICA,
    tourists: 79746,
    air_quality_index: 162,
    location: { lat: 37.73937, lng: -97.508295 },
    temp_average: -14,
    temp_low: -14,
    temp_high: -14,
    temp_feels_like: -21,
    description:
      "<p>The <b>United States of America</b>, commonly known as the <b>United States</b> or <b>America</b>, is a country primarily located in North America. It consists of 50 states, a federal district, five major unincorporated territories, 326 Indian reservations, and nine minor outlying islands. At nearly 3.8 million square miles, it is the world's third- or fourth-largest country by geographic area. The United States shares land borders with Canada to the north and Mexico to the south as well as maritime borders with the Bahamas, Cuba, Russia, and other countries. With a population of more than 331 million people, it is the third most populous country in the world. The national capital is Washington, D.C., and the most populous city and financial center is New York City.</p>",
  },
];

export { countries };
