// Tool Icons
import GitLabIcon from "./GitLabIcon.png";
import ReactIcon from "./ReactIcon.png";
import MUIIcon from "./MUIIcon.png";
import JestIcon from "./JestIcon.png";
import AxiosIcon from "./AxiosIcon.svg";
import AWSIcon from "./AWSIcon.png";
import PostmanIcon from "./PostmanIcon.png";
import DockerIcon from "./DockerIcon.png";
import DiscordIcon from "./DiscordIcon.svg";
import PrettierIcon from "./PrettierIcon.png";
import MobxIcon from "./MobxIcon.png";
import NamecheapIcon from "./NamecheapIcon.png";
import FlaskIcon from "./FlaskIcon.png";
import SQLAlchemyIcon from "./SQLAlchemyIcon.png";
import VercelIcon from "./VercelIcon.png";
import SeleniumIcon from "./SeleniumIcon.png";
import GoogleCloudIcon from "./GoogleCloudIcon.png";
import ElephantIcon from "./ElephantIcon.png";
import RechartsIcon from "./RechartsIcon.png";
import ReactVisIcon from "./ReactVisIcon.png";
import ReactSimpleMapsIcon from "./ReactSimpleMapsIcon.jpg";

// Data Source Icons
import MapboxIcon from "./MapboxIcon.png";
import TravelRestrictionsAPIIcon from "./TravelRestrictionsAPIIcon.png";
import FlightAwareIcon from "./FlightAwareIcon.png";
import RESTCountriesAPIIcon from "./RESTCountriesAPIIcon.png";
import APINinjasIcon from "./APINinjasIcon.png";
import GoogleMapsIcon from "./GoogleMapsIcon.svg";
import APILayerIcon from "./APILayerIcon.png";

// Other Icons
import IssuesIcon from "./IssuesIcon.svg";
import CommitIcon from "./CommitIcon.svg";
import TestIcon from "./TestIcon.svg";

export {
  GitLabIcon,
  ReactIcon,
  MUIIcon,
  JestIcon,
  AxiosIcon,
  AWSIcon,
  PostmanIcon,
  DockerIcon,
  DiscordIcon,
  NamecheapIcon,
  VercelIcon,
  SeleniumIcon,
  GoogleCloudIcon,
  ElephantIcon,
  RechartsIcon,
  ReactVisIcon,
  ReactSimpleMapsIcon,
  APILayerIcon,
  MapboxIcon,
  FlaskIcon,
  SQLAlchemyIcon,
  TravelRestrictionsAPIIcon,
  FlightAwareIcon,
  RESTCountriesAPIIcon,
  APINinjasIcon,
  GoogleMapsIcon,
  PrettierIcon,
  MobxIcon,
  IssuesIcon,
  CommitIcon,
  TestIcon,
};
