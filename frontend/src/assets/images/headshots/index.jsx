import DanielHeadshot from "./DanielHeadshot.png";
import JayHeadshot from "./JayHeadshot.jpg";
import SaranHeadshot from "./SaranHeadshot.png";
import DillonHeadshot from "./DillonHeadshot.jpg";
import WilleeHeadshot from "./WilleeHeadshot.jpg";

export { 
  DanielHeadshot, 
  JayHeadshot,
  SaranHeadshot,
  DillonHeadshot,
  WilleeHeadshot 
};