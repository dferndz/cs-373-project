import React, { Suspense } from "react";
import { ThemeProvider } from "@mui/material";
import { NavBar } from "./components/navigation/NavBar";
import { theme } from "./theme";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { pages } from "./pages.jsx";
import { Fallback } from "./components/suspense";
import { Footer } from "./components/navigation/Footer";

const App = () => {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <NavBar />
        <Suspense fallback={<Fallback />}>
          <Routes>
            {pages.map((value, index) => {
              // don't create route if view prop doesn't exist
              if (!value?.element) {
                return null;
              }
              return (
                <Route {...value} path={value.url} key={`${index}-router`} />
              );
            })}
          </Routes>
        </Suspense>
        <Footer />
      </ThemeProvider>
    </BrowserRouter>
  );
};

export default App;
