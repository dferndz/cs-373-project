import { useCallback } from "react";
import { useNavigate } from "react-router";

const useGoBack = () => {
  const navigate = useNavigate();

  return useCallback(
    (event) => {
      event.preventDefault();
      navigate(-1);
    },
    [navigate]
  );
};

export { useGoBack };
