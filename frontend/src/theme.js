import { createTheme } from "@mui/material";

const theme = createTheme({
  site: {
    title: "SafeTravels",
  },
});

export { theme };
