import React from "react";
import { lazy } from "react";

const CountryList = lazy(() =>
  import("./containers/country/ContryList").then((module) => ({
    default: module.CountryList,
  }))
);
const CountryInstance = lazy(() =>
  import("./containers/country/CountryInstance").then((module) => ({
    default: module.CountryInstance,
  }))
);

const AboutPage = lazy(() =>
  import("./containers/about/AboutPage").then((module) => ({
    default: module.AboutPage,
  }))
);

const HomePage = lazy(() =>
  import("./containers/home/HomePage").then((module) => ({
    default: module.HomePage,
  }))
);
const FlightList = lazy(() =>
  import("./containers/flight/FlightList").then((module) => ({
    default: module.FlightList,
  }))
);
const FlightInstance = lazy(() =>
  import("./containers/flight/FlightInstance").then((module) => ({
    default: module.FlightInstance,
  }))
);

const CityList = lazy(() =>
  import("./containers/city/CityList").then((module) => ({
    default: module.CityList,
  }))
);

const CityInstance = lazy(() =>
  import("./containers/city/CityInstance").then((module) => ({
    default: module.CityInstance,
  }))
);

const SearchPage = lazy(() =>
  import("./containers/search/SearchPage").then((module) => ({
    default: module.SearchPage,
  }))
);

const VisualizationsPage = lazy(() =>
  import("./containers/visualizations/VisualizationsPage").then((module) => ({
    default: module.VisualizationsPage,
  }))
);

const ProviderVisualizationsPage = lazy(() =>
  import("./containers/visualizations/ProviderVisualizationsPage").then(
    (module) => ({
      default: module.ProviderVisualizationsPage,
    })
  )
);

const pages = [
  {
    title: "Splash",
    url: "/",
    showInMenu: false,
    exact: true,
    element: <HomePage />,
  },
  {
    title: "Countries",
    url: "/countries",
    showInMenu: true,
    exact: true,
    element: <CountryList />,
  },
  {
    title: "Country",
    url: "/countries/:id",
    showInMenu: false,
    exact: true,
    element: <CountryInstance />,
  },
  {
    title: "Cities",
    url: "/cities",
    showInMenu: true,
    exact: true,
    element: <CityList />,
  },
  {
    title: "City",
    url: "/cities/:id",
    showInMenu: false,
    exact: true,
    element: <CityInstance />,
  },
  {
    title: "Flights",
    url: "/flights",
    showInMenu: true,
    exact: true,
    element: <FlightList />,
  },
  {
    title: "Flight",
    url: "/flights/:id",
    showInMenu: false,
    exact: true,
    element: <FlightInstance />,
  },
  {
    title: "About",
    url: "/about",
    showInMenu: true,
    exact: true,
    element: <AboutPage />,
  },
  {
    title: "Search",
    url: "/search",
    showInMenu: true,
    exact: true,
    element: <SearchPage />,
  },
  {
    title: "Visualizations",
    url: "/visualizations",
    showInMenu: true,
    exact: true,
    element: <VisualizationsPage />,
  },
  {
    title: "Provider Visualizations",
    url: "/providervisualizations",
    showInMenu: true,
    exact: true,
    element: <ProviderVisualizationsPage />,
  },
];

export { pages };
