import unittest
import os

SUPPORTED_OS = ["linux", "posix"]


def passing_condition(results):
    return int(len(results.errors) != 0 or len(results.failures) != 0)


def main():
    if os.name not in SUPPORTED_OS:
        print("Invalid Operating System")
        exit(1)

    current_dir = os.path.dirname(os.path.realpath(__file__))
    runner = unittest.TextTestRunner()
    suites = unittest.TestLoader().discover(current_dir, pattern="test_*.py")
    results = runner.run(suites)

    exit(passing_condition(results))


if __name__ == "__main__":
    main()
