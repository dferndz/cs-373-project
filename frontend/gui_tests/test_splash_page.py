from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import unittest
from selenium.webdriver.common.by import By

"""
make sure geckodriver is put into the path variable
"""

BASE_URL = "https://www.plansafetrips.me"

# Adapted code from the texasvotes.me repository
class TestSplashPage(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(self):
        firefox_options = FirefoxOptions()
        firefox_options.add_argument("--headless")
        firefox_options.add_argument("--no-sandbox")
        firefox_options.add_argument("--disable-dev-shm-usage")

        self.driver = webdriver.Firefox(options=firefox_options)
        self.driver.get(f"{BASE_URL}")
        # self.driver.implicitly_wait (5)

    def setUp(self):
        self.driver.get(f"{BASE_URL}")

    def test_flight_card(self):
        self.driver.get(f"{BASE_URL}")
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/")

        self.driver.find_element(By.XPATH, "//div[normalize-space()='Flights']").click()

        self.driver.implicitly_wait(5)

        self.assertEqual(self.driver.current_url, f"{BASE_URL}/flights/")

    def test_about_us_button(self):
        self.driver.get(f"{BASE_URL}")
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/")

        self.driver.find_element(
            By.XPATH, "//button[normalize-space()='About Us']"
        ).click()

        self.assertEqual(self.driver.current_url, f"{BASE_URL}/about/")

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(self):
        # print ('Quitting...')
        self.driver.quit()
        print("\nQuit successfully!\n")
