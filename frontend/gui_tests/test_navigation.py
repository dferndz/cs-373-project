from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import unittest
from selenium.webdriver.common.by import By

"""
make sure geckodriver is put into the path variable
"""

BASE_URL = "https://www.plansafetrips.me"

# Adapted code from the texasvotes.me repository
class TestNavigation(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(self):
        firefox_options = FirefoxOptions()
        firefox_options.add_argument("--headless")
        firefox_options.add_argument("--no-sandbox")
        firefox_options.add_argument("--disable-dev-shm-usage")

        self.driver = webdriver.Firefox(options=firefox_options)
        # self.driver.implicitly_wait (5)

    def setUp(self):
        self.driver.get(f"{BASE_URL}/")

    """ 
    Source: https://stackoverflow.com/questions/12323403/how-do-i-find-an-element-that-contains-specific-text-in-selenium-webdriver-pyth
    """

    def find_elements_by_text(self, text):
        return self.driver.find_elements(By.XPATH, f"//*[contains(text(), '{text}')]")

    def test_home_link(self):
        # print (self.find_elements_by_text (self.driver, "SafeTravels"))
        home = self.driver.find_element(
            By.XPATH, "//a[normalize-space()='SafeTravels']"
        )
        home.click()

        home_redirect = self.driver.find_element(
            By.XPATH, "//a[normalize-space()='SafeTravels']"
        )

        self.assertEqual(home.text, home_redirect.text)
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/")

    def test_navigate_countries(self):
        self.driver.find_element(
            By.XPATH, "//button[normalize-space()='Countries']"
        ).click()

        self.assertEqual(self.driver.current_url, f"{BASE_URL}/countries")

    def test_navigate_cities(self):
        self.driver.find_element(
            By.XPATH, "//button[normalize-space()='Cities']"
        ).click()

        self.assertEqual(self.driver.current_url, f"{BASE_URL}/cities")

    def test_navigate_flights(self):
        self.driver.find_element(
            By.XPATH, "//button[normalize-space()='Flights']"
        ).click()

        self.assertEqual(self.driver.current_url, f"{BASE_URL}/flights")

    def test_flights_to_home(self):

        # click on flights navigation page, should redirect to /flights
        self.driver.find_element(
            By.XPATH, "//button[normalize-space()='Flights']"
        ).click()
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/flights")

        # click on home page link, should redirect to /
        self.driver.find_element(
            By.XPATH, "//a[normalize-space()='SafeTravels']"
        ).click()
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/")

    def test_back_button(self):

        original_url = self.driver.current_url

        self.driver.find_element(
            By.XPATH, "//button[normalize-space()='Flights']"
        ).click()
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/flights")

        self.driver.back()

        self.assertEqual(self.driver.current_url, original_url)

    # def test_countries_id (self) :
    #     self.driver.get (f"{BASE_URL}/countries")
    #     self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries")

    #     search_bar = self.driver.find_element_by_id ("mui-4")
    #     search_bar.send_keys ("China")

    #     country_page = self.driver.find_element_by_xpath ("//span[normalize-space()='China']")
    #     country_page.click ()

    #     self.assertNotEqual (self.driver.current_url, f"{BASE_URL}/countries")

    #     self.driver.back ()

    #     self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries")

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(self):
        # print ('Quitting...')
        self.driver.quit()
        # print ('\nQuit successfully!\n')
