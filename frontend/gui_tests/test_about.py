from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import unittest
from selenium.webdriver.common.by import By

"""
make sure geckodriver is put into the path variable
"""
BASE_URL = "https://www.plansafetrips.me"

# Adapted code from the texasvotes.me repository
class TestNavigation(unittest.TestCase):
    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(self):
        firefox_options = FirefoxOptions()
        firefox_options.add_argument("--headless")
        firefox_options.add_argument("--no-sandbox")
        firefox_options.add_argument("--disable-dev-shm-usage")

        self.driver = webdriver.Firefox(options=firefox_options)

    def test_git_repo_link(self):
        self.driver.get(f"{BASE_URL}/about")
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/about")
        gitlab_card = self.driver.find_element(
            By.XPATH, "//a[normalize-space()='GitLab Repository']"
        )

        gitlab_card.click()

        self.assertEqual(
            self.driver.current_url, "https://gitlab.com/dferndz/cs-373-project"
        )

    def test_postman_link(self):
        self.driver.get(f"{BASE_URL}/about")
        self.assertEqual(self.driver.current_url, f"{BASE_URL}/about")
        gitlab_card = self.driver.find_element(
            By.XPATH, "//a[normalize-space()='Postman API']"
        )

        gitlab_card.click()

        self.assertEqual(
            self.driver.current_url,
            "https://documenter.getpostman.com/view/19718978/UVkqsvCG",
        )

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(self):
        # print ('Quitting...')
        self.driver.quit()
        # print ('\nQuit successfully!\n')
