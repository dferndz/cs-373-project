from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By
import unittest

"""
make sure geckodriver is put into the path variable
"""
BASE_URL = "https://www.plansafetrips.me"

# Adapted code from the texasvotes.me repository
class TestSorting(unittest.TestCase):
    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(self):
        firefox_options = FirefoxOptions()
        firefox_options.add_argument("--headless")
        firefox_options.add_argument("--no-sandbox")
        firefox_options.add_argument("--disable-dev-shm-usage")

        self.driver = webdriver.Firefox(options=firefox_options)

    def test_sort_country_1(self):
        self.driver.get(f"{BASE_URL}/countries")

        self.driver.implicitly_wait(5)

        sort_dropdown = self.driver.find_element(By.ID, "sort")
        sort_dropdown.click()

        options = sort_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "Air Quality", options)
        next(options).click()  # only click the first air quality option (first found)

        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries?page=1&sort=air_quality_index")

        self.driver.implicitly_wait(5)

        airq_cards = self.driver.find_elements(
            By.XPATH, "//*[contains(normalize-space(text()), 'Air quality:')]"
        )
        air_quals = [
            int(elem.get_attribute("innerHTML").split()[-1]) for elem in airq_cards
        ]

        # air qualities should be ascending (list should be unchanged after sorting)
        self.assertListEqual(air_quals, sorted(air_quals))

    def test_sort_country_2(self):
        self.driver.get(f"{BASE_URL}/countries")

        self.driver.implicitly_wait(5)

        sort_dropdown = self.driver.find_element(By.ID, "sort")
        # sort_dropdown.get_attribute
        sort_dropdown.click()

        options = sort_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "Currency", options)
        next(options).click()  # only click the first currency option (first found)

        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries?page=1&sort=currency")

        self.driver.implicitly_wait(5)

        currency_cards = self.driver.find_elements(
            By.XPATH, "//*[contains(normalize-space(text()), 'Currency: ')]"
        )
        currencies = [
            elem.get_attribute("innerHTML").split()[-1] for elem in currency_cards
        ]

        # currency should be ascending (list should be unchanged after sorting)
        self.assertListEqual(currencies, sorted(currencies))

    def test_sort_city_1(self):
        self.driver.get(f"{BASE_URL}/cities")
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/cities?page=1")

        self.driver.implicitly_wait(5)

        sort_dropdown = self.driver.find_element(By.ID, "sort")
        # sort_dropdown.get_attribute
        sort_dropdown.click()

        options = sort_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "Quarantine", options)
        next(options).click()  # only click the first quarantine option (first found)

        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/cities?page=1&sort=quarantine_dur")

        self.driver.implicitly_wait(5)

        cards = self.driver.find_elements(
            By.XPATH, "//*[contains(normalize-space(text()), 'Quarantine Duration: ')]"
        )
        values = [int(elem.get_attribute("innerHTML").split()[-1]) for elem in cards]

        # quarantine duration should be ascending (list should be unchanged after sorting)
        self.assertListEqual(values, sorted(values))

    def test_sort_city_2(self):
        self.driver.get(f"{BASE_URL}/cities")
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/cities?page=1")

        self.driver.implicitly_wait(5)

        sort_dropdown = self.driver.find_element(By.ID, "sort")
        # sort_dropdown.get_attribute
        sort_dropdown.click()

        self.driver.implicitly_wait(5)

        options = sort_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "COVID Deaths", options)
        next(options).click()  # only click the first covid deaths option (first found)

        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/cities?page=1&sort=deaths")

        self.driver.implicitly_wait(5)

        cards = self.driver.find_elements(
            By.XPATH, "//*[contains(normalize-space(text()), 'Total Covid Deaths: ')]"
        )
        values = [int(elem.get_attribute("innerHTML").split()[-1]) for elem in cards]

        # num of deaths should be ascending (list should be unchanged after sorting)
        self.assertListEqual(values, sorted(values))

    def test_sort_city_3(self):
        self.driver.get(f"{BASE_URL}/cities")
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/cities?page=1")

        self.driver.implicitly_wait(5)

        sort_dropdown = self.driver.find_element(By.ID, "sort")
        # sort_dropdown.get_attribute
        sort_dropdown.click()

        self.driver.implicitly_wait(5)

        options = sort_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "COVID Cases", options)
        next(options).click()  # only click the first covid deaths option (first found)

        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/cities?page=1&sort=deaths")

        self.driver.implicitly_wait(5)

        cards = self.driver.find_elements(
            By.XPATH, "//*[contains(normalize-space(text()), 'Total Covid Cases: ')]"
        )
        values = [int(elem.get_attribute("innerHTML").split()[-1]) for elem in cards]

        # num of deaths should be ascending (list should be unchanged after sorting)
        self.assertListEqual(values, sorted(values))

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(self):
        # print ('Quitting...')
        self.driver.quit()
        # print ('\nQuit successfully!\n')
