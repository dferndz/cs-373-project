from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By
import unittest

"""
make sure geckodriver is put into the path variable
"""
BASE_URL = "https://www.plansafetrips.me"


# Adapted code from the texasvotes.me repository
class TestSearching(unittest.TestCase):
    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(self):
        firefox_options = FirefoxOptions()
        firefox_options.add_argument("--headless")
        firefox_options.add_argument("--no-sandbox")
        firefox_options.add_argument("--disable-dev-shm-usage")

        self.driver = webdriver.Firefox(options=firefox_options)

        """
        complex XPath
            //*[@data-testid='country-list-card'] : finds any tag with the data-testid attr being country-list-card
            /button/div[1]/div[2]/span[2]/span/span : nesting structure to get to continent text below country name
        """
        self.COUNTRY_CARD = "//*[@data-testid='country-list-card']"
        self.CITY_CARD = "//*[@data-testid='city-list-card']"
        self.CARD_TITLE = "button/div[1]/div[2]/span[1]/span/span"
        self.CARD_SUBTITLE = "button/div[1]/div[2]/span[2]/span/span"
        self.CARD_INFO_1 = "button/div[2]/span/span[1]/span"
        self.CARD_INFO_2 = "button/div[2]/span/span[2]/span"

    def test_filter_continent_1(self):
        self.driver.get(f"{BASE_URL}/countries")

        self.driver.implicitly_wait(5)

        filter_dropdown = self.driver.find_element(By.ID, "continent")
        filter_dropdown.click()

        options = filter_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "Africa", options)
        for elem in options:
            elem.click()

        self.driver.implicitly_wait(5)

        cards = self.driver.find_elements(
            By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_SUBTITLE}"
        )

        # assert that all subtitles have the text 'Africa'
        self.assertTrue(
            all(subtitle.get_attribute("innerHTML") == "Africa" for subtitle in cards)
        )

    def test_filter_continent_2(self):
        self.driver.get(f"{BASE_URL}/countries")

        self.driver.implicitly_wait(2)

        filter_dropdown = self.driver.find_element(By.ID, "continent")
        filter_dropdown.click()

        continent_selections = ["Asia", "Europe"]

        options = filter_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text in continent_selections, options)
        for elem in options:
            elem.click()

        self.driver.implicitly_wait(2)

        cards = self.driver.find_elements(
            By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_SUBTITLE}"
        )

        # assert that all subtitles have the text 'Asia' or 'Europe'
        self.assertTrue(
            all(
                subtitle.get_attribute("innerHTML") in continent_selections
                for subtitle in cards
            )
        )

    def test_filter_risk_1(self):
        self.driver.get(f"{BASE_URL}/cities")

        self.driver.implicitly_wait(2)

        filter_dropdown = self.driver.find_element(By.ID, "risk_level")
        filter_dropdown.click()

        options = filter_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "Extreme", options)
        for elem in options:
            elem.click()

        self.driver.implicitly_wait(2)

        cards = self.driver.find_elements(
            By.XPATH, f"{self.CITY_CARD}/{self.CARD_RISK_INFO}"
        )

        # assert that all elements selected have the text 'Risk Level: Extreme'
        self.assertTrue(
            all(
                elem.get_attribute("innerHTML") == "Risk Level: Extreme"
                for elem in cards
            )
        )

    def test_filter_risk_1(self):
        self.driver.get(f"{BASE_URL}/cities")

        self.driver.implicitly_wait(2)

        filter_dropdown = self.driver.find_element(By.ID, "risk_level")
        filter_dropdown.click()

        options = filter_dropdown.find_elements(By.XPATH, "//li[@role='option']")
        options = filter(lambda elem: elem.text == "Extreme", options)
        for elem in options:
            elem.click()

        self.driver.implicitly_wait(2)

        cards = self.driver.find_elements(
            By.XPATH, f"{self.CITY_CARD}/{self.CARD_INFO_1}"
        )

        # assert that all elements selected have the text 'Risk Level: Extreme'
        self.assertTrue(
            all(
                elem.get_attribute("innerHTML") == "Risk Level: Extreme"
                for elem in cards
            )
        )

    # def test_filter_binned_1(self):
    #     self.driver.get(f"{BASE_URL}/cities")

    #     self.driver.implicitly_wait(2)

    #     filter_dropdown = self.driver.find_element(By.ID, "deaths")
    #     filter_dropdown.click()

    #     options = filter_dropdown.find_elements(By.XPATH, "//li[@role='option']")
    #     self.assertGreaterEqual(len(options), 0)
    #     options[0].click()

    #     self.driver.implicitly_wait(2)

    #     cards = self.driver.find_elements(
    #         By.XPATH, f"{self.CITY_CARD}/{self.CARD_INFO_2}"
    #     )

    #     self.driver.implicitly_wait(5)

    #     num_cases = list(int (elem.get_attribute("innerHTML").split()[-1]) for elem in cards)

    #     # printing numbers in case of future errors
    #     print ("Deaths:", num_cases)

    #     # assert that deaths are between 0 and 50000
    #     self.assertTrue(
    #         all(
    #             0 <= case <= 50000
    #             for case in num_cases
    #         )
    #     )

    def test_url_search(self):
        """test navigation by passing in url params"""
        self.driver.get(f"{BASE_URL}/countries?currency=EUR&page=1")

        self.driver.implicitly_wait(2)

        # assert that the card with the search text appears
        cards = self.driver.find_elements(By.XPATH, self.COUNTRY_CARD)
        currency_text = [
            card.find_element(
                By.XPATH, ".//span[contains(normalize-space(text()), 'EUR')]"
            )
            for card in cards
        ]

        for elem in currency_text:
            self.assertEqual(elem.text, "Currency: EUR")

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(self):
        # print ('Quitting...')
        self.driver.quit()
        # print ('\nQuit successfully!\n')
