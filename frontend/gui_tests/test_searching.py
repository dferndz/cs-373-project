from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException
import unittest
import re

"""
make sure geckodriver is put into the path variable
"""

BASE_URL = "https://www.plansafetrips.me"

# Adapted code from the texasvotes.me repository
class TestSearching(unittest.TestCase):
    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(self):
        firefox_options = FirefoxOptions()
        firefox_options.add_argument("--headless")
        firefox_options.add_argument("--no-sandbox")
        # firefox_options.add_argument("--disable-dev-shm-usage")

        self.driver = webdriver.Firefox(options=firefox_options)

        self.COUNTRY_CARD = "//*[@data-testid='country-list-card']"
        self.CITY_CARD = "//*[@data-testid='city-list-card']"
        self.CARD_TITLE = "button/div[1]/div[2]/span[1]/span/mark"
        self.CARD_SUBTITLE = "/button/div[1]/div[2]/span[2]/span/mark"

    # def setUp(self):
    #     self.driver.get("https://plansafetrips.me")

    def test_simple_country(self):
        self.driver.get(f"{BASE_URL}/countries")
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries")

        self.driver.implicitly_wait(5)

        search_bar = self.driver.find_element(By.ID, "mui-1")
        search_bar.send_keys("United States of America")
        search_bar.send_keys(Keys.RETURN)

        # assert that the text in the search bar didn't disappear
        self.assertEqual(search_bar.get_attribute("value"), "United States of America")

        self.driver.implicitly_wait(10)
        # assert that the card with the search text appears
        elements = self.driver.find_elements(
            By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
        )
        self.assertGreater(len(elements), 0)
        for i in range(len(elements)):
            # the list must be refreshed constantly to prevent stale requests from the DOM
            try:
                text = self.driver.find_elements(
                    By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
                )[i].get_attribute("innerHTML")
                self.assertIn(text, "United States of America")
            except StaleElementReferenceException:
                pass
                # print ("Stale attempt. Skipping for now...")

        # assert that the url has changed and has the right params
        pat = re.compile("United%20States%20of%20America")
        self.assertTrue(pat.search(self.driver.current_url) is not None)

    def test_simple_country_2(self):
        self.driver.get(f"{BASE_URL}/countries")
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries")

        self.driver.implicitly_wait(5)

        search_bar = self.driver.find_element(By.ID, "mui-1")
        search_bar.send_keys("United Kingdom")
        search_bar.send_keys(Keys.RETURN)

        self.driver.implicitly_wait(5)

        # assert that the text in the search bar didn't disappear
        self.assertEqual(search_bar.get_attribute("value"), "United Kingdom")

        # assert that the card with the search text appears
        elements = self.driver.find_elements(
            By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
        )
        self.assertGreater(len(elements), 0)
        for i in range(len(elements)):
            # the list must be refreshed constantly to prevent stale requests from the DOM
            try:
                text = self.driver.find_elements(
                    By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
                )[i].get_attribute("innerHTML")
                self.assertIn(text, "United Kingdom")
            except StaleElementReferenceException:
                pass
                # print ("Stale attempt. Skipping for now...")

        # assert that the url has changed and has the right params
        pat = re.compile("United%20Kingdom")
        self.assertTrue(pat.search(self.driver.current_url) is not None)

    def test_simple_country_3(self):
        """test passing a partial search word (not a full country name)"""
        self.driver.get(f"{BASE_URL}/countries")
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries")

        self.driver.implicitly_wait(5)

        search_bar = self.driver.find_element(By.ID, "mui-1")
        search_bar.send_keys("United")
        search_bar.send_keys(Keys.RETURN)

        self.driver.implicitly_wait(5)

        # assert that the text in the search bar didn't disappear
        self.assertEqual(search_bar.get_attribute("value"), "United")

        # assert that the card with the search text appears
        elements = self.driver.find_elements(
            By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
        )
        self.assertGreater(len(elements), 0)
        for i in range(len(elements)):
            # the list must be refreshed constantly to prevent stale requests from the DOM
            try:
                text = self.driver.find_elements(
                    By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
                )[i].get_attribute("innerHTML")
                self.assertEqual("United", text)
            except StaleElementReferenceException:
                pass
                # print ("Stale attempt. Skipping for now...")

        # assert that the url has changed and has the right params
        pat = re.compile("United")
        self.assertTrue(pat.search(self.driver.current_url) is not None)

    def test_url_search(self):
        """test navigation by passing in url params"""
        self.driver.get(
            f"{BASE_URL}/countries?search=United%20States%20of%20America&page=1"
        )
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries?search=United%20%States%20of%20America&page=1")

        self.driver.implicitly_wait(5)

        # assert that the text in the search bar never showed up
        search_bar = self.driver.find_element(By.ID, "mui-1")
        self.assertEqual(search_bar.get_attribute("value"), "United States of America")

        self.driver.implicitly_wait(5)

        # assert that the card with the search text appears
        elements = self.driver.find_elements(
            By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
        )
        self.assertGreater(len(elements), 0)
        for i in range(len(elements)):
            # the list must be refreshed constantly to prevent stale requests from the DOM
            try:
                text = self.driver.find_elements(
                    By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
                )[i].get_attribute("innerHTML")
                self.assertIn(text, "United States of America")
            except StaleElementReferenceException:
                pass
                # print ("Stale attempt. Skipping for now...")

    def test_search_page(self):
        """test navigation by passing in url params"""
        self.driver.get(f"{BASE_URL}/search")
        # self.assertEqual (self.driver.current_url, f"{BASE_URL}/countries")

        self.driver.implicitly_wait(5)

        search_bar = self.driver.find_element(By.ID, "mui-1")
        search_bar.send_keys("Indonesia")
        search_bar.send_keys(Keys.RETURN)

        self.driver.implicitly_wait(5)
        # assert that the text in the search bar didn't disappear

        # assert that the card with the search text appears
        elements = self.driver.find_elements(
            By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
        )
        self.assertGreater(len(elements), 0)
        for i in range(len(elements)):
            # the list must be refreshed constantly to prevent stale requests from the DOM
            try:
                text = self.driver.find_elements(
                    By.XPATH, f"{self.COUNTRY_CARD}/{self.CARD_TITLE}"
                )[i].get_attribute("innerHTML")
                self.assertIn(text, "Indonesia")
            except StaleElementReferenceException:
                pass
                # print ("Stale attempt. Skipping for now...")

        # assert that the card with the search text appears
        elements = self.driver.find_elements(
            By.XPATH, f"{self.CITY_CARD}/{self.CARD_SUBTITLE}"
        )
        self.assertGreater(len(elements), 0)
        for i in range(len(elements)):
            # the list must be refreshed constantly to prevent stale requests from the DOM
            try:
                text = self.driver.find_elements(
                    By.XPATH, f"{self.CITY_CARD}/{self.CARD_SUBTITLE}"
                )[i].get_attribute("innerHTML")
                self.assertIn(text, "Indonesia")
            except StaleElementReferenceException:
                pass
                # print ("Stale attempt. Skipping for now...")

        # assert that the url has changed and has the right params
        pat = re.compile("Indonesia")
        self.assertTrue(pat.search(self.driver.current_url) is not None)

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(self):
        # print ('Quitting...')
        self.driver.quit()
        # print ('\nQuit successfully!\n')
