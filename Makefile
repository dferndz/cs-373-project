.DEFAULT: run
.ONESHELL:

format:
	@echo "Formatting backend"
	black .
	@echo ""
	@echo "Formatting frontend"
	cd frontend
	npm run lint
	cd ..
	@echo "Done."

start-docker:
	@echo "Starting Docker"
	cd ./backend
	docker compose up postgres -d
	docker compose up backend -d
	@echo "Done"
	@echo ""

stop-docker:
	@echo "Stopping Docker"
	docker compose down
	@echo "Done"
	@echo ""

restart-docker:
	@echo "Restarting Docker"
	@make stop-docker
	@make start-docker
	@echo "Done"

pull: 
	@echo "Getting latest commit to main branch"
	git pull origin main
	@echo "Done"
	@echo ""

install-front-end: 
	@echo "Installing tools for front-end..."
	@cd ./frontend
	npm install
	@echo "Done"
	@echo ""

front-end: install-front-end
	@echo "Starting front-end..."
	@cd ./frontend
	npm start
	@echo "Done" 
	@echo ""

test-front-end: install-front-end
	@echo "Running front-end tests..."
	@cd ./frontend
	@npm run testAll
	@echo "Done"
	@echo ""

install-back-end:
	@echo "Installing tools for back-end..."
	cd ./backend
	pip install -r requirements.txt
	@echo "Done"
	@echo ""

test-back-end: 
	@echo "Running back-end tests..."
	cd ./backend
	python3 test_main.py -a
	@EXIT_CODE=$$?
	@echo "Done"
	@echo ""
	@exit $$EXIT_CODE

back-end: start-docker
	@echo "Starting back-end container"
	@echo "Clearing screen..."
	clear
	docker exec -ti backend /bin/bash

test: test-front-end test-back-end

run-back: back-end

run-front: front-end

clean:
	@echo "Cleaning subdirectories"
	@cd frontend
	@rm -rf node_modules
	@echo "Done"
	@echo ""
